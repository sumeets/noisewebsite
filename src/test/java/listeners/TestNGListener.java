package listeners;

import org.testng.IExecutionListener;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import Base.Apache_POI_TC;
import utility.Email;

public class TestNGListener implements IExecutionListener {
	
	@Override
	public void onExecutionStart() {

	}

	@Override
	public void onExecutionFinish() {

		String outputdir=System.getProperty("user.dir")+"\\test-output";
		
	   Email.sendEmail(outputdir,Apache_POI_TC.TimeStamp);
        
	    System.out.println("Email is sent");
	}


	
}
	
	

	


