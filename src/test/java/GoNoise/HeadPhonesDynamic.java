package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.BuletoothEarPhonesPage;
import pages.HeadphonesPage;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.Wirelessearbudspage;
import utility.ExtentReport;
import utility.commonutilmethods;
import utility.values_constant;


public class HeadPhonesDynamic extends Apache_POI_TC
{
	String actual,expected;
	ArrayList <String> list;
	
	
	
	@Test(description = "Verify Noise Powr")	
	   public void TC_verifyNoisePowr(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisePowr",
					"Verify Noise Powr");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   //String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   
            /***** code added 20/07/2022 *********/
		   
		      String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			/***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(HeadphonesPage.getNoisepowrheader()).getText(), "Noise Powr", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Noise Powr Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Headphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noise Powr is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify One Headphones")	
	   public void TC_verifyOneHeadPhones(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyOneHeadPhones",
					"Verify One Headphones");
		   
		   
		   System.out.println("name"+name);
			
			   //String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   
             /***** code added 20/07/2022 *********/
		   
		   	   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(HeadphonesPage.getoneheadphonesheader()).getText(), "One Headphones", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to One Headphones Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Headphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("One Headphones is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Defy ANC Headphones")	
	   public void TC_verifyDefyANCHeadphones(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyDefyANCHeadphones",
					"Verify Defy ANC Headphones");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			  // String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   
		   		/***** code added 20/07/2022 *********/
		   
		   		String name1=name.toLowerCase();
		   		//span[contains(text(),'ColorFit Pulse Grand')]
		   		String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
		   
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(HeadphonesPage.getDefyANCHeadphonesheader()).getText(), "Defy ANC Headphones", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Defy ANC Headphones Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Headphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Defy ANC Headphones is sold out");
			 
		   }
		   
		  
	   }
	
	
	
}
