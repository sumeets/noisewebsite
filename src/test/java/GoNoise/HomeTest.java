package GoNoise;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import utility.ExtentReport;


public class HomeTest extends Apache_POI_TC
{
	
   @Test(description = "Verify product click functionality",priority=1)	
   public void TC_verifyClickProductLink() throws InterruptedException
   {
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickProductLink",
				"Verify product click functionality");
	   
	   driver.findElement(NoiseHomePage.getProductLink()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Product Link is Clicked");
	   
	   
	   cm.waitMethod();
	   
	   cm.scrolldown();
	   
	   /************ Go Back to Home Page *********/
	   
	   cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
	   /************ Go Back to Home Page *********/
   }
   
   @Test(description = "Verify support click functionality",priority=2)	
   public void TC_verifyClickSupportLink() throws InterruptedException
   {
	   cm.waitMethod();
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickSupportLink",
				"Verify support click functionality");
	   
	   driver.findElement(NoiseHomePage.getSupportLink()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Support Link is Clicked");
	   
	   cm.waitMethod();
	   
	   cm.scrolldown();
	   
	   ExtentReport.ExtentReportInfoLog("Scroll down to below");
	   
	   //System.out.println("Scroll down to below");
	   
	   /************ Go Back to Home Page *********/
	   
	   cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
	   
	   /************ Go Back to Home Page *********/
   }
   
   @Test(description = "Verify stories click functionality",priority=3)	
   public void TC_verifyClickStoriesLink() throws InterruptedException
   {
	   cm.waitMethod();
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickStoriesLink",
				"Verify stories click functionality");
	   
	   driver.findElement(NoiseHomePage.getStoriesLink()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Stories Link is Clicked");
	   
	   cm.waitMethod();
	   
	   cm.scrolldown();
	   
	   System.out.println("Scroll down to below");
	   
	   /************ Go Back to Home Page *********/
	   
	   cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	      
	   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
	   /************ Go Back to Home Page *********/
   }
   
   @Test(description = "Verify search functionality",priority=4)	
   public void TC_verifySearchFunction() throws InterruptedException
   {
	   cm.waitMethod();
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySearchFunction",
				"Verify search functionality");
	   
	   driver.findElement(NoiseHomePage.getSearchIcon()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Search Icon is clicked");
	   
	   driver.findElement(By.id("menuSearch")).sendKeys("colorfit pro 3 assist");
	   
	   cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getSearchText()).click();
	   
	   ExtentReport.ExtentReportInfoLog("product is searched");
	   
	   cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
   }
   
   @Test(description = "Verify Cart Click functionality",priority=5)	
   public void TC_verifyClickCart() throws InterruptedException
   {
	   cm.waitMethod();
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickCart",
				"Verify Cart Click functionality");
	   
	   driver.findElement(NoiseHomePage.getCartIcon()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Cart Icon is clicked");
	   
	   cm.waitMethod();
	   
	   String actualcarttext=driver.findElement(NoiseHomePage.getCartEmptyText()).getText();
	   
	   Assert.assertEquals(actualcarttext, "Your cart is empty", "Not Navigated to Cart Page");
	   
	   ExtentReport.ExtentReportInfoLog("Navigated to Cart Page");
	   
	   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
   }
   
   @Test(description = "Verify Account Click functionality",priority=6)	
   public void TC_verifyClickAccount() throws InterruptedException
   {
	   cm.waitMethod();
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickAccount",
				"Verify Account Click functionality");
	   
	   driver.findElement(NoiseHomePage.getMyAccountIcon()).click();
	   
	   ExtentReport.ExtentReportInfoLog("My Account Icon is clicked");
	   
	   
	   String actualText=driver.findElement(NoiseHomePage.getHelloNoiseMaker()).getText();
	   
	   Assert.assertEquals(actualText, "Hello NoiseMaker!");
	   
	   ExtentReport.ExtentReportInfoLog("Navigated to Account Page");
	  
	   cm.waitMethod();
	   
      driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
      ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
   }
   
   @Test(description = "Verify Scroll down and Know More click",priority=7)	
   public void TC_scrollToKnowMoreAndClick() throws InterruptedException
   {
	   cm.waitMethod();
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_scrollToKnowMoreAndClick",
				"Verify Scroll down and Know More click");
	   
	   /******************** Start Scroll and click of Wireless Earbuds and Smart Watches **************/
	   
	   cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getWirelessBudsText()));
	   
	   ExtentReport.ExtentReportInfoLog("Scroll down to Wireless Earbuds");
	
	   cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getKnowMoreWirelessBuds()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Know More of Wireless Earbuds is clicked");
	  
	   cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
	   cm.waitMethod();
	   
       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getWirelessBudsText()));
       
       ExtentReport.ExtentReportInfoLog("Scroll down to Smart Watches");
       
       cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getKnowMoreSmartWatches()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Know More of Smart Watches is clicked");
	  
	   cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
	   cm.waitMethod();
	   
	   /******************** End  Scroll and click of Wireless Earbuds and Smart Watches **************/
	   
	   /******************** Start Scroll and click of Bluetooth Neckbands and Accessories **************/
	   
       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getBluetoothNeckbandsText()));
       
       ExtentReport.ExtentReportInfoLog("Scroll down to Bluetooth Neckbands");
       
       cm.waitMethod();
	   
	   driver.findElement(NoiseHomePage.getKnowMoreBluetoothNeckbands()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Know More of Bluetooth Neckbands is clicked");
	   
	   cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
	   cm.waitMethod();
	   
       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getBluetoothNeckbandsText()));
       
       ExtentReport.ExtentReportInfoLog("Scroll down to Bluetooth Neckbands");
      
       cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getKnowMoreAccessories()).click();
       
       ExtentReport.ExtentReportInfoLog("Know More of Accessories is clicked");
	   
	   cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
	   /******************** End Scroll and click of Bluetooth Neckbands and Accessories **************/
	   
	   
	  
   }
   
   @Test(description = "Verify Scroll down Read All and click",priority=8)	
   public void TC_scrollToReadAllAndClick() throws InterruptedException
   {
	   cm.waitMethod();
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_scrollToReadAllAndClick",
				"Verify Scroll down Read All and click");
	   
       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getreadalltext()));
       
       ExtentReport.ExtentReportInfoLog("Scroll down to Read All Text");
        
       cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getReadAllLink()).click();
       
       ExtentReport.ExtentReportInfoLog("Read all is clicked and navigated to page");
	   
	  
	   cm.waitMethod();
	   
	   cm.scrolldown();
	   
	   cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   
   }
   
   @Test(description = "Verify Scroll down to Watch Now",priority=9)	
   public void TC_scrollToWatchNow() throws InterruptedException
   {
       cm.waitMethod();
       
       ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_scrollToWatchNow",
				"Verify Scroll down to Watch Now");
	   
       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getWatchNow()));
       
       ExtentReport.ExtentReportInfoLog("Scroll down to Watch Now");
       
     
       cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getWatchNow()).click();
       
       ExtentReport.ExtentReportInfoLog("Click on Watch Now");
	  
	   cm.waitMethod();
	   
	   /*************** Switch to the video frame **********/
	   
	   
	   //driver.switchTo().frame(1);
	   
	   driver.switchTo().frame(driver.findElement(By.xpath("//div[@class='video-wrapper']/iframe")));
	   
	   cm.waitMethod();
	   
	   /*************** Switch to the video frame **********/
	   
	   /*************** Play and pause the video ************/
	   
	  
	   
	   cm.YouTubeVideoPlayPause(driver.findElement(NoiseHomePage.getVideoPlayIcon()));
	  
	  /*************** Play and pause the video ************/
	   
	   ExtentReport.ExtentReportInfoLog("Video start playing.");
	   
	  
	   cm.waitMethod();
	   
	   driver.navigate().back();
	   
	   
	   cm.waitMethod();
	   
       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
	   
       ExtentReport.ExtentReportInfoLog("Navigate back to Home Page");
	  
	   
   }
   
   @Test(description = "Verify Scroll down to Sign Up",priority=10)	
   public void TC_scrollToSignUp() throws InterruptedException
   {
 	  
	      cm.waitMethod();
	      ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_scrollToSignUp",
					"Verify Scroll down to Sign Up");
	   
	      cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getsignupstatictext()));
	      
	      ExtentReport.ExtentReportInfoLog("Scrolled down to Sign Up Subscribe");
	      
	      driver.findElement(NoiseHomePage.getsignuptxtbox()).sendKeys("testtut@test.com");
	      
	      ExtentReport.ExtentReportInfoLog("Email Entered");
	      
	      cm.waitMethod();
	      
	      driver.findElement(NoiseHomePage.getsignupbutton()).click();
	      
	      cm.waitMethod();
	      
	      ExtentReport.ExtentReportInfoLog("Sign Up Button is clicked");
	      
	      //Using id tagname attribute combination for css expression
	      WebElement para = driver.findElement(NoiseHomePage.getparatag());
	      // getting the type attribute and printing in console
	      String attributevalue = para.getAttribute("class");
	      
	      //System.out.println("attribute value:"+attributevalue);
	      
	      if(attributevalue.equalsIgnoreCase("success"))
	      {
	   	   Assert.assertEquals(driver.findElement(NoiseHomePage.getsignupsuccesstext()).getText(), "Thanks for subscribing!");
	   	   
	   	   ExtentReport.ExtentReportInfoLog("Thanks for subscribing!");
	   	  
	   	 
	      }
	      else
	      {
	   	   Assert.assertEquals(driver.findElement(NoiseHomePage.getsignuperrortext()).getText(), "Email already taken");
	   	   
	   	   ExtentReport.ExtentReportInfoLog("Email already taken");
	   	   
	   	   
	      }
	      
	     
	     
 	  
   }
   
   
   @Test(description = "Verify Footer Links",priority=11)	
   public void TC_verifyFooterLinks() throws InterruptedException
   {
 	  cm.waitMethod();
 	  
 	 ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyFooterLinks",
				"Verify Footer Links");
 	  
 	  cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Scroll down to the footer");
 	  
 	  /*************** Our Products  and For Business ***********/
 	 
 	ExtentReport.ExtentReportInfoLog("Click on smart watches link");
 	  
 	  driver.findElement(NoiseHomePage.getsmartwatcheslink()).click();
 	  
 	  Thread.sleep(3000);
 	  
 	
 	  cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on earphones link");
 	  
 	  driver.findElement(NoiseHomePage.getearphoneslink()).click();
 	  
 	  Thread.sleep(3000);
 	  
 	  cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on wireless earbuds link");
 	  
 	  driver.findElement(NoiseHomePage.getwirelessearbudslink()).click();
 	  
 	  Thread.sleep(3000);
 	  
 	  cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on noisefit app link");
 	  
       driver.findElement(NoiseHomePage.getnoisefitapplink()).click();
 	  
 	  Thread.sleep(3000);
 	  
 	  cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on accessories link");
 	  
       driver.findElement(NoiseHomePage.getaccessorieslink()).click();
 	  
 	  Thread.sleep(3000);
 	  
 	  cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on corporate enquiries link");
 	  
 	  driver.findElement(NoiseHomePage.getcorporateenquirieslink()).click();
 	  
       Thread.sleep(3000);
 	  
 	  cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on affiliate link");
 	 
       driver.findElement(NoiseHomePage.getaffiliatelink()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	  
 	  /*************** Our Products ***********/
 	  
 	  /*************** Support ***********/
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on faq link");
 	  
       driver.findElement(NoiseHomePage.getfaqlink()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on track order link");
 	  
       driver.findElement(NoiseHomePage.gettrackorderlink()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on download invoice link");
 	  
       driver.findElement(NoiseHomePage.getdownloadinvoice()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on register warranty link");
 	  
       driver.findElement(NoiseHomePage.getregisterwarranty()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on register complaint link");
 	  
       driver.findElement(NoiseHomePage.getregistercomplaint()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on product support link");
 	  
       driver.findElement(NoiseHomePage.getproductsupport()).click();
 	  
       Thread.sleep(3000);
       
       driver.navigate().back();
 	   
 	  cm.waitMethod();
 	 ExtentReport.ExtentReportInfoLog("Click on warranty guidelines link");
 	  
       driver.findElement(NoiseHomePage.getwarrantyguidelines()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on shipping returns link");
 	  
       driver.findElement(NoiseHomePage.getshippingreturns()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on contact us link");
 	  
       driver.findElement(NoiseHomePage.getcontactus()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	  /*************** Support ***********/
 	  
       /*************** About Noise ***********/
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on about us link");
 	  
       driver.findElement(NoiseHomePage.getaboutus()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on stories link");
 	  
       driver.findElement(NoiseHomePage.getstories()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on careers link");
 	  
 	  driver.findElement(NoiseHomePage.getcareers()).click();
 			  
 	   Thread.sleep(3000);
 		      
 	  cm.scrolldown();
 			  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on press link");
 			  
 	  driver.findElement(NoiseHomePage.getpress()).click();
 	  
       Thread.sleep(3000);
 	  
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	  
 	  /*************** About Noise ***********/
 	  
       /*************** Legal ***********/
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on terms link");
       
       driver.findElement(NoiseHomePage.getterms()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on privacy link");
 	  
       driver.findElement(NoiseHomePage.getprivacy()).click();
 	  
       Thread.sleep(3000);
       
       cm.scrolldown();
 	  
 	  cm.waitMethod();
 	  
 	  /*************** Legal ***********/
       
       /*************** Follow Us ***********/
 	  
 	 ExtentReport.ExtentReportInfoLog("Click on facebook link");
       
       driver.findElement(NoiseHomePage.getfacebooklink()).click();
 	  
       cm.waitMethod();
       
       cm.switchwindow();
       
       cm.waitMethod();
       
       ExtentReport.ExtentReportInfoLog("Click on instagram link");
 	  
       driver.findElement(NoiseHomePage.getinstalink()).click();
 	  
       cm.waitMethod();
       
       cm.switchwindow();
       
       cm.waitMethod();
       
       ExtentReport.ExtentReportInfoLog("Click on youtube link");
     
       driver.findElement(NoiseHomePage.getyoutubelink()).click();
 	  
       cm.waitMethod();
       
       cm.switchwindow();
       
       cm.waitMethod();
       
       
       
       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
 	   
       ExtentReport.ExtentReportInfoLog("Noise logo is clicked and navigated to home page");
 	  
 	   cm.waitMethod();
       
       /*************** Follow Us ***********/
 	  
   }
  
}
