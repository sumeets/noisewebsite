package GoNoise;

import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.ContactUsPage;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class SupportFooterLinks extends Apache_POI_TC
{
	
	@Test(description = "Verify FAQ Click",priority=1)	
	   public void TC_verifyClickFAQ() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickFAQ",
					"Verify FAQ Click");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getfaqlink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("FAQ Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("FAQ Page is opened");
			   
			   try
			   {
				   cm.scrolldowntoElement(driver.findElement(By.xpath("//h3[normalize-space()='Have a question? Ask us']")));
				   
				  // cm.scrolldown();
				   
				   driver.findElement(By.xpath("(//a[contains(text(),'Contact Us')])[1]")).click();
				   
				   Thread.sleep(10000);
				   
				   ExtentReport.ExtentReportInfoLog("Contact Us button is clicked and redirected to Contact Us Page");
				   
				 
			   }
			   
			   catch(Exception e)
			   {
				   ExtentReport.ExtentReportInfoLog("Contact Us is not found nor clicked");
			   }
			   
			   driver.navigate().to("https://www.gonoise.com/");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
			   
			 
			   
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("FAQ Link is not Clicked");
		   }
		  
		 
	   }
	
	
	  @Test(description = "Verify Download Invoice",priority=2,enabled=false)	
	   public void TC_verifyClickDownloadInvoice() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickDownloadInvoice",
					"Verify Download Invoice");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getdownloadinvoice()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Download Invoice Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Download Invoice Page is opened");
			   
			   
				   /*************************** valid order 1936834 and 8964001354 *************/
			   
				  driver.findElement(By.id("order-id")).sendKeys("12345");
				  
				  ExtentReport.ExtentReportInfoLog("Order id is entered");
				   
				  cm.waitMethod();
				   
	              driver.findElement(By.id("mobile-number")).sendKeys("8240724302");
				  
				  ExtentReport.ExtentReportInfoLog("Mobile Number is entered");
				   
				  cm.waitMethod();
				  
				  driver.findElement(NoiseHomePage.getinvoicebutton()).click();
				  
				  ExtentReport.ExtentReportInfoLog("download invoice button is clicked");
				   
			      Thread.sleep(5000);
			      
			      String errmsg=driver.findElement(By.id("download-invalid-detail")).getText();
			      
			      System.out.println("error message is"+errmsg);
			      
			     if(errmsg!="")
			    {
			    	 //ExtentReport.ExtentReportInfoLog("error is "+driver.findElement(By.id("download-invalid-detail")).getText());
			    	 ExtentReport.ExtentReportErrorLog("error is "+errmsg);
			    }
			     else
			     {
			    	 ExtentReport.ExtentReportInfoLog("Invoice generated");
			    	 
			     }
			   
			     driver.navigate().to("https://www.gonoise.com/");
				   
				   cm.waitMethod();
				   
				   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Download Invoice Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	  
	  @Test(description = "Verify Track Your Order",priority=3,enabled=false)	
	   public void TC_verifyClickTrackYourOrder() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickTrackYourOrder",
					"Verify Track Your Order");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
				   driver.findElement(NoiseHomePage.gettrackorderlink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Track Your Order Link is Clicked");
				   
				   
				   
				   cm.waitMethod();
				   
				   ExtentReport.ExtentReportInfoLog("Track Your Order Page is opened");
			   
			   
				   /*************************** valid order 1936834 and 8964001354 *************/
			   
				  driver.findElement(By.name("order_id")).sendKeys("1936834");
				  
				  ExtentReport.ExtentReportInfoLog("Order id is entered");
				   
				  cm.waitMethod();
				   
	              
				  driver.findElement(trackyourorderpage.getBtnTrackOrder()).click();
				  
				  ExtentReport.ExtentReportInfoLog("Track Order button is clicked");
				  
				  ExtentReport.ExtentReportInfoLog("It is redirected to shiprocket page");
				   
			      Thread.sleep(5000);
			      
			      String orderId="1936834";
			      
			      driver.findElement(By.name("track_id")).sendKeys(orderId);
			      
			      cm.waitMethod();
			      
			      driver.findElement(trackyourorderpage.getBtnSubmit()).click();
			      
			      Thread.sleep(5000);
			      
			    //  cm.scrolldown();
			      
			     System.out.println(driver.findElements(By.xpath("//strong[@id='error_text']")).size());
			     int size=driver.findElements(By.xpath("//strong[@id='error_text']")).size();
			     
			     /********* If error message exists **********/
			     if(size>0)
			     {
			    	 String errmsg=driver.findElement(By.xpath("//strong[@id='error_text']")).getText();
				      
				      Thread.sleep(3000);
				      
				      ExtentReport.ExtentReportErrorLog(errmsg);
			     }
			     else
			     {
			    	 Assert.assertEquals(driver.findElement(By.xpath("(//span[contains(@class,'pull-right')])[1]")).getText(),orderId);
			    	 
			    	 ExtentReport.ExtentReportInfoLog("Invoice page is opened with Order Id"+orderId);
			     }
			     
			     driver.navigate().to("https://www.gonoise.com/");
				   
				   cm.waitMethod();
				   
				   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
			 
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Track Order Link is not Found nor clicked");
		   }
		  
		 
	   }
	  
	
   @Test(description = "Verify Shipping and returns",priority=4)	
   public void TC_verifyShippingAndReturns() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyShippingAndReturns",
				"Verify Shipping and returns");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	  // cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
			   driver.findElement(NoiseHomePage.getshippingreturns()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Shipping and Returns Link is Clicked");
			   
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Shipping and Returns Page is opened");
		   
		       cm.scrolldown(); 
		       
		       ExtentReport.ExtentReportInfoLog("Scroll down to below");
		       
		       cm.waitMethod();
		       
		       driver.navigate().back();
		       
		       ExtentReport.ExtentReportInfoLog("Going back to Home Page");
		       
		       cm.waitMethod();
			
		 
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Shipping and Returns Link is not Found nor clicked");
	   }
	  
	 
   }
   
   @Test(description = "Verify Warranty Guidelines",priority=5)	
   public void TC_verifyWarrantyGuidelines() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyWarrantyGuidelines",
				"Verify Warranty Guidelines");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	   cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
			   driver.findElement(NoiseHomePage.getwarrantyguidelines()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Warranty and Guidelines Link is Clicked");
			   
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Warranty and Guidelines Page is opened");
		   
		       cm.scrolldown(); 
		       
		       ExtentReport.ExtentReportInfoLog("Scroll down to below");
		       
		       cm.waitMethod();
		       
		       driver.navigate().back();
		       
		       ExtentReport.ExtentReportInfoLog("Going back to Home Page");
		       
		       cm.waitMethod();
			
		 
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Warranty and Guidelines is not Found nor clicked");
	   }
	  
	 
   }

   @Test(description = "Verify Product Support",priority=6)	
   public void TC_verifyProductSupport() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyProductSupport",
				"Verify Product Support");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	   cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
			   driver.findElement(NoiseHomePage.getproductsupport()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Product Support Link is Clicked");
			   
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Product Support Page is opened");
		   
		       cm.scrolldown(); 
		       
		       ExtentReport.ExtentReportInfoLog("Scroll down to below");
		       
		       cm.waitMethod();
		       
		       driver.navigate().back();
		       
		       ExtentReport.ExtentReportInfoLog("Going back to Home Page");
		       
		       cm.waitMethod();
			
		 
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Product Support is not Found nor clicked");
	   }
	  
	 
   }
   
   @Test(description = "Verify Contact Us",priority=7)	
   public void TC_verifyContactUs() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyContactUs",
				"Verify Contact Us");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	   cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
			   driver.findElement(NoiseHomePage.getcontactus()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Contact Us Link is Clicked");
			   
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Contact Us Page is opened");
		   
			   EnterForm();
		       
			   ExtentReport.ExtentReportInfoLog("Contact Us form is submitted");
		      
		 
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Contact Us is not Found nor clicked");
	   }
	  
	 
   }

   
   private void EnterForm() throws InterruptedException {
		
		
	   driver.findElement(By.name("full_name")).sendKeys("TestData");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Full Name is entered");
	   
	   driver.findElement(By.name("email")).sendKeys("test@test.com");
	   
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Email is entered");
	   
	   driver.findElement(By.name("phone")).sendKeys("8240724302");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Mobile is entered");
	   
	   driver.findElement(By.name("subject")).sendKeys("Hello this is a subject");
	   
	   cm.waitMethod();
	   
	  
	   ExtentReport.ExtentReportInfoLog("Subject is entered");
	   
	   
	   driver.findElement(By.name("message")).sendKeys("Hi this is a message");
	   
	   cm.waitMethod();
	   
	  
	   ExtentReport.ExtentReportInfoLog("Message is entered");
	   
	   cm.waitMethod();
	   
	
	
	   
       driver.findElement(ContactUsPage.getsubmitbutton()).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Submit button is clicked");
	   
	   
   }

  
}
