package GoNoise;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.values_constant;


public class smartwatchesTest2 extends Apache_POI_TC
{
	String actual,expected;
	
   @Test(description = "Verify Smart Watch Brio",priority=1)	
   public void TC_verifySmartWatchBrio() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchesBrio",
				"Verify Smart Watch Brio");
	   
	   WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
	   cm.hoverElement(mainMenu);
	   
	   cm.waitMethod();
	   
	   driver.findElement(ProductCartCheckoutPage.getViewAllsmartwatches()).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Smart Watches");
	   
	   /**************** click on load more link ************/
	   try
	   {
		   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
		   {
			   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
			   cm.waitMethod();
			   cm.scrollup();
			   cm.waitMethod();
		   }
	   }
	   catch(Exception e)
	   {
	    
		   ExtentReport.ExtentReportInfoLog("Load More Link not found");
		  
	   }
	  
	   
	   /**************** click on load more link ************/
	   
	  
	  try
	  {
		  if(cm.existsElement(SmartWatchesPage.getbrio()))
		  {
			  WebElement brio=driver.findElement(SmartWatchesPage.getbrio());
			   cm.scrolldowntoElement(brio);
			   
			   ExtentReport.ExtentReportInfoLog("Scroll down to Brio");
			   
		  }
	  }
	  catch(Exception e)
	   {
		  ExtentReport.ExtentReportInfoLog("Brio watch not present in smart watches listing");
		    	
		 
	   }
	  
	   
	   try {
			   if(cm.existsElement(ProductCartCheckoutPage.getbriolink()))
			   {
				   driver.findElement(ProductCartCheckoutPage.getbriolink()).click();
				   
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getbrioheader()).getText(), "ColorFit Brio", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Brio Product");
				   
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				  
				   //System.out.print(values_constant.setBrioColors().get(0));
				   int count=values_constant.setBrioColors().size();
				   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				  // System.out.println("licount"+licount);
				   WebElement speciallaunchtext= driver.findElement(SmartWatchesPage.getspeciallaunchtext());
				   cm.scrolldowntoElement(speciallaunchtext);
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setBrioColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								  
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Brio watch is not present or clickable");
		 
	   }
	   
	   driver.navigate().back();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   
   }
   
   @Test(description = "Verify Smart Watch Qube O2",priority=2)	
   public void TC_verifySmartWatchQube() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchQube",
				"Verify Smart Watch Qube O2");
	 
	   try
	   {
		   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
		   {
			   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
			   cm.waitMethod();
			   cm.scrollup();
			   cm.waitMethod();
		   }
	   }
	   catch(Exception e)
	   {
	    
		    	 System.out.print("Load More Link not found");
		 
	   }
	   
	   
	   try
		  {
			  if(cm.existsElement(SmartWatchesPage.getbrio()))
			  {
				  WebElement brio=driver.findElement(SmartWatchesPage.getbrio());
				   cm.scrolldowntoElement(brio);
				   
			  }
		  }
		  catch(Exception e)
		   {
		    
			    	 System.out.print("element not present in smart watches listing");
			 
		   }
	   
	   try {
		   
		   if(cm.existsElement(SmartWatchesPage.getqubelink()))
		   {
			   cm.waitMethod();
			   driver.findElement(SmartWatchesPage.getqubelink()).click();
			   driver.navigate().refresh();    // This line is added as the page is getting blank now 
			   cm.waitMethod();
			   Assert.assertEquals(driver.findElement(SmartWatchesPage.getqubeheader()).getText(), "ColorFit Qube O2", "Header Text not matched");
			   //ExtentReport.ExtentReportInfoLog("Redirected to brio");
			   ExtentReport.ExtentReportInfoLog("Redirected to Qube Product");
			   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
			   cm.waitMethod();
			  
			   //System.out.print(values_constant.setBrioColors().get(0));
			   int count=values_constant.setQubeColors().size();
			   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
			   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
			   System.out.println("licount"+licount);
			   System.out.println("count"+count);
			   WebElement qty= driver.findElement(SmartWatchesPage.getqty());
			   cm.scrolldowntoElement(qty);
			   
			   cm.waitMethod();
			  
			    if(licount==count)
				   {
					   for(int i=0;i<licount;i++)
					   {
						   try
						   {
							   expected =values_constant.setQubeColors().get(i);
							   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
							   Assert.assertEquals(actual,expected);
							   
							   System.out.println("color matched:"+actual);
							   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
							 
							   try
							   {
								   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
								   {
									   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
									   driver.findElement(SmartWatchesPage.getnotifyme()).click();
									   cm.waitMethod();
									   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
									   cm.waitMethod();
									   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
								   }
								
								
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
								   
							   }
							  
							   try
							   {
								   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
								   ExtentReport.ExtentReportInfoLog("Next color is clicked");
								   cm.waitMethod();
							   }
								   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog("Next label color not found");
								   break;
								   
							   }
							   
							  
							  
						   }
						   
						   catch(Exception ex)
						   {
							   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
							   break;
						   }
						 
						  
					   }
				   }
			    else
			    {
			    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
			    }
			   
			   
			   
		   }
   }
    catch(Exception e)
   {
    
    	ExtentReport.ExtentReportInfoLog("Qube watch is not present or clickable");
	 
   }
   
   driver.navigate().back();
   cm.waitMethod();
   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
   }
   
  
  
   
   
}
