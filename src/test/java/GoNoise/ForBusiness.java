package GoNoise;

import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.ContactUsPage;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class ForBusiness extends Apache_POI_TC
{
	
	@Test(description = "Verify Corporate Enquiries",priority=1)	
	   public void TC_verifyCorporateEnquiries() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyCorporateEnquiries",
					"Verify Corporate Enquiries");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getcorporateenquirieslink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Corporate Enquiries Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Corporate Enquiries Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down");
			   
			
			   
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Corporate Enquiries is not Clicked");
		   }
		  
		 
	   }
	
	
	  @Test(description = "Verify Become an Affiliate",priority=2)	
	   public void TC_verifyAffiliate() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAffiliate",
					"Verify Become an Affiliate");
		   
		   /****************** Click on not now in Popup *****************/
		   
		/*   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();*/
		   
			/* if(!driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }*/
			 
			 /****************** Click on not now in Popup *****************/
		   
		/*   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");*/
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getaffiliatelink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Become an Affiliate Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Become an Affiliate Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down");
				
			   
		       driver.navigate().to("https://www.gonoise.com/");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Become an Affiliate Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	
}
