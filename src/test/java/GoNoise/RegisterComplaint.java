package GoNoise;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import utility.ExtentReport;


public class RegisterComplaint extends Apache_POI_TC
{
	
   @Test(description = "Verify Register Complaint",priority=1)	
   public void TC_verifyClickRegisterComplaint() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickRegisterComplaint",
				"Verify Register Complaint");
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
		   driver.findElement(NoiseHomePage.getregistercomplaint()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Register Complaint is Clicked");
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("Register Complaint page is opened");
		   
		   try
		   {
			   driver.findElement(NoiseHomePage.getregisteronlinecomplaint()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Register an online complaint button is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Register Complaint Form is opened");
			   
			   EnterForm();
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Register an online complaint button is not Clicked");
		   }
		   
		   
		  
		   
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Register Complaint is not Clicked");
	   }
	  
	   
	   
	  
	 
   }
   
   

private void EnterForm() throws InterruptedException {
	
	
	  driver.findElement(By.id("full-name")).sendKeys("testabcd");
	   
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Full Name is entered");
	   
	   driver.findElement(By.id("email")).sendKeys("abcd@gmail.com");
	   
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Email is entered");
	   
	   driver.findElement(By.id("mobile-number")).sendKeys("8240724302");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Mobile is entered");
	   
	   driver.findElement(Registercomplaintpage.getNextbutton()).click();
	   
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Next button is clicked");
	   
       /************************ OTP ************************/
	   
	   Scanner user= new Scanner(System.in);
	   System.out.println("Enter the OTP: ");
	   String usern=user.nextLine();
	   cm.waitMethod();
	   
	   
	   driver.findElement(Registercomplaintpage.getOTPTxtbox()).sendKeys(usern);
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("OTP is entered");
	   
	   driver.findElement(Registercomplaintpage.getverifyOTPButton()).click();
	   
	   Thread.sleep(10000);
	   
	   ExtentReport.ExtentReportInfoLog("OTP is verified");
	   
	   driver.findElement(By.name("w3review")).sendKeys("abcd");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Description is entered");
	   
	   cm.scrollup();
	   
	   cm.waitMethod();
	   
	   driver.findElement(By.xpath("//div[@class='select-product-css css-b62m3t-container']")).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Product Name is clicked");
	   
	   driver.findElement(By.id("react-select-2-input")).sendKeys(Keys.DOWN);
	   cm.waitMethod();
	   
	   driver.findElement(By.id("react-select-2-input")).sendKeys(Keys.DOWN);
	   cm.waitMethod();
	   
	   driver.findElement(By.id("react-select-2-input")).sendKeys(Keys.ENTER);
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Product is selected");
	   
       driver.findElement(Registercomplaintpage.getNextbutton()).click();
	   
	   Thread.sleep(10000);
	   
	   
	   ExtentReport.ExtentReportInfoLog("Next button is clicked and Switched to Next page");
	   
	   
	   /*********************** OTP **************************/
	   
	   
	   
	   /********************* Purchase Info ***************/
	   
	   
	   driver.findElement(By.xpath("//input[@placeholder='Enter your Purchase Date']")).click();
	   cm.waitMethod();
	   
	   driver.findElement(By.xpath("//input[@placeholder='Enter your Purchase Date']")).sendKeys("11072022");
	   
	   ExtentReport.ExtentReportInfoLog("Purchase date is filled");
	   
	   Thread.sleep(10000);
	   
	   /**************** File Upload **************/
	   
	   WebElement upload_file = driver.findElement(By.id("my-file"));

	   upload_file.sendKeys("D:\\Noise Website\\sample image\\1.png");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Product Image is uploaded");
	   
	   
	   WebElement upload_file1 = driver.findElement(By.id("my-invoice"));

	   upload_file1.sendKeys("D:\\Noise Website\\sample image\\sample.pdf");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Product Invoice is uploaded");
	   
       driver.findElement(Registercomplaintpage.getNextbutton()).click();
	   
	   Thread.sleep(10000);
	   
	   
	   ExtentReport.ExtentReportInfoLog("Next button is clicked and Switched to Next page");
	   
	   /**************** File Upload **************/
	   
	   /********************* Purchase Info ***************/
	   
	   /********************* Address Info ***************/
	   
	   driver.findElement(Registercomplaintpage.getshippingaddress()).sendKeys("Behala,kolkata,WB");
	   
	   ExtentReport.ExtentReportInfoLog("Shipping address is filled");
	   
	   cm.waitMethod();
	   
      driver.findElement(Registercomplaintpage.getlandmark()).sendKeys("near golftower");
      
      ExtentReport.ExtentReportInfoLog("landmark is filled");
	   
	   cm.waitMethod();
	   
      driver.findElement(Registercomplaintpage.getpincode()).sendKeys("700055");
      
      ExtentReport.ExtentReportInfoLog("pincode is filled");
	   
	   cm.waitMethod();
	   
      driver.findElement(Registercomplaintpage.getsubmitbutton()).click();
      
      ExtentReport.ExtentReportInfoLog("submit button is clicked and Form is submitted");
	   
	   cm.waitMethod();
	   
	   /********************* Address Info ***************/
	   
}
   
 
   
  
}
