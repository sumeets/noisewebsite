package GoNoise;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import utility.ExtentReport;
import utility.Xls_Reader;
import utility.constant;


public class BrioTest extends Apache_POI_TC
{
	String q1,a1;
	
	 @Test(description = "scroll to Brio and click",priority=1)	
	   public void verifyClickProductLink() throws InterruptedException
	   {
		   WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.findElement(ProductCartCheckoutPage.getViewAllsmartwatches()).click();
		   
		   cm.waitMethod();
		   
		   System.out.println("View all Link is Clicked");
		   
		   WebElement brio=driver.findElement(ProductCartCheckoutPage.getbrio());
		   cm.scrolldowntoElement(brio);
		   
		   System.out.println("Scroll to the Colorfit Brio");
		   
		   driver.findElement(ProductCartCheckoutPage.getbriolink()).click();
		   
		   cm.waitMethod();
		 
		   WebElement faqs=driver.findElement(By.xpath("//div[@class='content-wrap']"));
		   
		   cm.scrolldowntoElement(faqs);
		   
		   cm.waitMethod();
		   
		   driver.findElement(By.xpath("//p[normalize-space()='ABOUT THE PRODUCT']/parent::li")).click();
		   
		   cm.waitMethod();
		   
		  
		   
		   
	   }
		   
	
	

   @Test(description = "read data from sheet",priority=2)	
   public void TC_readDataFromSheet() throws InterruptedException
   {
	   System.out.println("path"+System.getProperty("user.dir")+"\\resources\\"+constant.File_TestData);
	   
	   Xls_Reader reader = new Xls_Reader(System.getProperty("user.dir")+"\\resources\\"+constant.File_TestData);
		String sheetName = "FAQ";
		int rowCount = reader.getRowCount(sheetName);
		
		for(int rowNum=2; rowNum<=rowCount; rowNum++){
			String question = reader.getCellData(sheetName, "question", rowNum);
			String answer = reader.getCellData(sheetName, "answer", rowNum);
			
		
			 q1=driver.findElement(By.xpath("//b[contains(text(),"+question+")]")).getText();
			 a1=driver.findElement(By.xpath("//p[contains(text(),"+answer+")]")).getText();
			System.out.println(question + " " + answer);
			System.out.println("-------------");
			System.out.println(q1 + " " + a1);
			
			/*Assert.assertEquals(q1, question, "q1 not matched");
			Assert.assertEquals(a1, question, "a1 not matched");
			
			System.out.println("question1 and answer1 are matched");*/
			
		}
		
   }
   
}