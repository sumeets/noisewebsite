package GoNoise;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.values_constant;


public class SmartWatchesNew extends Apache_POI_TC
{
	String actual,expected;
	
	@Test(description = "Verify Smart Watch Xfit-1",priority=1)	
	   public void TC_verifySmartWatchXfit() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchXfit",
					"Verify Smart Watch Noise X-Fit 1");
		   
		   WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.findElement(ProductCartCheckoutPage.getViewAllsmartwatches()).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Smart Watches");
		   
		   /**************** click on load more link ************/
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(ProductCartCheckoutPage.getxfitlink()))
				   {
					   driver.findElement(ProductCartCheckoutPage.getxfitlink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getxfit1header()).getText(), "Noise X-Fit 1", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Xfit-1 Product");
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int count=values_constant.setXfit1Colors().size();
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   cm.waitMethod();
					  
					    if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								   try
								   {
									   expected =values_constant.setXfit1Colors().get(i);
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color not found");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Xfit-1 watch is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	@Test(description = "Verify Smart Watch Brio",priority=2)	
	   public void TC_verifySmartWatchBrio() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchesBrio",
					"Verify Smart Watch Brio");
		
		   
		   /**************** click on load more link ************/
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(ProductCartCheckoutPage.getbriolink()))
				   {
					   driver.findElement(ProductCartCheckoutPage.getbriolink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getbrioheader()).getText(), "ColorFit Brio", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Brio Product");
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int count=values_constant.setBrioColors().size();
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   cm.waitMethod();
					  
					    if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								   try
								   {
									   expected =values_constant.setBrioColors().get(i);
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color not found");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Brio watch is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	   
	   @Test(description = "Verify Smart Watch Qube O2",priority=3)	
	   public void TC_verifySmartWatchQube() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchQube",
					"Verify Smart Watch Qube O2");
		 
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		 
		   
		   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getqubelink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getqubelink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getqubeheader()).getText(), "ColorFit Qube O2", "Header Text not matched");
				   //ExtentReport.ExtentReportInfoLog("Redirected to brio");
				   ExtentReport.ExtentReportInfoLog("Redirected to Qube Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				  
				   //System.out.print(values_constant.setBrioColors().get(0));
				   int count=values_constant.setQubeColors().size();
				   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setQubeColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Qube watch is not present or clickable");
		 
	   }
	   
	   }
	   
	  
	   @Test(description = "Verify Smart Watch Pulse",priority=4)	
	   public void TC_verifySmartWatchPulse() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchPulse",
					"Verify Smart Watch Pulse");
		 
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   
		
		   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getpulselink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getpulselink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpulheader()).getText(), "ColorFit Pulse", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Pulse Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setPulseColors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setPulseColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Pulse watch is not present or clickable");
		 
	   }
	   
	   
	   }
	   
	   
	   @Test(description = "Verify Smart Watch Ultra",priority=5)	
	   public void TC_verifySmartWatchUtra() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchUtra",
					"Verify Smart Watch Ultra");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		 
		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getultralink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getultralink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getultraheader()).getText(), "ColorFit Ultra", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Ultra Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setUltraColors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				/*   WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setUltraColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Ultra watch is not present or clickable");
		 
	   }
	   
	  
		
	    }
	   
	  
	   @Test(description = "Verify Smart Watch pro3",priority=6)	
	   public void TC_verifySmartWatchpro3() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchpro3",
					"Verify Smart Watch pro3");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   

		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getpro3link()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getpro3link()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro3header()).getText(), "ColorFit Pro 3", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Pro3 Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setPro3Colors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setPro3Colors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Pro3 watch is not present or clickable");
		 
	   }
	   
	 
		
	    }
	   
	   @Test(description = "Verify Smart Watch active",priority=7)	
	   public void TC_verifySmartWatchactive() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchactive",
					"Verify Smart Watch active");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   
		 
		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getactivelink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getactivelink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getactiveheader()).getText(), "NoiseFit Active", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Noisefit Active Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setActiveColors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setActiveColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Active watch is not present or clickable");
		 
	   }
	   
	   
		
	    }
	   
	   
	   @Test(description = "Verify Smart Watch pro3 assist",priority=8)	
	   public void TC_verifySmartWatchpro3assist() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchpro3assist",
					"Verify Smart Watch pro3 assist");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   
		 
		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getpro3assistlink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getpro3assistlink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro3assistheader()).getText(), "ColorFit Pro 3 Assist", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Colorfit Pro3 assist Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setPro3AssistColors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setPro3AssistColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Colorfit Pro3 assist watch is not present or clickable");
		 
	   }
	   
	   
		
	    }
	   
	   @Test(description = "Verify Smart Watch Colorfit Pro2",priority=9)	
	   public void TC_verifySmartWatchcolorfitpro2() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchcolorfitpro2",
					"Verify Smart Watch Colorfit Pro2");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   
		 
		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getpro2link()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getpro2link()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro2header()).getText(), "ColorFit Pro 2", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to ColorFit Pro 2 Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setPro2Colors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setPro2Colors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("ColorFit Pro 2 watch is not present or clickable");
		 
	   }
	   
	   
		
	    }
	   
	   
	   @Test(description = "Verify Smart Watch Noisefit Endure",priority=10)	
	   public void TC_verifySmartWatchNoisefitEndure() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchNoisefitEndure",
					"Verify Smart Watch Noisefit Endure");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   
		 
		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getendurelink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getendurelink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getendureheader()).getText(), "NoiseFit Endure", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Noisefit Endure Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setEndureColors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setEndureColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Noisefit Endure watch is not present or clickable");
		 
	   }
	   
	   
		
	    }
	   
	   
	   @Test(description = "Verify Smart Watch Colorfit Nav",priority=11)	
	   public void TC_verifySmartWatchColorfitNav() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchColorfitNav",
					"Verify Smart Watch Colorfit Nav");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   
		 
		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getnavlink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getnavlink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getnavheader()).getText(), "ColorFit Nav", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Colorfit Nav Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setNavColors().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setNavColors().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Colorfit Nav is not present or clickable");
		 
	   }
	   
	   
		
	    }
	   
   
	   @Test(description = "Verify Smart Watch Colorfit Pro2 Oxy",priority=12)	
	   public void TC_verifySmartWatchColorfitPro2Oxy() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchColorfitPro2Oxy",
					"Verify Smart Watch ColorFit Pro 2 Oxy");
		   
		  
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			    	 System.out.print("Load More Link not found");
			 
		   }
		   
		   
		 
		   
	   try {
			   
			   if(cm.existsElement(SmartWatchesPage.getpro2oxylink()))
			   {
				   cm.waitMethod();
				   driver.findElement(SmartWatchesPage.getpro2oxylink()).click();
				   driver.navigate().refresh();    // This line is added as the page is getting blank now 
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro2oxyheader()).getText(), "ColorFit Pro 2 Oxy", "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Colorfit Pro2 Oxy Product");
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   cm.waitMethod();
				   int count=values_constant.setpro2oxycolor().size();
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("licount"+licount);
				   System.out.println("count"+count);
				  /* WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				   cm.scrolldowntoElement(qty);*/
				   
				   cm.waitMethod();
				  
				    if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							   try
							   {
								   expected =values_constant.setpro2oxycolor().get(i);
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								   
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifyme()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color not found");
									   break;
									   
								   }
								   
								  
								  
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			   }
			   driver.navigate().back();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("ColorFit Pro 2 Oxy is not present or clickable");
		 
	   }
	   
	   
		
	    }
	   
   
	
    
   
}
