package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.commonutilmethods;
import utility.values_constant;


public class SmartWatchesDynamic 
{
	String actual,expected;
	ArrayList <String> list;
	commonutilmethods cm;
	
	
	@Test(description = "Verify Colorfit pulse Grand")	
	   public void TC_verifyPulseGrand(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyPulseGrand",
					"Verify Colorfit pulse Grand");
		   System.out.println("name"+name);
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		 
		 try {
				 //  if(cm.existsElement(By.xpath(xpath)))
				   //{
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpulsegrandheader()).getText(), "ColorFit Pulse Grand", "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Pulse Grand Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
		   //}
		 }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse Grand watch is sold out");
			 
		   }
		 
		   
	   
	   }
	
	@Test(description = "Verify Colorfit Ultra Buzz")	
	   public void TC_verifyUltraBuzz(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUltraBuzz",
					"Verify Colorfit Ultra Buzz");
		   System.out.println("name"+name);
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		 
		 try {
				 //  if(cm.existsElement(By.xpath(xpath)))
				   //{
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getultrabuzzheader()).getText(), "Ultra Buzz", "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Ultra Buzz Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
		   //}
		 }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Ultra Buzz is sold out");
			 
		   }
		 
		   
	   
	   }
	
	 @Test(description = "Verify Colorfit Ultra2")	
	   public void TC_verifyColorfitUltra2(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitUltra2",
					"Verify Colorfit Ultra2");
		   System.out.println("name"+name);
		   

		   
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		  try {
				  // if(cm.existsElement(By.xpath(xpath)))
				   //{
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getultra2header()).getText(), "ColorFit Ultra 2", "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Ultra2 Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				  // }
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Ultra 2 watch is sold out");
			 
		   }
		 
		   
	   }
	 @Test(description = "Verify Smart Watch Colorfit Pro3 Alpha")	
	   public void TC_verifyColorfitPro3Alpha(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchColorfitPro3Alpha",
					"Verify Smart Watch Colorfit Pro3 Alpha");
		   System.out.println("name"+name);
		   
            
		   
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		
		  
			   try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					  // {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro3alphaheader()).getText(), "ColorFit Pro 3 Alpha", "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Pro3 Alpha Product");
						   
						 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
						   //cm.waitMethod();
						  
						   //System.out.print(values_constant.setBrioColors().get(0));
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarraynew(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
						   
						  // System.out.println("licount"+licount);
						  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
						  // cm.scrolldowntoElement(qty);
						   
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifyme()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			   }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Pro3 alpha watch is sold out");
				 
			   }
		   }
		  
		 
		   
	   
	 
	   @Test(description = "Verify Smart Watch Colorfit Caliber")	
	   public void TC_verifyColorfitCaliber(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitCaliber",
					"Verify Smart Watch Colorfit Caliber");
		   System.out.println("name"+name);

		   
		   
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		  try {
				 //  if(cm.existsElement(By.xpath(xpath)))
				   //{
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getcaliberheader()).getText(), "ColorFit Caliber", "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Caliber Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   //}
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Caliber watch is sold out");
			 
		   }
		 
		   
	   }
	   
	   @Test(description = "Verify Noisefit Evolve 2")	
	   public void TC_verifyNoisefitEvolve2(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisefitEvolve2",
					"Verify Noisefit Evolve 2");
		   System.out.println("name"+name);
		   

		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		  try {
				   //if(cm.existsElement(By.xpath(xpath)))
				   //{
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getevolve2header()).getText(), "NoiseFit Evolve 2", "Header Text not matched");
					   
					   System.out.println("Redirected to Noisefit Evolve2 Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   
				//   }
		  }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noisefit Evolve2 watch is sold out");
			 
		   }
		 
		   
	   }
	
	   @Test(description = "Verify Colorfit Pulse")	
	   public void TC_verifyColorfitPulse(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitPulse",
					"Verify Colorfit Pulse");
		   System.out.println("name"+name);
		   

		   
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		   try {
				  // if(cm.existsElement(By.xpath(xpath)))
				  // {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpulheader()).getText(), "ColorFit Pulse", "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Pulse Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				  // }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is sold out");
			 
		   }
		 
		   
	   } 
	   
	   @Test(description = "Verify Smart Watch Colorfit Icon Buzz")	
	   public void TC_verifySmartWatchColorfitIconBuzz(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchColorfitIconBuzz",
					"Verify Smart Watch Colorfit Icon Buzz");
		   System.out.println("name"+name);
		   

		   
		   //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name+"']/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   

		   
		   try {
				 //  if(cm.existsElement(By.xpath(xpath)))
				  // {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.geticonbuzzheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Icon Buzz Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				 //  }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Icon Buzz watch is sold out");
			 
		   }
		 
		   
	   }
	   
	   @Test(description = "Verify Colorfit Ultra")	
	   public void TC_verifyColorfitUltra(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitUltra",
					"Verify Colorfit Ultra");
		   System.out.println("name"+name);
		   

		   
		   String xpath="//span[text()='"+name+"']/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		   try {
				  // if(cm.existsElement(By.xpath(xpath)))
				 //  {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getultraheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Ultra Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				 //  }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Ultra watch is sold out");
			 
		   }
		 
		   
	   }
	   
	   
	   @Test(description = "Verify XFit-1 HRX Edition")	
	   public void TC_verifyXFit1(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyXFit1",
					"Verify XFit-1 HRX Edition");
		   System.out.println("name"+name);
		   

		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		  try {
				 //  if(cm.existsElement(By.xpath(xpath)))
				  // {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getxfit1header()).getText(), "X-Fit 1 (HRX Edition)", "Header Text not matched");
					   
					   System.out.println("Redirected to XFit-1 HRX Edition Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				  // }
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("XFit-1 watch is sold out");
			 
		   }
		 
		   
	   }
	   
	   
	   @Test(description = "Verify Colorfit Brio")	
	   public void TC_verifyColorfitBrio(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitBrio",
					"Verify Colorfit Brio");
		   System.out.println("name"+name);
		   

		   
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		   try {
				  // if(cm.existsElement(By.xpath(xpath)))
				   //{
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getbrioheader()).getText(), "ColorFit Brio", "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Brio Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				  // }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Brio watch is sold out");
			 
		   }
		 
		   
	   }
	   
	   
	   @Test(description = "Verify Noisefit Core Oxy")	
	   public void TC_verifyCoreOxy(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyCoreOxy",
					"Verify Noisefit Core Oxy");
		   System.out.println("name"+name);
		   

		   
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name1+"']/parent::a";
		   
		   /***** code added 20/07/2022 *********/
		   cm=new commonutilmethods(driver);
		   
		   try {
				   //if(cm.existsElement(By.xpath(xpath)))
				 //  {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getcoreoxyheader()).getText(), "NoiseFit Core Oxy", "Header Text not matched");
					   
					   System.out.println("Redirected to Noisefit Core Oxy Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				  // }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noisefit Core Oxy watch is sold out");
			 
		   }
		 
		   
	   }
	
	   
	   
	   
	     @Test(description = "Verify Colorfit Pro3 Assist")	
		   public void TC_verifyColorfitPro3Assist(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitPro3Assist",
						"Verify Colorfit Pro3 Assist");
			   System.out.println("name"+name);
			   
	
			   String xpath="//span[text()='"+name+"']/parent::a";
			   System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
			   
			   try {
					  // if(cm.existsElement(By.xpath(xpath)))
					 //  {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro3assistheader()).getText(), name, "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Pro3 Assist Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					//   }
			   }
					   
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Pro3 Assist watch is sold out");
				 
			   }
			 
			   
		   }
	     
	     
	     @Test(description = "Verify Champ Kids Smartband")	
		   public void TC_verifyChampKids(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyChampKids",
						"Verify Champ Kids Smartband");
			   System.out.println("name"+name);
			   

			   
			   	/***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getchampkidsheader()).getText(), "Noise Champ Kids smartband", "Header Text not matched");
						   
						   System.out.println("Redirected to Champ Kids Smartband Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Champ Kids watch is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Colorfit Pro 3")	
		   public void TC_verifyColorfitPro3(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitPro3",
						"Verify Colorfit Pro 3");
			   System.out.println("name"+name);
			   

			   
			   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro3header()).getText(), "ColorFit Pro 3", "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Pro 3 Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Pro 3 is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Colorfit Pro 2")	
		   public void TC_verifyColorfitPro2(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitPro2",
						"Verify Colorfit Pro 2");
			   System.out.println("name"+name);
			   

			   
			   String xpath="//span[text()='"+name+"']/parent::a";
			   System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro2header()).getText(), name, "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Pro 2 Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Pro 2 is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Noisefit Endure")	
		   public void TC_verifyNoisefitEndure(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisefitEndure",
						"Verify Noisefit Endure");
			   System.out.println("name"+name);
			   

			   
			   String xpath="//span[text()='"+name+"']/parent::a";
			   System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getendureheader()).getText(), name, "Header Text not matched");
						   
						   System.out.println("Redirected to Noisefit Endure Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Noisefit Endure is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Colorfit pro2 Oxy")	
		   public void TC_verifyColorfitPro2Oxy(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitPro2Oxy",
						"Verify Colorfit pro2 Oxy");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro2oxyheader()).getText(), "ColorFit Pro 2 Oxy", "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit pro2 Oxy Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit pro2 Oxy is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Noisefit active")	
		   public void TC_verifyNoisefitActive(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisefitActive",
						"Verify Noisefit active");
			   System.out.println("name"+name);
			   

			   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getactiveheader()).getText(), "NoiseFit Active", "Header Text not matched");
						   
						   System.out.println("Redirected to Noisefit active Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Noisefit active is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Noisefit agile")	
		   public void TC_verifyNoisefitAgile(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisefitAgile",
						"Verify Noisefit agile");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getagileheader()).getText(), "NoiseFit Agile", "Header Text not matched");
						   
						   System.out.println("Redirected to Noisefit agile Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Noisefit agile is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Colorfit Nav Plus")	
		   public void TC_verifyColorfitNavPlus(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitNavPlus",
						"Verify Colorfit Nav Plus");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getnavplusheader()).getText(), "ColorFit Nav+", "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Nav Plus Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Nav Plus is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Colorfit Beat")	
		   public void TC_verifyColorfitBeat(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitBeat",
						"Verify Colorfit Beat");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getbeatheader()).getText(), "ColorFit Beat", "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Beat Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Beat is sold out");
				 
			   }
			 
			   
		   }
	     
	     @Test(description = "Verify Colorfit Vision")	
		   public void TC_verifyColorfitVision(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitVision",
						"Verify Colorfit Vision");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getvisionheader()).getText(), name, "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Vision Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Vision is sold out");
				 
			   }
			 
			   
		   }
	     
	      @Test(description = "Verify Colorfit Nav")	
		   public void TC_verifyColorfitNav(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitNav",
						"Verify Colorfit Nav");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getnavheader()).getText(), name, "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Nav Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Nav is sold out");
				 
			   }
			 
			   
		   }
	      
	      @Test(description = "Verify Noisefit Buzz")	
		   public void TC_verifyNoisefitBuzz(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisefitBuzz",
						"Verify Noisefit Buzz");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getbuzzheader()).getText(), "NoiseFit Buzz", "Header Text not matched");
						   
						   System.out.println("Redirected to Noisefit Buzz Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Noisefit Buzz is sold out");
				 
			   }
			 
			   
		   }
	      
	      
	      @Test(description = "Verify Noise Excel")	
		   public void TC_verifyNoiseExcel(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoiseExcel",
						"Verify Noise Excel");
			   System.out.println("name"+name);
			   

			   
   /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getexcelheader()).getText(), "Noise Excel", "Header Text not matched");
						   
						   System.out.println("Redirected to Noise Excel Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Noise Excel is sold out");
				 
			   }
			 
			   
		   }
	      
	      
	      @Test(description = "Verify ColorFit Qube O2")	
		   public void TC_verifyColorfitQubeO2(WebDriver driver,String name) throws InterruptedException
		   {
			   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitQubeO2",
						"Verify ColorFit Qube O2");
			   System.out.println("name"+name);
			   

			   
			   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
			   System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
			   
			  try {
					 //  if(cm.existsElement(By.xpath(xpath)))
					//   {
						   driver.findElement(By.xpath(xpath)).click();
						   
						   cm.waitMethod();
						   Assert.assertEquals(driver.findElement(SmartWatchesPage.getbuzzheader()).getText(), name, "Header Text not matched");
						   
						   System.out.println("Redirected to Colorfit Qube O2 Product");
						   
						 
						   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
						   System.out.println("list count:"+licount);
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   String colortext;
						   list = new ArrayList<String>();
						   list=cm.valueaddinarray(driver, licount);
						   
						   System.out.println("list size:"+list.size());
						   
						   System.out.println("list element:"+list.get(0));
						   
						   /******************  Fetch the color text and insert in array      *******************/
						   
						  
						   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
						  
						   int count=list.size();
						   
						   cm.waitMethod();
						  
						   if(licount==count)
							   {
								   for(int i=0;i<licount;i++)
								   {
									 
									   try
									   {
										  expected =list.get(i);
										 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
										   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
										   Assert.assertEquals(actual,expected);
										  
										   System.out.println("color matched:"+actual);
										   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
										 
										   try
										   {
											   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
											   {
												   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
												   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
												   cm.waitMethod();
												   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
												   cm.waitMethod();
												   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
											   }
											
											
										   }
										   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
											   
										   }
										  
										   try
										   {
											   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
											   ExtentReport.ExtentReportInfoLog("Next color is clicked");
											   cm.waitMethod();
										   }
											   
										   catch(Exception ex)
										   {
											   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
											   break;
											   
										   }
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
										  
									   }
									 
									  
								   }
							}
						    else
						    {
						    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
						    }
						   
						   
						   
					   
		   
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
					   cm.waitMethod();
					   list.clear();
					 //  }
			  }
			   
			    catch(Exception e)
			   {
			    
			    	ExtentReport.ExtentReportInfoLog("Colorfit Qube O2 is sold out");
				 
			   }
			 
			   
		   }

}