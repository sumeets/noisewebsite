package GoNoise;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class test {
	
	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.chrome.driver","./resources/chromedriver.exe");
		WebDriver driver = new ChromeDriver();
	     driver.get("https://www.gonoise.com/collections/bluetooth-earphones");
		driver.manage().window().maximize();
		
		System.out.println(driver.findElement(By.xpath("//span[text()='Tune Sport 2']")).getText());
	}
	
}
