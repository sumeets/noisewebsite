package GoNoise;

import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class trackyourorder extends Apache_POI_TC
{
	
   @Test(description = "Verify Track Your Order",priority=1)	
   public void TC_verifyClickTrackYourOrder() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickTrackYourOrder",
				"Verify Track Your Order");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	   cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
			   driver.findElement(NoiseHomePage.gettrackorderlink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Track Your Order Link is Clicked");
			   
			   
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Track Your Order Page is opened");
		   
		   
			   /*************************** valid order 1936834 and 8964001354 *************/
		   
			  driver.findElement(By.name("order_id")).sendKeys("1936834");
			  
			  ExtentReport.ExtentReportInfoLog("Order id is entered");
			   
			  cm.waitMethod();
			   
              
			  driver.findElement(trackyourorderpage.getBtnTrackOrder()).click();
			  
			  ExtentReport.ExtentReportInfoLog("Track Order button is clicked");
			  
			  ExtentReport.ExtentReportInfoLog("It is redirected to shiprocket page");
			   
		      Thread.sleep(5000);
		      
		      String orderId="1936834";
		      
		      driver.findElement(By.name("track_id")).sendKeys(orderId);
		      
		      cm.waitMethod();
		      
		      driver.findElement(trackyourorderpage.getBtnSubmit()).click();
		      
		      Thread.sleep(5000);
		      
		    //  cm.scrolldown();
		      
		     System.out.println(driver.findElements(By.xpath("//strong[@id='error_text']")).size());
		     int size=driver.findElements(By.xpath("//strong[@id='error_text']")).size();
		     
		     /********* If error message exists **********/
		     if(size>0)
		     {
		    	 String errmsg=driver.findElement(By.xpath("//strong[@id='error_text']")).getText();
			      
			      Thread.sleep(3000);
			      
			      ExtentReport.ExtentReportErrorLog(errmsg);
		     }
		     else
		     {
		    	 Assert.assertEquals(driver.findElement(By.xpath("(//span[contains(@class,'pull-right')])[1]")).getText(),orderId);
		    	 
		    	 ExtentReport.ExtentReportInfoLog("Invoice page is opened with Order Id"+orderId);
		     }
		 
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Track Order Link is not Found nor clicked");
	   }
	  
	 
   }


  
}
