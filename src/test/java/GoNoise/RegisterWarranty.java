package GoNoise;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.Registerwarrantypage;
import utility.ExtentReport;


public class RegisterWarranty extends Apache_POI_TC
{
	
   @Test(description = "Verify Register Warranty Entry",priority=1)	
   public void TC_verifyClickRegisterWarrantyLink() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickRegisterWarrantyLink",
				"Verify Register Warranty Entry");
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
		   driver.findElement(NoiseHomePage.getregisterwarranty()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Register Warranty is Clicked");
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("Register Warranty page is opened");
		   
		   EnterForm();
		   
		   
		   
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Register Warranty is not Clicked");
	   }
	  
	   
	   
	  
	 
   }
   
   

private void EnterForm() throws InterruptedException {
	
	
	   driver.findElement(Registerwarrantypage.getFullName()).sendKeys("testabcd");
	   
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Full Name is entered");
	   
	   driver.findElement(Registerwarrantypage.getEmail()).sendKeys("test@test.com");
	   
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Email is entered");
	   
	   driver.findElement(Registerwarrantypage.getmobile()).sendKeys("8240724302");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Mobile is entered");
	   driver.findElement(Registerwarrantypage.getdistributer()).click();
	   
	   cm.waitMethod();
	   
	   cm.doSelectByVisibleText(Registerwarrantypage.getdistributer(),"Gonoise");
	   
	   cm.waitMethod();
	   
	   
	   ExtentReport.ExtentReportInfoLog("Distributor is clicked and value selected");
	   
	   driver.findElement(Registerwarrantypage.getsearchproduct()).click();
	   
	   Thread.sleep(5000);
	   
	  /* cm.waitMethod();
	   
	   Actions builder = new Actions(driver);        
	   builder.SendKeys(Keys.ENTER);*/
	   
	   ExtentReport.ExtentReportInfoLog("Select your product is clicked");
	   
	   driver.findElement(Registerwarrantypage.getsearchproduct()).sendKeys(Keys.DOWN);
	   cm.waitMethod();
	   
	   driver.findElement(Registerwarrantypage.getsearchproduct()).sendKeys(Keys.ENTER);
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Product is selected");
	   
       driver.findElement(Registerwarrantypage.getordernumber()).sendKeys("123456");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Order Number is entered");
	   
       driver.findElement(Registerwarrantypage.getserialnumber()).sendKeys("7896578");
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Serial Number is entered");
	   
       driver.findElement(Registerwarrantypage.getregisterbutton()).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Register button is clicked");
	   
	   
	   
	   /************************ OTP ************************/
	   
	   Scanner user= new Scanner(System.in);
	   System.out.println("Enter the OTP: ");
	   String usern=user.nextLine();
	   cm.waitMethod();
	   
	   
	   driver.findElement(Registerwarrantypage.getOTPTxtbox()).sendKeys(usern);
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("OTP is entered");
	   
	   driver.findElement(Registerwarrantypage.getverifyOTPButton()).click();
	   
	   Thread.sleep(5000);
	   
	   ExtentReport.ExtentReportInfoLog("OTP is verified");
	   
	   ExtentReport.ExtentReportInfoLog("Warranty is registered successfully");
	   
	   
	   /*********************** OTP **************************/
	   
	   
	   
	   
}
   
 
   
  
}
