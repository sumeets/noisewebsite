package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.Wirelessearbudspage;
import utility.ExtentReport;
import utility.commonutilmethods;
import utility.values_constant;


public class WirelessearbudsDynamic extends Apache_POI_TC
{
	String actual,expected;
	ArrayList <String> list;
	
	
	
	@Test(description = "Verify Buds VS103")	
	   public void TC_verifyBudsVS103(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS103",
					"Verify Buds VS103");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
           /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getVS103header()).getText(), "Buds VS103", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS103 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   WebElement producttitle= driver.findElement(Wirelessearbudspage.getbudsproducttitle());
						cm.scrolldowntoElement(producttitle);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(Wirelessearbudspage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(Wirelessearbudspage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(Wirelessearbudspage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS103 is sold out");
			 
		   }
		   
		  
	   }
	
	
	
	@Test(description = "Verify Air Buds Mini",priority=2)	
	   public void TC_verifyAirBudsMini() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirBudsMini",
					"Verify Air Buds Mini");
		   
		
		 
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getairbudminilink()))
				   {
					   driver.findElement(Wirelessearbudspage.getairbudminilink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsminiheader()).getText(), "Air Buds Mini", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Air Buds Mini Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Air Buds Mini is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	@Test(description = "Verify Air Buds Pro")	
	   public void TC_verifyAirBudsPro(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirBudsPro",
					"Verify Air Buds Pro");
		   
		
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   /***** code added 20/07/2022 *********/
		   
		   	  String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getairbudminilink()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsproheader()).getText(), "Air Buds Pro", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Air Buds Pro Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Air Buds Pro is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	@Test(description = "Verify Buds VS303")	
	   public void TC_verifyBudsVS303(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS303",
					"Verify Buds VS303");
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
           /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		   
		   try {
				 //  if(cm.existsElement(Wirelessearbudspage.getairbudspluslink()))
				   //{
			        driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getvs303header()).getText(), "Buds VS303", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS303 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				 //  }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS303 is sold out");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	
	
	@Test(description = "Verify Airbuds Plus")	
	   public void TC_verifyAirbudsplus(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirbudsplus",
					"Verify Airbuds Plus");
		   
		
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
		 
		   
		   try {
				   
			          driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsplusheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Air Buds Plus Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Airbuds Plus is sold out");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	   @Test(description = "Verify Buds VS303",priority=5)	
	   public void TC_verifyVS303() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyVS303",
					"Verify Buds VS303");
		   
		
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getVS303link()))
				   {
					   driver.findElement(Wirelessearbudspage.getVS303link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getvs303header()).getText(), "Buds VS303", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS303 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS303 is not present or clickable");
			 
		   }
		   
		  
	   }
	   
	   
	   @Test(description = "Verify Buds VS102")	
	   public void TC_verifyVS102(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyVS102",
					"Verify Buds VS102");
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name+"']/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS102link()))
				   //{
					   driver.findElement(Wirelessearbudspage.getVS102link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getVS102header()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS102 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS102 is sold out");
			 
		   }
		   
		  
	   }
	   
  @Test(description = "Verify Buds VS201")	
	   public void TC_verifyBudsVS201(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS201",
					"Verify Buds VS201");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name+"']/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getbudsVS201header()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS201 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS201 is sold out");
			 
		   }
		
		   
	   }   
  
  
  
  @Test(description = "Verify Shots Neo 2")	
  public void TC_verifyShotsNeo2(WebDriver driver,String name) throws InterruptedException
  {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyShotsNeo2",
				"Verify Shots Neo 2");
	    
	   System.out.println("name"+name);
		 //span[contains(text(),'ColorFit Pulse Grand')]
	   String xpath="//span[text()='"+name+"']/parent::a";
	  // System.out.println("xpath:"+xpath);
	   cm=new commonutilmethods(driver);
	 
	   
	 
	   
	   
	   try {
			  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
			   //{
		   driver.findElement(By.xpath(xpath)).click();
				   
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getshotsneo2header()).getText(), name, "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Shots Neo2 Product");
				   
				 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   //cm.waitMethod();
				  
				   //System.out.print(values_constant.setBrioColors().get(0));
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("list count:"+licount);
				   
				   /******************  Fetch the color text and insert in array      *******************/
			       String colortext;
				   list = new ArrayList<String>();
				   list=cm.valueaddinarray(driver, licount);
				   
				   System.out.println("list size:"+list.size());
				   
				   System.out.println("list element:"+list.get(0));
				   
				   /******************  Fetch the color text and insert in array      *******************/
				   
				   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
				   
				  // System.out.println("licount"+licount);
				  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				  // cm.scrolldowntoElement(qty);
				   
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				  
				   int count=list.size();
				   
				   cm.waitMethod();
				  
				   if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							  
							   try
							   {
								  expected =list.get(i);
								//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								  
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								   cm.waitMethod();
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
									  break;
									   
								   }
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			  // }
			   
			   driver.navigate().back();
			   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
			   cm.waitMethod();
			   list.clear();
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Shots Neo 2 is sold out");
		 
	   }
	
	   
  } 
  
  @Test(description = "Verify Buds Solo")	
  public void TC_verifyBudsSolo(WebDriver driver,String name) throws InterruptedException
  {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsSolo",
				"Verify Buds Solo");
	    
	   System.out.println("name"+name);
		 //span[contains(text(),'ColorFit Pulse Grand')]
	   String xpath="//span[text()='"+name+"']/parent::a";
	  // System.out.println("xpath:"+xpath);
	   cm=new commonutilmethods(driver);
	 
	   
	 
	   
	   
	   try {
			  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
			   //{
		   driver.findElement(By.xpath(xpath)).click();
				   
				   cm.waitMethod();
				   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getbudssoloheader()).getText(), name, "Header Text not matched");
				   
				   ExtentReport.ExtentReportInfoLog("Redirected to Buds Solo Product");
				   
				 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				   //cm.waitMethod();
				  
				   //System.out.print(values_constant.setBrioColors().get(0));
				   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
				   System.out.println("list count:"+licount);
				   
				   /******************  Fetch the color text and insert in array      *******************/
			       String colortext;
				   list = new ArrayList<String>();
				   list=cm.valueaddinarray(driver, licount);
				   
				   System.out.println("list size:"+list.size());
				   
				   System.out.println("list element:"+list.get(0));
				   
				   /******************  Fetch the color text and insert in array      *******************/
				   
				   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
				   
				  // System.out.println("licount"+licount);
				  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
				  // cm.scrolldowntoElement(qty);
				   
				   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
				  
				   int count=list.size();
				   
				   cm.waitMethod();
				  
				   if(licount==count)
					   {
						   for(int i=0;i<licount;i++)
						   {
							  
							   try
							   {
								  expected =list.get(i);
								//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
								   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
								   Assert.assertEquals(actual,expected);
								  
								   System.out.println("color matched:"+actual);
								   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
								   cm.waitMethod();
								 
								   try
								   {
									   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
									   {
										   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
										   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
										   cm.waitMethod();
										   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
										   cm.waitMethod();
										   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
									   }
									
									
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
									   
								   }
								  
								   try
								   {
									   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
									   ExtentReport.ExtentReportInfoLog("Next color is clicked");
									   cm.waitMethod();
								   }
									   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
									  break;
									   
								   }
							   }
							   
							   catch(Exception ex)
							   {
								   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
								   break;
							   }
							 
							  
						   }
					   }
				    else
				    {
				    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
				    }
				   
				   
				   
			  // }
			   
			   driver.navigate().back();
			   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
			   cm.waitMethod();
			   list.clear();
	   }
	    catch(Exception e)
	   {
	    
	    	ExtentReport.ExtentReportInfoLog("Buds Solo is sold out");
		 
	   }
	
	   
  } 
  
  
  
	
  
	@Test(description = "Verify Buds VS202")	
	   public void TC_verifyBudsVS202(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS202",
					"Verify Buds VS202");
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
 /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		  
		 
		   
		   try {
				 //  if(cm.existsElement(Wirelessearbudspage.getVS202link()))
				  // {
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   
					   WebElement producttitle= driver.findElement(Wirelessearbudspage.getbudsproducttitle());
					   cm.scrolldowntoElement(producttitle);
					   
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getbudsproducttitle()).getText(), "Buds VS202", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS202 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(Wirelessearbudspage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(Wirelessearbudspage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(Wirelessearbudspage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
					   cm.waitMethod();
					   list.clear();
					   
				 //  }
				   
				  
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS202 is sold out");
			 
		   }
		   
		
		   
	   }
	
	   @Test(description = "Verify Buds VS104")	
	   public void TC_verifyBudsVS104(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS104",
					"Verify Buds VS104");
		   
		   System.out.println("name"+name);
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			  System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
		  
		 
		   
		   try {
				 //  if(cm.existsElement(Wirelessearbudspage.getVS202link()))
				  // {
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   
					   WebElement producttitle= driver.findElement(Wirelessearbudspage.getbudsproducttitle());
					   cm.scrolldowntoElement(producttitle);
					   
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getbudsproducttitle()).getText(), "Buds VS104", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS104 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(Wirelessearbudspage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(Wirelessearbudspage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(Wirelessearbudspage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
					   cm.waitMethod();
					   list.clear();
					   
				 //  }
				   
				  
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS104 is sold out");
			 
		   }
		   
		
		   
	   }
	
	   @Test(description = "Verify Buds Ace")	
	   public void TC_verifyBudsAce(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsAce",
					"Verify Buds Ace");
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
             /***** code added 20/07/2022 *********/
		   
		       String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   
			   cm=new commonutilmethods(driver);
		 
		   
		   
		   try {
				 //  if(cm.existsElement(Wirelessearbudspage.getairbudspluslink()))
				   //{
			        driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getBudsAceheader()).getText(), "Buds Ace", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds Ace Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				 //  }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds Ace is sold out");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	@Test(description = "Verify Noise Beads")	
	  public void TC_verifyNoiseBeads(WebDriver driver,String name) throws InterruptedException
	  {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoiseBeads",
					"Verify Noise Beads");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name+"']/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getnoisebeadsheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Noise Beads Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noise Beads is sold out");
			 
		   }
		
		   
	  } 
	
	@Test(description = "Verify Airbuds Mini")	
	  public void TC_verifyAirbudsMini(WebDriver driver,String name) throws InterruptedException
	  {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirbudsMini",
					"Verify Airbuds Mini");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name+"']/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsminiheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Airbuds Mini Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Airbuds Mini is sold out");
			 
		   }
		
		   
	  } 
	
	
	@Test(description = "Verify Airbuds")	
	  public void TC_verifyAirbuds(WebDriver driver,String name) throws InterruptedException
	  {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirbuds",
					"Verify Airbuds");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		 
		   String xpath="//span[text()='"+name+"']/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Airbuds Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Airbuds is sold out");
			 
		   }
		
		   
	  } 
	
	@Test(description = "Verify Shots Rush")	
	  public void TC_verifyShotsRush(WebDriver driver,String name) throws InterruptedException
	  {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyShotsRush",
					"Verify Shots Rush");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name+"']/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getshotsrushheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Shots Rush Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Shots Rush is sold out");
			 
		   }
		
		   
	  } 
	
	
	
	
	
	@Test(description = "Verify Airbuds Nano")	
	  public void TC_verifyAirbudsNano(WebDriver driver,String name) throws InterruptedException
	  {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirbudsNano",
					"Verify Airbuds Nano");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsnanoheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Airbuds Nano Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Airbuds Nano is sold out");
			 
		   }
		
		   
	  } 

	
	@Test(description = "Verify Buds Smart")	
	  public void TC_verifyBudsSmart(WebDriver driver,String name) throws InterruptedException
	  {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsSmart",
					"Verify Buds Smart");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
             /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getBudsSmartheader()).getText(), "Buds Smart", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds Smart Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds Smart is sold out");
			 
		   }
		
		   
	  } 
	
	
	@Test(description = "Verify Buds Prima")	
	  public void TC_verifyBudsPrima(WebDriver driver,String name) throws InterruptedException
	  {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsPrima",
					"Verify Buds Prima");
		    
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[text()='"+name+"']";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getBudsSmartheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds Smart Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds Smart is sold out");
			 
		   }
		
		   
	  } 

	
}
