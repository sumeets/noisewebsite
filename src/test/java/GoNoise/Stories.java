package GoNoise;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class Stories extends Apache_POI_TC
{
	
   @Test(description = "Verify Stories",priority=1)	
   public void TC_verifyStories() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyStories",
				"Verify Stories");
	   
	   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS); 
	   
	   /****************** Click on not now in Popup *****************/
	   
	   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
	  
	  
	  /****************** Click on not now in Popup *****************/
	   
	  
	   try {
		   
		       driver.findElement(NoiseHomePage.getstorieslink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Stories Link is Clicked and redirected to Stories Page");
			   
			   
			//   cm.waitMethod();
			   
			   driver.navigate().to("https://www.gonoise.com/blogs/posts");
			   
			//  Thread.sleep(10000);
			   
			  ExtentReport.ExtentReportInfoLog("Redirected to Posts Page");
			   
			   cm.scrolldownone();
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Scroll down to below");
			   cm.scrollup();
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Scroll down to Up");
			   
			   driver.navigate().back();
			   
			//   Thread.sleep(10000);
			   
			   ExtentReport.ExtentReportInfoLog("Go back to stories Page");
		 
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Stories Link is not Found nor clicked");
	   }
	  
	 
   }


  
}
