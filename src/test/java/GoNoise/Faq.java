package GoNoise;

import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import utility.ExtentReport;


public class Faq extends Apache_POI_TC
{
	
   @Test(description = "Verify FAQ Click",priority=1)	
   public void TC_verifyClickFAQ() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickFAQ",
				"Verify FAQ Click");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	   cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
		   driver.findElement(NoiseHomePage.getfaqlink()).click();
		   
		   ExtentReport.ExtentReportInfoLog("FAQ Link is Clicked");
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("FAQ Page is opened");
		   
		   try
		   {
			   cm.scrolldowntoElement(driver.findElement(By.xpath("//h3[normalize-space()='Have a question? Ask us']")));
			   
			  // cm.scrolldown();
			   
			   driver.findElement(By.xpath("(//a[contains(text(),'Contact Us')])[1]")).click();
			   
			   Thread.sleep(10000);
			   
			   ExtentReport.ExtentReportInfoLog("Contact Us button is clicked and redirected to Contact Us Page");
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Contact Us is not found nor clicked");
		   }
		   
		   
		  
		   
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("FAQ Link is not Clicked");
	   }
	  
	 
   }
   
  
}
