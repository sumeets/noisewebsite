package GoNoise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.AccountLoginpage;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.Wirelessearbudspage;
import utility.ExtentReport;
import utility.GetContentFromExcelSheets;
import utility.commonutilmethods;


public class ProductCartCheckout extends Apache_POI_TC
{
	int count=3;
	String email,firstname,lastname,phone,address,otp,username,pwd;

	
	BufferedReader br;
	
	Scanner obj;
	
	
	
   @Test(description = "Verify Product Checkout Flow")	
   public void verifyProductCheckout() throws InterruptedException, IOException
   {
	   
	  
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("verifyProductCheckout",
				"Verify Product Checkout Flow");
	   
	   
	   
	   /****************** Click on not now in Popup *****************/
	   
	   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
	  
	  
	  /****************** Click on not now in Popup *****************/
	  
	  WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
	   cm.hoverElement(mainMenu);
	   
	   cm.waitMethod();
	   
	   
	   driver.findElement(ProductCartCheckoutPage.getViewAllEarBuds()).click();
	   
	   cm.waitMethod();
	   
	   System.out.println("View all Link is Clicked");
	   
	  
	   
	 /* WebElement airbudsplus=driver.findElement(ProductCartCheckoutPage.getairbudsplus());
	   cm.scrolldowntoElement(airbudsplus);
	   
	   System.out.println("Scroll to the Airbuds+");*/
	   
	   cm.scrolldownone();
	   cm.waitMethod();
	   cm.scrollup();
	   cm.waitMethod();
	   
	   try
	   {
		   if(cm.existsElement(Wirelessearbudspage.getVS303linknew()))
		   {
			   driver.findElement(Wirelessearbudspage.getVS303linknew()).click();
			   
			   cm.waitMethod();
			   
			   
			   try
			   {
				   driver.findElement(ProductCartCheckoutPage.getaddtobag()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Add to Bag is clicked");
			   }
			   catch(Exception e)
			   {
			    
				   driver.findElement(ProductCartCheckoutPage.getbuynow()).click();
				   ExtentReport.ExtentReportInfoLog("Buy Now is clicked");
				 
			   }
			  
			   cm.waitMethod();
			   
			   
			   driver.navigate().to("https://www.gonoise.com/cart");
			   
			   ExtentReport.ExtentReportInfoLog("Navigated to Cart Page");
			  cm.waitMethod();
			   
			   /******************** Increase the qty of product by + icon ***********/
			   
			  for(int i=1;i<count;i++)
			   {
				 
				 try
				 {
					 driver.findElement(ProductCartCheckoutPage.getQtyPlusbutton()).click();
					   
					  Thread.sleep(3000);
				 }
				 
				  catch(Exception e)
				   {
					  ExtentReport.ExtentReportInfoLog("Quantity is not increased");
				   }
				 
				
				  
			   }
			   
			  /*WebElement paycard=driver.findElement(ProductCartCheckoutPage.getpaycard());
			   cm.scrolldowntoElement(paycard);*/
			   
			   
			   /******************** Increase the qty of product by + icon ***********/
			  
			  /******************** apply coupon *****************/
			  
		/*	  try
				 {
				 Thread.sleep(5000);
				 
				    System.out.println("coupon applied....");
				  
					 driver.findElement(ProductCartCheckoutPage.getapplycoupon()).click();
					   
					 cm.waitMethod();
					  
					  driver.findElement(ProductCartCheckoutPage.getcoupontxtbox()).sendKeys("gosmart");
					  
					  cm.waitMethod();
					  
                      driver.findElement(ProductCartCheckoutPage.getbtnApply()).click();
					  
					  cm.waitMethod();
					  
					  driver.findElement(By.xpath("//span[@class='popup-close ']")).click();
					  
					  cm.waitMethod();
					  
					  ExtentReport.ExtentReportInfoLog("Coupon is applied");
					  
					  
				 }
				 
				  catch(Exception e)
				   {
					  System.out.println("coupon not applied");
				   }*/
			  
			  
			  
			  /******************** apply coupon *****************/
			  
			  
			  
				  driver.findElement(ProductCartCheckoutPage.getpaycard()).click();
				  ExtentReport.ExtentReportInfoLog("Pay Via Card/Wallet is clicked and redirected to Checkout Page");
				  cm.waitMethod();
				  
				   fillCheckoutForm();
			 
			  
			 
			 
		   }
	   }
	   
	  
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("The product VS303 is not found");
	   }
	   
	  
   }
   
  	
   public void fillCheckoutForm() throws InterruptedException, IOException
   {
      
	   
       List<String> checkoutdata=new ArrayList<String>();
	   
       checkoutdata=GetContentFromExcelSheets.readData("excelFileContents.xlsx",1);
	   
	   System.out.println(checkoutdata.size());

	    email=checkoutdata.get(0);
	    firstname=checkoutdata.get(1);
	    lastname=checkoutdata.get(2);
	    phone=checkoutdata.get(3);
	    address=checkoutdata.get(4);
	   
	   
	   driver.findElement(ProductCartCheckoutPage.getcontactEmail()).sendKeys("abcd@gmail.com");
	   
	   cm.waitMethod();
	   
	   driver.findElement(By.id("checkout_shipping_address_first_name")).sendKeys("abcd");
	   
	   cm.waitMethod();
	   
	   driver.findElement(By.id("checkout_shipping_address_last_name")).sendKeys("ddd");
	   
       cm.waitMethod();
	   
	   driver.findElement(By.id("checkout_shipping_address_phone_clone")).sendKeys("8240724302");
	   
	   cm.waitMethod();
   
	   driver.findElement(By.id("checkout_shipping_address_address1")).sendKeys("kishangarh,vasantkunj,new delhi");
	   
	   cm.waitMethod();
	   
	   driver.findElement(By.id("zipcode")).sendKeys("110090");
	   
       Thread.sleep(5000);
	   
       driver.findElement(By.id("checkout_shipping_address_city")).click();
       
       cm.waitMethod();
       
	  /* driver.findElement(By.id("checkout_shipping_address_city")).sendKeys("Delhi");
	   
	   cm.waitMethod();*/
	   
	  driver.findElement(ProductCartCheckoutPage.getConfirmButton()).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Redirected to Payment Page");
	   
	   
	   /********** apply discount code ********/
	   
      /* driver.findElement(By.id("checkout_reduction_code")).sendKeys("gosmart");
	   
	   cm.waitMethod();
	
	   
	   driver.findElement(ProductCartCheckoutPage.getapplybutton()).click();
	   
	   Thread.sleep(10000);*/
	   
	   /********** apply discount code ********/
	   
	   /********** remove discount code ********/
	   
	   try
	   {
		   driver.findElement(By.xpath("//div[@class='order-summary__section order-summary__section--discount']//div[@class='tags-list']//button[@type='submit']//*[name()='svg']")).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("Coupon is removed");
		   
	   }
	   
	   catch(Exception e)
	   {
		  System.out.println("coupon removed skipped");
	   }
	   
	  
	   /********** remove discount code ********/
	   
	   driver.findElement(By.xpath("(//input[@name='checkout[payment_gateway]'])[2]")).click();
	   
	   ExtentReport.ExtentReportInfoLog("Payment method COD is selected");
	   
	   cm.waitMethod();
	   
	   /****************Otp *******************/
	    
     /*  Scanner otpInput = new Scanner(System.in);
	    
	    System.out.println("Enter Otp");
	    otp=otpInput.nextLine();
	    System.out.println(otp);
	    cm.waitMethod();*/
	    
	 // It will return the parent window name as a String
/*	    String parent=driver.getWindowHandle();

	    Set<String>s=driver.getWindowHandles();
	    
	 // Now iterate using Iterator
	    Iterator<String> I1= s.iterator();

	    while(I1.hasNext())
	    {

	    String child_window=I1.next();


	    if(!parent.equals(child_window))
	    {
		    driver.switchTo().window(child_window);
	
		    //System.out.println(driver.switchTo().window(child_window).getTitle());
		    
		    driver.findElement(By.xpath("//div[@class='checkout-otp-popup']//div[@class='otp-input-box']/input[@id='votp']")).sendKeys(otp);
		    
		    Thread.sleep(3000);
	
		    driver.close();
	    }

	    }*/
	    
	    
	    
	  /* driver.findElement(By.xpath("//div[@class='checkout-otp-popup']//div[@class='otp-input-box']/input[@id='votp']")).click();
	   Thread.sleep(5000);
	    driver.findElement(By.xpath("//div[@class='checkout-otp-popup']//div[@class='otp-input-box']/input[@id='votp']")).sendKeys(otp);
	    cm.waitMethod();*/
	    //otpInput.close();
	   
	 /*  BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	   System.out.println("Enter Otp");
	   otp=br.readLine();
	   System.out.println("Entered OTP is:"+otp);*/
	   
	  /*   otp="1234";
	    
	    driver.findElement(By.id("votp")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.id("votp")).sendKeys(otp);
	    Thread.sleep(5000);
	  //  cm.waitMethod();
	    ExtentReport.ExtentReportInfoLog("Otp Entered");*/
	    
	    
	    /****************Otp *******************/
	   
	   
	  
   }
	  
  
}
