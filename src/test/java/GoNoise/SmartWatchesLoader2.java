package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.commonutilmethods;
import utility.values_constant;


public class SmartWatchesLoader2 
{
	String actual,expected;
	ArrayList <String> list;
	commonutilmethods cm;
	
	
	@Test(description = "Verify Colorfit pulse Grand")	
	   public void TC_verifyPulseGrand(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyPulseGrand",
					"Verify Colorfit pulse Grand");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		 //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		  try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpulsegrandheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Pulse Grand Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse Grand watch is not present or clickable");
			 
		   }
		 
		   
	   }
	
	
     @Test(description = "Verify Smart Watch Colorfit Pro3 Alpha")	
	   public void TC_verifySmartWatchColorfitPro3Alpha(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySmartWatchColorfitPro3Alpha",
					"Verify Smart Watch Colorfit Pro3 Alpha");
		   System.out.println("name"+name);
		   
 /********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   //span[contains(text(),'ColorFit Pulse Grand')]
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   

		   
		   try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro3alphaheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Pro3 Alpha Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
	
     @Test(description = "Verify Smart Watch Colorfit Caliber")	
	   public void TC_verifyColorfitCaliber(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitCaliber",
					"Verify Smart Watch Colorfit Caliber");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		  try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getcaliberheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Caliber Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify Noisefit Evolve 2")	
	   public void TC_verifyNoisefitEvolve2(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisefitEvolve2",
					"Verify Noisefit Evolve 2");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		  // System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		  try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getevolve2header()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Noisefit Evolve2 Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   
				   }
		  }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noisefit Evolve2 watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify Colorfit Pulse")	
	   public void TC_verifyColorfitPulse(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitPulse",
					"Verify Colorfit Pulse");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="(//span[contains(text(),'"+name+"')]/parent::a)[2]";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		   try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpulheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Pulse Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify Colorfit Ultra2")	
	   public void TC_verifyColorfitUltra2(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitUltra2",
					"Verify Colorfit Ultra2");
		   System.out.println("name"+name);
		   

		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		  try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getultra2header()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Ultra2 Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
	
     @Test(description = "Verify Colorfit Ultra")	
	   public void TC_verifyColorfitUltra(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitUltra",
					"Verify Colorfit Ultra");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="(//span[contains(text(),'"+name+"')]/parent::a)[2]";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		   try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getultraheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Ultra Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify XFit-1 HRX Edition")	
	   public void TC_verifyXFit1(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyXFit1",
					"Verify XFit-1 HRX Edition");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		  try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getxfit1header()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to XFit-1 HRX Edition Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify Noisefit Core Oxy")	
	   public void TC_verifyCoreOxy(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyCoreOxy",
					"Verify Noisefit Core Oxy");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		   try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getcoreoxyheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Noisefit Core Oxy Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify Noisefit Pro3 Assist")	
	   public void TC_verifyNoisefitPro3Assist(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoisefitPro3Assist",
					"Verify Noisefit Pro3 Assist");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		   try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getpro3assistheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Noisefit Pro3 Assist Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		   }
				   
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify Champ Kids Smartband")	
	   public void TC_verifyChampKids(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyChampKids",
					"Verify Champ Kids Smartband");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		  try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getchampkidsheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Champ Kids Smartband Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		  }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     @Test(description = "Verify Colorfit Qube O2")	
	   public void TC_verifyColorfitQubeO2(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitQubeO2",
					"Verify Colorfit Qube O2");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		   try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getqubeheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Qube O2 Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }
     
     
     @Test(description = "Verify Colorfit Brio")	
	   public void TC_verifyColorfitBrio(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyColorfitBrio",
					"Verify Colorfit Brio");
		   System.out.println("name"+name);
		   
/********************** scroll down and Load all products ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		   String xpath="//span[contains(text(),'"+name+"')]/parent::a";
		   System.out.println("xpath:"+xpath);
		   cm=new commonutilmethods(driver);
		   
		   try {
				   if(cm.existsElement(By.xpath(xpath)))
				   {
					   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(SmartWatchesPage.getbrioheader()).getText(), name, "Header Text not matched");
					   
					   System.out.println("Redirected to Colorfit Brio Product");
					   
					 
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					  
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								 
								   try
								   {
									  expected =list.get(i);
									 // driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										   break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									  
								   }
								 
								  
							   }
						}
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   
	   
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Smart Watches List");
				   cm.waitMethod();
				   list.clear();
				   }
		   }
		   
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Colorfit Pulse watch is not present or clickable");
			 
		   }
		 
		   
	   }

	
}