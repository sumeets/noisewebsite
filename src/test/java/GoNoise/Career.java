package GoNoise;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.CareerPage;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class Career extends Apache_POI_TC
{
	
   @Test(description = "Verify Career",priority=1)	
   public void TC_verifyCareer() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyCareer",
				"Verify Career");
	   
	   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS); 
	   
	   /****************** Click on not now in Popup *****************/
	   
	   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
	   
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to below");
	  
	  /****************** Click on not now in Popup *****************/
	   
	  
	   try {
		   
		       driver.findElement(NoiseHomePage.getcareers()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Career Link is Clicked and redirected to Career Page");
			   
			   driver.findElement(CareerPage.getopenpositionslink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("See Open positions link is clicked");
			   cm.waitMethod();
			   
			   cm.scrolldownone();
			   cm.waitMethod();
			   cm.scrollup();
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Scroll down below and Up");
			   
			   driver.navigate().back();
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Go back to Home Page");
			   
			   
		      
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Career Link is not Found nor clicked");
	   }
	  
	 
   }


  
}
