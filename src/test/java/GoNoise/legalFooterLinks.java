package GoNoise;

import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.ContactUsPage;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class legalFooterLinks extends Apache_POI_TC
{
	
	@Test(description = "Verify Terms and Conditions",priority=1)	
	   public void TC_verifyClickTermsConditions() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickTermsConditions",
					"Verify Terms and Conditions");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getterms()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Terms and Conditions Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Terms and Conditions Page is opened");
			   
			
			   
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Terms and Conditions is not Clicked");
		   }
		  
		 
	   }
	
	
	  @Test(description = "Verify Privacy Policy",priority=2)	
	   public void TC_verifyPrivacyPolicy() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyPrivacyPolicy",
					"Verify Privacy Policy");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getprivacy()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Privacy Policy Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Privacy Policy Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down");
				
			   
			     driver.navigate().to("https://www.gonoise.com/");
				   
				   cm.waitMethod();
				   
				   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Privacy Policy Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	
}
