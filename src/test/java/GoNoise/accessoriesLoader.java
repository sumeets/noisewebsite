package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.SpeakersPage;
import pages.Wirelessearbudspage;
import pages.accessoriesPage;
import pages.BuletoothEarPhonesPage;
import pages.HeadphonesPage;
import utility.ExtentReport;
import utility.values_constant;


public class accessoriesLoader extends Apache_POI_TC
{
	String actual,expected;
	ArrayList <String> list;
	
	
	
	  @Test(description = "Verify Classic Nylon 22mm Strap",priority=1)	
	   public void TC_verifyclassicNylon22mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyclassicNylon22mm",
					"Verify Classic Nylon 22mm Strap");
		   
		   WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		  driver.navigate().to("https://www.gonoise.com/collections/accessories");
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Accessories");
		
          /********************** scroll down ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down ******************/ 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getclassicNylon22mmLink()))
				   {
					   driver.findElement(accessoriesPage.getclassicNylon22mmLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicNylon22mmheader()).getText(), "Classic Nylon 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Nylon 22mm Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									   
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Nylon 22mm is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	@Test(description = "Verify Classic Leather 22mm(Strap)",priority=2)	
	   public void TC_verifyClassicLeather22mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClassicLeather22mm",
					"Verify Classic Leather 22mm(Strap)");
		   
		 
		   
		   /**************** click on load more link ************/
		  /* try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getclassicLeather22mmLink()))
				   {
					   driver.findElement(accessoriesPage.getclassicLeather22mmLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicLeather22mmheader()).getText(), "Classic Leather 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Leather 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Leather 22mm (Strap) is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	@Test(description = "Verify Uniweave 22mm XS Strap",priority=3)	
	   public void TC_verifyUniweave22mmXSStrap() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave22mmXSStrap",
					"Verify Uniweave 22mm XS Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		 /*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getuniweave22mmxsstrapLink()))
				   {
					   driver.findElement(accessoriesPage.getuniweave22mmxsstrapLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getuniweave22mmxsheader()).getText(), "Uni Weave 22mm - XS (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uniweave 22mm XS Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uniweave 22mm XS Strap is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	@Test(description = "Verify Uniweave 22mm Small Strap",priority=4)	
	   public void TC_verifyUniweave22mmSmall() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave22mmSmall",
					"Verify Uniweave 22mm Small Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		  /* try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getuniweave22mmsmallLink()))
				   {
					   driver.findElement(accessoriesPage.getuniweave22mmsmallLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getuniweave22mmsmallheader()).getText(), "Uni Weave 22mm - Small (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uniweave 22mm Small Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uniweave 22mm Small Strap is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Uniweave 22mm Large Strap",priority=5)	
	   public void TC_verifyUniweave22mmLarge() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave22mmLarge",
					"Verify Uniweave 22mm Large Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		/*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getuniweave22mmlargeLink()))
				   {
					   driver.findElement(accessoriesPage.getuniweave22mmlargeLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getuniweave22mmlargeheader()).getText(), "Uni Weave 22mm - Large (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uniweave 22mm Large Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uniweave 22mm Large Strap is not present or clickable");
			 
		   }
	   
		   
	   }
	
	
	@Test(description = "Verify magnetic leather 22mm Strap",priority=6)	
	   public void TC_verifyMagneticLeather22mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMagneticLeather22mm",
					"Verify magnetic leather 22mm Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		  /* try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getmagneticleather22mmLink()))
				   {
					   driver.findElement(accessoriesPage.getmagneticleather22mmLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getmagneticleather22mmheader()).getText(), "Magnetic Leather 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Magnetic Leather 22mm Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Magnetic Leather 22mm Strap is not present or clickable");
			 
		   }
	   
		   
	   }
	

	@Test(description = "Verify Classic Silicon 20mm Strap",priority=7)	
	   public void TC_verifyClassicSilicon20mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClassicSilicon20mm",
					"Verify Classic Silicon 20mm Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		  /* try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getclassicsilicon20mmLink()))
				   {
					   driver.findElement(accessoriesPage.getclassicsilicon20mmLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicsilicon20mmheader()).getText(), "Classic Silicone 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Silicone 20mm Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Silicone 20mm Strap is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Classic Silicon 19mm Strap",priority=8)	
	   public void TC_verifyClassicSilicon19mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClassicSilicon19mm",
					"Verify Classic Silicon 19mm Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getclassicsilicon19mmLink()))
				   {
					   driver.findElement(accessoriesPage.getclassicsilicon19mmLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicsilicon19mmheader()).getText(), "Classic Silicone 19mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Silicone 19mm Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Silicone 19mm Strap is not present or clickable");
			 
		   }
	   
		   
	   }
	
	
	@Test(description = "Verify Sports Edition 22mm Strap",priority=9)	
	   public void TC_verifySportsEdition22mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySportsEdition22mm",
					"Verify Sports Edition 22mm Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getSportsEdition22mmLink()))
				   {
					   driver.findElement(accessoriesPage.getSportsEdition22mmLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getSportsEdition22mmheader()).getText(), "Sports Edition 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Sports Edition 22mm Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Sports Edition 22mm Strap is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Premium Silicon 22mm Strap",priority=10)	
	   public void TC_verifyPremiumSilicon22mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyPremiumSilicon22mm",
					"Verify Premium Silicon 22mm Strap");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getPremiumSilicon22mmLink()))
				   {
					   driver.findElement(accessoriesPage.getPremiumSilicon22mmLink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getPremiumSilicon22mmheader()).getText(), "Premium Silicone 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Premium Silicon 22mm Strap Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Premium Silicon 22mm Strap is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Noise Magnetic Charging Cable",priority=11)	
	   public void TC_verifyMagneticChargingCable() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMagneticChargingCable",
					"Verify Noise Magnetic Charging Cable");
		   
		 
		   
		   /**************** click on load more link ************/
		 /*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getNoiseMagneticChargingCable()))
				   {
					   driver.findElement(accessoriesPage.getNoiseMagneticChargingCable()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getNoiseMagneticChargingCableHeader()).getText(), "Noise Magnetic Charging Cable", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Magnetic Charging Cable Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Magnetic Charging Cable is not present or clickable");
			 
		   }
	   
		   
	   }
	
	
	@Test(description = "Verify Striped Silicon XFit-1",priority=12)	
	   public void TC_verifyStripedSiliconXFit1() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyStripedSiliconXFit1",
					"Verify Striped Silicon XFit-1");
		   
		 
		   
		   /**************** click on load more link ************/
		 /*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getStripedSiliconXFit1Link()))
				   {
					   driver.findElement(accessoriesPage.getStripedSiliconXFit1Link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getStripedSiliconXFit1Header()).getText(), "Striped Silicone X-Fit 1 Strap (20 MM)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Striped Silicon XFit-1 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Striped Silicon XFit-1 is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Metallic Link 22mm",priority=13)	
	   public void TC_verifyMetallicLink22mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMetallicLink22mm",
					"Verify Metallic Link 22mm");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getMetallicLink22mmStrap()))
				   {
					   driver.findElement(accessoriesPage.getMetallicLink22mmStrap()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getMetallicLink20mmHeader()).getText(), "Metallic Link 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Metallic Link 22mm Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Metallic Link 22mm is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Magnetic Leather 20mm",priority=14)	
	   public void TC_verifyMagneticLeather20mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMagneticLeather20mm",
					"Verify Magnetic Leather 20mm");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getMagneticLeather20mmStrap()))
				   {
					   driver.findElement(accessoriesPage.getMagneticLeather20mmStrap()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getMagneticLeather20mmHeader()).getText(), "Magnetic Leather 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Magnetic Leather 20mm Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Magnetic Leather 20mm is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Metallic Link 20mm",priority=15)	
	   public void TC_verifyMetallicLink20mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMetallicLink20mm",
					"Verify Metallic Link 20mm");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getMetallicLink20mmStrap()))
				   {
					   driver.findElement(accessoriesPage.getMetallicLink20mmStrap()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getMetallicLink20mmHeader()).getText(), "Metallic Link 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Metallic Link 20mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Metallic Link 20mm (Strap) is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Uniweave 20mm L",priority=16)	
	   public void TC_verifyUniweave20mmL() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave20mmL",
					"Verify Uniweave 20mm L");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getUniWeave20mmLStrap()))
				   {
					   driver.findElement(accessoriesPage.getUniWeave20mmLStrap()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getUniWeave20mmLHeader()).getText(), "Uni Weave 20mm - L (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 20mm - L (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 20mm - L (Strap) is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Uniweave 20mm S",priority=17)	
	   public void TC_verifyUniweave20mmS() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave20mmS",
					"Verify Uniweave 20mm S");
		   
		 
		   
		   /**************** click on load more link ************/
		 /* try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getUniWeave20mmSStrap()))
				   {
					   driver.findElement(accessoriesPage.getUniWeave20mmSStrap()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getUniWeave20mmSHeader()).getText(), "Uni Weave 20mm - S (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 20mm - S (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 20mm - S (Strap) is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Uniweave 20mm XS",priority=18)	
	   public void TC_verifyUniweave20mmXS() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave20mmXS",
					"Verify Uniweave 20mm XS");
		   
		 
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getUniWeave20mmXSStrap()))
				   {
					   driver.findElement(accessoriesPage.getUniWeave20mmXSStrap()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getUniWeave20mmXSHeader()).getText(), "Uni Weave 20mm - XS (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 20mm - XS (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 20mm - XS (Strap) is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Pride Edition 22mm",priority=19)	
	   public void TC_verifyPrideEdition22mm() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyPrideEdition22mm",
					"Verify Pride Edition 22mm");
		   
		 
		   
		   /**************** click on load more link ************/
		  /* try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getPriceEdition22mmStrap()))
				   {
					   driver.findElement(accessoriesPage.getPriceEdition22mmStrap()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getPriceEdition22mmHeader()).getText(), "Pride Edition 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Pride Edition 22mm Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Pride Edition 22mm is not present or clickable");
			 
		   }
	   
		   
	   }
	
	@Test(description = "Verify Noise smartwatch charging dock",priority=20)	
	   public void TC_verifyChargingDock() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyChargingDock",
					"Verify Noise smartwatch charging dock");
		   
		 
		   
		   /**************** click on load more link ************/
		 /*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(accessoriesPage.getChargingdockType4()))
				   {
					   driver.findElement(accessoriesPage.getChargingdockType4()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getChargingDockHeader()).getText(), "Noise Smartwatch Charging Dock Type 4 (For ColorFit Ultra 2)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Pride Edition 22mm Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noise smartwatch charging dock is not present or clickable");
			 
		   }
	   
		   
	   }
	
}