package GoNoise;

import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.ContactUsPage;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class AboutNoiseFooterLinks extends Apache_POI_TC
{
	
	@Test(description = "Verify About Us Click",priority=1)	
	   public void TC_verifyClickAboutUs() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickAboutUs",
					"Verify About Us Click");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getaboutus()).click();
			   
			   ExtentReport.ExtentReportInfoLog("About Us Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("About Us is opened");
			   
			 
			   
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("About Us Link is not Clicked");
		   }
		  
		 
	   }
	
	
	  @Test(description = "Verify Stories",priority=2)	
	   public void TC_verifyClickStories() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickStories",
					"Verify Stories");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getstories()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Stories Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Stories Page is opened");
			   
			   cm.scrolldown();
			   
			   cm.waitMethod();
				
			   
			     driver.navigate().to("https://www.gonoise.com/");
				   
				   cm.waitMethod();
				   
				   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Stories Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	  
	  @Test(description = "Verify Careers",priority=3)	
	   public void TC_verifyClickCareers() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickCareers",
					"Verify Careers");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
				   driver.findElement(NoiseHomePage.getcareers()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Careers Link is Clicked");
				   
				   
				   
				   cm.waitMethod();
				   
				   ExtentReport.ExtentReportInfoLog("Careers Page is opened");
			   
			   
				 
			     
			     driver.navigate().to("https://www.gonoise.com/");
				   
				   cm.waitMethod();
				   
				   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
			 
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Careers Link is not Found nor clicked");
		   }
		  
		 
	   }
	  
	
   @Test(description = "Verify In The Press",priority=4)	
   public void TC_verifyInThePress() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyInThePress",
				"Verify In The Press");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	  // cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
			   driver.findElement(NoiseHomePage.getpress()).click();
			   
			   ExtentReport.ExtentReportInfoLog("In The Press Link is Clicked");
			   
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("In The Press Page is opened");
		   
		       cm.scrolldown(); 
		       
		       ExtentReport.ExtentReportInfoLog("Scroll down to below");
		       
		       cm.waitMethod();
		       
		       driver.navigate().back();
		       
		       ExtentReport.ExtentReportInfoLog("Going back to Home Page");
		       
		       cm.waitMethod();
			
		 
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("In The Press Link is not Found nor clicked");
	   }
	  
	 
   }
   
  
  
}
