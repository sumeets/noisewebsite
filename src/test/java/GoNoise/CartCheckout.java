package GoNoise;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.AccountLoginpage;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.Wirelessearbudspage;
import pages.trackyourorderpage;
import utility.ExtentReport;
import utility.GetContentFromExcelSheets;



public class CartCheckout extends Apache_POI_TC
{
	
	String username,pwd,email,firstname,lastname,phone,address,otp;
	int count=3;
	
   @Test(description = "Verify Login",priority=1)	
   public void TC_verifyLogin() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyLogin",
				"Verify Login");
	   
	  
	   cm.waitMethod();
	  
	  
	  /****************** Click on not now in Popup *****************/
	   
	   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
	  
	  
	  /****************** Click on not now in Popup *****************/
	  
	/*  WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
	   cm.hoverElement(mainMenu);
	   
	   cm.waitMethod();
	   
	   driver.findElement(ProductCartCheckoutPage.getViewAllEarBuds()).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Wireless Earbuds");*/
	   
	   driver.findElement(By.xpath("(//img[@class='icon-img'])[2]/parent::a")).click();
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("My account is clicked");
	   
	   Scanner userInput1 = new Scanner(System.in);
	    
	    System.out.println("Enter Email Id");
	    username=userInput1.nextLine();
	    System.out.println(username);
	   
	   cm.waitForElement(AccountLoginpage.getEmailLogin()).sendKeys(username);
	   
	   ExtentReport.ExtentReportInfoLog("Email Id entered");
	   
	   Scanner userInput2 = new Scanner(System.in);
	    
	    System.out.println("Enter Password");
	    pwd=userInput2.nextLine();
	    System.out.println(pwd);
	   
	   cm.waitForElement(AccountLoginpage.getPwdLogin()).sendKeys(pwd);
	   
	   ExtentReport.ExtentReportInfoLog("password entered");
	   
       cm.waitForElement(AccountLoginpage.getbtnSignin()).click();
	   
	   ExtentReport.ExtentReportInfoLog("You have logged in successfully");
	   
	   cm.waitMethod();
	   
	   
	   
	  
	 
   }
   
   @Test(description = "Verify Product Cart to Checkout",priority=2)	
   public void TC_verifyCartToCheckout() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyCartToCheckout",
				"Verify Product Cart to Checkout");
	   
	  
	   cm.waitMethod();
	   
	   WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
	   cm.hoverElement(mainMenu);
	   
	   cm.waitMethod();
	   
	   driver.findElement(ProductCartCheckoutPage.getViewAllEarBuds()).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Wireless Earbuds");
	   
	   
	   /********************** scroll down and Load all products ******************/
	   
	   cm.scrolldownone();
	   cm.waitMethod();
	   cm.scrollup();
	   cm.waitMethod();
	   
	   
	   /********************** scroll down and Load all products ******************/ 
	   
	   try
	   {
		   if(cm.existsElement(Wirelessearbudspage.getVS303linknew()))
		   {
			   driver.findElement(Wirelessearbudspage.getVS303linknew()).click();
			   
			   cm.waitMethod();
			   
			   
			   try
			   {
				   driver.findElement(ProductCartCheckoutPage.getaddtobag()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Add to Bag is clicked");
			   }
			   catch(Exception e)
			   {
			    
				   driver.findElement(ProductCartCheckoutPage.getbuynow()).click();
				   ExtentReport.ExtentReportInfoLog("Buy Now is clicked");
				 
			   }
			  
			   cm.waitMethod();
			   
             
			   driver.findElement(ProductCartCheckoutPage.getgotobagiconlink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Navigated to Cart Page");
			   Thread.sleep(5000);
			   
			   /******************** Increase the qty of product by + icon ***********/
			   
				  for(int i=1;i<count;i++)
				   {
					 
					 try
					 {
						 driver.findElement(ProductCartCheckoutPage.getQtyPlusbutton()).click();
						   
						  Thread.sleep(3000);
					 }
					 
					  catch(Exception e)
					   {
						
						 // ExtentReport.ExtentReportInfoLog("Quantity is not increased");
						  System.out.println("Quantity is not increased");
					   }
					 
					
					  
				   }
				   
				
				   
				/******************** Increase the qty of product by + icon ***********/
				  
				  
				  driver.findElement(ProductCartCheckoutPage.getpaycard()).click();
				  ExtentReport.ExtentReportInfoLog("COD/Card is clicked and redirected to Checkout Page");
				  cm.waitMethod();
				  
				  /************************* Fill form **************/
				  
					 List<String> checkoutdata=new ArrayList<String>();
					   
				        checkoutdata=GetContentFromExcelSheets.readData("excelFileContents.xlsx",1);
					   
					    System.out.println(checkoutdata.size());

					    email=checkoutdata.get(0);
					    firstname=checkoutdata.get(1);
					    lastname=checkoutdata.get(2);
					    phone=checkoutdata.get(3);
					    address=checkoutdata.get(4);
					   
					   
					 //  driver.findElement(ProductCartCheckoutPage.getcontactEmail()).sendKeys("abcd@gmail.com");
					   
					//   cm.waitMethod();
					   
					   driver.findElement(By.id("checkout_shipping_address_first_name")).sendKeys("abcd");
					   
					   cm.waitMethod();
					   
					   driver.findElement(By.id("checkout_shipping_address_last_name")).sendKeys("ddd");
					   
				       cm.waitMethod();
					   
					   driver.findElement(By.id("checkout_shipping_address_phone_clone")).sendKeys("8240724302");
					   
					   cm.waitMethod();
				   
					   driver.findElement(By.id("checkout_shipping_address_address1")).sendKeys("kishangarh,vasantkunj,new delhi");
					   
					   cm.waitMethod();
					   
					   driver.findElement(By.id("zipcode")).sendKeys("110090");
					   
				       Thread.sleep(5000);
					   
				       driver.findElement(By.id("checkout_shipping_address_city")).click();
				       
				       cm.waitMethod();
				       
					  /* driver.findElement(By.id("checkout_shipping_address_city")).sendKeys("Delhi");
					   
					   cm.waitMethod();*/
					   
					   driver.findElement(ProductCartCheckoutPage.getConfirmButton()).click();
					   
					   cm.waitMethod();
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Payment Page");
					   
					   Thread.sleep(5000);
					   
					  
					   driver.findElement(By.xpath("(//input[@name='checkout[payment_gateway]'])[3]")).click();
					   
					   ExtentReport.ExtentReportInfoLog("Payment method COD is selected");
					   
					   cm.waitMethod();
					   
					   /****************** Otp **********************/
					     // otp="1234";
					      
					/*      Scanner otpval= new Scanner(System.in);
						   System.out.println("Enter the OTP: ");
						   String otpvalue=otpval.nextLine();
						   cm.waitMethod();
					    
					    driver.findElement(By.id("votp")).click();
					    Thread.sleep(5000);
					    driver.findElement(By.id("votp")).sendKeys(otpvalue);
					    Thread.sleep(5000);
					  //  cm.waitMethod();
					    ExtentReport.ExtentReportInfoLog("Otp Entered");*/
					   
					   
					   
					   
					   /****************** Otp **********************/
				  
				   
				
			 
			   
		   }
	   }
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("The product VS303 is not found");
	   }
			   
			   
   }
   
   
  /* private void fillCheckoutForm() throws InterruptedException, IOException
   {
      
	   
	   /************ Fill the form *************/
		  
	/*	 List<String> checkoutdata=new ArrayList<String>();
		   
	        checkoutdata=GetContentFromExcelSheets.readData("excelFileContents.xlsx",1);
		   
		    System.out.println(checkoutdata.size());

		    email=checkoutdata.get(0);
		    firstname=checkoutdata.get(1);
		    lastname=checkoutdata.get(2);
		    phone=checkoutdata.get(3);
		    address=checkoutdata.get(4);
		   
		   
		 //  driver.findElement(ProductCartCheckoutPage.getcontactEmail()).sendKeys("abcd@gmail.com");
		   
		//   cm.waitMethod();
		   
		/*   driver.findElement(By.id("checkout_shipping_address_first_name")).sendKeys("abcd");
		   
		   cm.waitMethod();
		   
		   driver.findElement(By.id("checkout_shipping_address_last_name")).sendKeys("ddd");
		   
	       cm.waitMethod();
		   
		   driver.findElement(By.id("checkout_shipping_address_phone_clone")).sendKeys("8240724302");
		   
		   cm.waitMethod();
	   
		   driver.findElement(By.id("checkout_shipping_address_address1")).sendKeys("kishangarh,vasantkunj,new delhi");
		   
		   cm.waitMethod();
		   
		   driver.findElement(By.id("zipcode")).sendKeys("110090");
		   
	       Thread.sleep(5000);
		   
	       driver.findElement(By.id("checkout_shipping_address_city")).click();
	       
	       cm.waitMethod();
	       
		  /* driver.findElement(By.id("checkout_shipping_address_city")).sendKeys("Delhi");
		   
		   cm.waitMethod();*/
	/*	   
		   driver.findElement(ProductCartCheckoutPage.getConfirmButton()).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("Redirected to Payment Page");
		   
		  
		   driver.findElement(By.xpath("(//input[@name='checkout[payment_gateway]'])[3]")).click();
		   
		   ExtentReport.ExtentReportInfoLog("Payment method COD is selected");
		   
		   cm.waitMethod();
		   
		   /****************** Otp **********************/
		     // otp="1234";
		      
		/*      Scanner otpval= new Scanner(System.in);
			   System.out.println("Enter the OTP: ");
			   String otpvalue=otpval.nextLine();
			   cm.waitMethod();
		    
		    driver.findElement(By.id("votp")).click();
		    Thread.sleep(5000);
		    driver.findElement(By.id("votp")).sendKeys(otpvalue);
		    Thread.sleep(5000);
		  //  cm.waitMethod();
		    ExtentReport.ExtentReportInfoLog("Otp Entered");*/
		   
		   
		   
		   
		   /****************** Otp **********************/
	   
	
	   
	  
   //}


  
}
