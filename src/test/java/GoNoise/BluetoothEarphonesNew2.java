package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.Wirelessearbudspage;
import pages.BuletoothEarPhonesPage;
import utility.ExtentReport;
import utility.values_constant;


public class BluetoothEarphonesNew2 extends Apache_POI_TC
{
	String actual,expected;
	ArrayList <String> list;
	
	
	
	@Test(description = "Verify Combat Gaming Neckband",priority=1)	
	   public void TC_verifyCombatGaming() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyCombatGaming",
					"Verify Combat Gaming Neckband");
		   
		   WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.findElement(ProductCartCheckoutPage.getViewAllbluetoothearphones()).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Bluetooth Earphones");
		
		   
		   /**************** click on load more link ************/
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(BuletoothEarPhonesPage.getCombatlink()))
				   {
					   driver.findElement(BuletoothEarPhonesPage.getCombatlink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.getcombatheader()).getText(), "Combat Gaming Neckband", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Combat Gaming Neckband Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Combat Neckband is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	@Test(description = "Verify Tune Sport2",priority=9)	
	   public void TC_verifyTuneSport2() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyTuneSport2",
					"Verify Tune Sport2");
		   
		 
		   
		   /**************** click on load more link ************/
		   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }
		  
		   
		   /**************** click on load more link ************/
		   
		   
		 
		   
		   try {
				   if(cm.existsElement(BuletoothEarPhonesPage.getTuneSport2link()))
				   {
					   driver.findElement(BuletoothEarPhonesPage.getTuneSport2link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.gettunesport2header()).getText(), "Tune Sport 2", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Tune Sport2 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Tune Sport2 is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	
}