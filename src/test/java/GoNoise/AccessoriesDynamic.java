package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.Wirelessearbudspage;
import pages.accessoriesPage;
import utility.ExtentReport;
import utility.commonutilmethods;
import utility.values_constant;


public class AccessoriesDynamic extends Apache_POI_TC
{
	String actual,expected;
	ArrayList <String> list;
	
	
	
	@Test(description = "Verify Classic Silicon 19mm Strap")	
	   public void TC_verifyClassicSilicon19mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClassicSilicon19mm",
					"Verify Classic Silicon 19mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
           /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			  
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicsilicon19mmheader()).getText(), "Classic Silicone 19mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Silicone 19mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Silicone 19mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Classic Leather 22mm Strap")	
	   public void TC_verifyClassicLeather22mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClassicLeather22mm",
					"Verify Classic Leather 22mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
        /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicLeather22mmheader()).getText(), "Classic Leather 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Leather 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Leather 22mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Premium Silicone 22mm Strap")	
	   public void TC_verifyPremiumSilicone22mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyPremiumSilicone22mm",
					"Verify Premium Silicone 22mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
     /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getPremiumSilicon22mmheader()).getText(), "Premium Silicone 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Premium Silicone 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Premium Silicone 22mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Classic Silicone 20mm Strap")	
	   public void TC_verifyClssicSilicone20mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClssicSilicone20mm",
					"Verify Classic Silicone 20mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
  /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicsilicon20mmheader()).getText(), "Classic Silicone 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Silicone 20mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Silicone 20mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Classic Nylon 22mm Strap")	
	   public void TC_verifyClssicNylon22mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClssicNylon22mm",
					"Verify Classic Nylon 22mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
/***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicNylon22mmheader()).getText(), "Classic Nylon 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Nylon 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Nylon 22mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Magnetic Leather 22mm Strap")	
	   public void TC_verifyMagneticLeather22mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMagneticLeather22mm",
					"Verify Magnetic Leather 22mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getmagneticleather22mmheader()).getText(), "Magnetic Leather 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Magnetic Leather 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Magnetic Leather 22mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Metallic Link 22mm Strap")	
	   public void TC_verifyMetallicLink22mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMetallicLink22mm",
					"Verify Metallic Link 22mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
           /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getMetallicLink22mmHeader()).getText(), "Metallic Link 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Metallic Link 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Metallic Link 22mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Uni Weave 22m XS Strap")	
	   public void TC_verifyUniWeave22mmxs(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniWeave22mmxs",
					"Verify Uni Weave 22m XS Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
        /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getuniweave22mmxsheader()).getText(), "Uni Weave 22mm - XS (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 22mm - XS (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 22mm - XS (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Pride Edition 22mm Strap")	
	   public void TC_verifyPrideEdition22mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyPrideEdition22mm",
					"Verify Pride Edition 22mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
     /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getPriceEdition22mmHeader()).getText(), "Pride Edition 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Pride Edition 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Pride Edition 22mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Sports Edition 22mm Strap")	
	   public void TC_verifySportsEdition22mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySportsEdition22mm",
					"Verify Sports Edition 22mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
  /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getSportsEdition22mmheader()).getText(), "Sports Edition 22mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Sports Edition 22mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Sports Edition 22mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Uniweave 22mm Strap Large")	
	   public void TC_verifyUniWeave22mmLarge(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniWeave22mmLarge",
					"Verify Uniweave 22mm Strap Large");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
           /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getuniweave22mmlargeheader()).getText(), "Uni Weave 22mm - Large (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 22mm - Large (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 22mm - Large (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Uniweave 22mm Strap Small")	
	   public void TC_verifyUniWeave22mmSmall(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniWeave22mmSmall",
					"Verify Uniweave 22mm Strap Small");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
        /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getuniweave22mmsmallheader()).getText(), "Uni Weave 22mm - Small (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 22mm - Small (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 22mm - Small (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Magnetic Leather 20mm Strap")	
	   public void TC_verifyMagneticLeather20mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMagneticLeather20mm",
					"Verify Magnetic Leather 20mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
     /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getMagneticLeather20mmHeader()).getText(), "Magnetic Leather 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Magnetic Leather 20mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Magnetic Leather 20mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	
	
	
	   @Test(description = "Verify Metallic Link 20mm Strap")	
	   public void TC_verifyMetallicLink20mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMetallicLink20mm",
					"Verify Metallic Link 20mm Strap");
		   
		   

		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
  /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getMetallicLink20mmHeader()).getText(), "Metallic Link 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Metallic Link 20mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Metallic Link 20mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	   
	   
	   @Test(description = "Verify Uniweave 20mm -l Strap")	
	   public void TC_verifyUniweave20mml(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave20mml",
					"Verify Uniweave 20mm -l Strap");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getUniWeave20mmLHeader()).getText(), "Uni Weave 20mm - L (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 20mm - L (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 20mm - L (Strap) is sold out");
			 
		   }
		   
		  
	   }
	   
	   
	   @Test(description = "Verify Uniweave 20mm -s Strap")	
	   public void TC_verifyUniweave20mms(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave20mms",
					"Verify Uniweave 20mm -s Strap");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getUniWeave20mmSHeader()).getText(), "Uni Weave 20mm - S (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 20mm - S (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 20mm - S (Strap) is sold out");
			 
		   }
		   
		  
	   }

	   
	   @Test(description = "Verify Uniweave 20mm -xs Strap")	
	   public void TC_verifyUniweave20mmxs(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyUniweave20mmxs",
					"Verify Uniweave 20mm -xs Strap");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getUniWeave20mmXSHeader()).getText(), "Uni Weave 20mm - XS (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Uni Weave 20mm - XS (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Uni Weave 20mm - XS (Strap) is sold out");
			 
		   }
		   
		  
	   }
	   
	   @Test(description = "Verify Striped Silicone X-Fit 1 Strap (20 MM)")	
	   public void TC_verifySiliconexfit1(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySiliconexfit1",
					"Verify Striped Silicone X-Fit 1 Strap (20 MM) strap");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getStripedSiliconXFit1Header()).getText(), "Striped Silicone X-Fit 1 Strap (20 MM)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Striped Silicone X-Fit 1 Strap (20 MM) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Striped Silicone X-Fit 1 Strap (20 MM) is sold out");
			 
		   }
		   
		  
	   }
	   
	   @Test(description = "Verify Classic Nylon 20mm (Strap)")	
	   public void TC_verifyClassicNylon20mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClassicNylon20mm",
					"Verify Classic Nylon 20mm (Strap)");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getclassicNylon20mmheader()).getText(), "Classic Nylon 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Classic Nylon 20mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Classic Nylon 20mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	   
	   
	   
	   @Test(description = "Verify Sports Edition 20mm (Strap)")	
	   public void TC_verifySportsEdition20mm(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySportsEdition20mm",
					"Verify Sports Edition 20mm (Strap)");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getSportsEdition20mmheader()).getText(), "Sports Edition 20mm (Strap)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Sports Edition 20mm (Strap) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Sports Edition 20mm (Strap) is sold out");
			 
		   }
		   
		  
	   }
	   
	   
	   @Test(description = "Verify noise smartwatch charging dock type 4 (for colorfit ultra 2)")	
	   public void TC_verifyChargingDockType4(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyChargingDockType4",
					"Verify noise smartwatch charging dock type 4 (for colorfit ultra 2)");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getChargingDockHeader()).getText(), "Noise Smartwatch Charging Dock Type 4 (For ColorFit Ultra 2)", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Noise Smartwatch Charging Dock Type 4 (For ColorFit Ultra 2) Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noise Smartwatch Charging Dock Type 4 (For ColorFit Ultra 2) is sold out");
			 
		   }
		   
		  
	   }
	   
	   @Test(description = "Verify noise magnetic charging cable")	
	   public void TC_verifyMagneticChargingCable(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyMagneticChargingCable",
					"Verify noise magnetic charging cable");
		   
		   

		       System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
              /***** code added 20/07/2022 *********/
			   
			   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		 
		   
		 
		   
		   
		   try {
				  // if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   //{
			           driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(accessoriesPage.getNoiseMagneticChargingCableHeader()).getText(), "Noise Magnetic Charging Cable", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Noise Magnetic Charging Cable Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				       String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				  // }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Accessories List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Noise Magnetic Charging Cable is sold out");
			 
		   }
		   
		  
	   }

}
