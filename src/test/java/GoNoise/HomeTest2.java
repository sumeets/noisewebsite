package GoNoise;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import utility.ExtentReport;


public class HomeTest2 extends Apache_POI_TC
{
	
  
  
  
	  @Test(description = "Verify product click functionality",priority=1)	
	   public void TC_verifyClickProductLink() throws InterruptedException
	   {
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickProductLink",
					"Verify product click functionality");
		   
		   driver.findElement(NoiseHomePage.getProductLink()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Product Link is Clicked");
		   
		   
		   cm.waitMethod();
		   
		   cm.scrolldown();
		   
		   /************ Go Back to Home Page *********/
		   
		   cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
		   /************ Go Back to Home Page *********/
	   }
	   
	   @Test(description = "Verify support click functionality",priority=2)	
	   public void TC_verifyClickSupportLink() throws InterruptedException
	   {
		   cm.waitMethod();
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickSupportLink",
					"Verify support click functionality");
		   
		   driver.findElement(NoiseHomePage.getSupportLink()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Support Link is Clicked");
		   
		   cm.waitMethod();
		   
		   cm.scrolldown();
		   
		   ExtentReport.ExtentReportInfoLog("Scroll down to below");
		   
		   //System.out.println("Scroll down to below");
		   
		   /************ Go Back to Home Page *********/
		   
		   cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
		   
		   /************ Go Back to Home Page *********/
	   }
	   
	   @Test(description = "Verify stories click functionality",priority=3)	
	   public void TC_verifyClickStoriesLink() throws InterruptedException
	   {
		   cm.waitMethod();
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickStoriesLink",
					"Verify stories click functionality");
		   
		   driver.findElement(NoiseHomePage.getStoriesLink()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Stories Link is Clicked");
		   
		   cm.waitMethod();
		   
		   cm.scrolldown();
		   
		   System.out.println("Scroll down to below");
		   
		   /************ Go Back to Home Page *********/
		   
		   cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		      
		   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
		   /************ Go Back to Home Page *********/
	   }
	   
	   @Test(description = "Verify search functionality",priority=4)	
	   public void TC_verifySearchFunction() throws InterruptedException
	   {
		   cm.waitMethod();
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySearchFunction",
					"Verify search functionality");
		   
		   driver.findElement(NoiseHomePage.getSearchIcon()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Search Icon is clicked");
		   
		   driver.findElement(By.id("menuSearch")).sendKeys("colorfit pro 3 assist");
		   
		   cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getSearchText()).click();
		   
		   ExtentReport.ExtentReportInfoLog("product is searched");
		   
		   cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   }
	   
	   @Test(description = "Verify Cart Click functionality",priority=5)	
	   public void TC_verifyClickCart() throws InterruptedException
	   {
		   cm.waitMethod();
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickCart",
					"Verify Cart Click functionality");
		   
		   driver.findElement(NoiseHomePage.getCartIcon()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Cart Icon is clicked");
		   
		   cm.waitMethod();
		   
		   String actualcarttext=driver.findElement(NoiseHomePage.getCartEmptyText()).getText();
		   
		   Assert.assertEquals(actualcarttext, "Your cart is empty", "Not Navigated to Cart Page");
		   
		   ExtentReport.ExtentReportInfoLog("Navigated to Cart Page");
		   
		   driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   }
	   
	   @Test(description = "Verify Account Click functionality",priority=6)	
	   public void TC_verifyClickAccount() throws InterruptedException
	   {
		   cm.waitMethod();
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickAccount",
					"Verify Account Click functionality");
		   
		   driver.findElement(NoiseHomePage.getMyAccountIcon()).click();
		   
		   ExtentReport.ExtentReportInfoLog("My Account Icon is clicked");
		   
		   
		   String actualText=driver.findElement(NoiseHomePage.getHelloNoiseMaker()).getText();
		   
		   Assert.assertEquals(actualText, "Hello NoiseMaker!");
		   
		   ExtentReport.ExtentReportInfoLog("Navigated to Account Page");
		  
		   cm.waitMethod();
		   
	      driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
	      ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
	   }
	   
	/*   @Test(description = "Verify Scroll down and Know More click",priority=7)	
	   public void TC_scrollToKnowMoreAndClick() throws InterruptedException
	   {
		   cm.waitMethod();
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_scrollToKnowMoreAndClick",
					"Verify Scroll down and Know More click");
		   
		   /******************** Start Scroll and click of Wireless Earbuds and Smart Watches **************/
		   
		/*   cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getWirelessBudsText()));
		   
		   ExtentReport.ExtentReportInfoLog("Scroll down to Wireless Earbuds");
		
		   cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getKnowMoreWirelessBuds()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Know More of Wireless Earbuds is clicked");
		  
		   cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
	       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
		   cm.waitMethod();
		   
	       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getWirelessBudsText()));
	       
	       ExtentReport.ExtentReportInfoLog("Scroll down to Smart Watches");
	       
	       cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getKnowMoreSmartWatches()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Know More of Smart Watches is clicked");
		  
		   cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
	       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
		   cm.waitMethod();
		   
		   /******************** End  Scroll and click of Wireless Earbuds and Smart Watches **************/
		   
		   /******************** Start Scroll and click of Bluetooth Neckbands and Accessories **************/
		   
	  /*     cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getBluetoothNeckbandsText()));
	       
	       ExtentReport.ExtentReportInfoLog("Scroll down to Bluetooth Neckbands");
	       
	       cm.waitMethod();
		   
		   driver.findElement(NoiseHomePage.getKnowMoreBluetoothNeckbands()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Know More of Bluetooth Neckbands is clicked");
		   
		   cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
	       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
		   cm.waitMethod();
		   
	       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getBluetoothNeckbandsText()));
	       
	       ExtentReport.ExtentReportInfoLog("Scroll down to Bluetooth Neckbands");
	      
	       cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getKnowMoreAccessories()).click();
	       
	       ExtentReport.ExtentReportInfoLog("Know More of Accessories is clicked");
		   
		   cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
	       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
		   /******************** End Scroll and click of Bluetooth Neckbands and Accessories **************/
		   
		   
		  
	//   }
	  
	/*   @Test(description = "Verify Scroll down Read All and click",priority=8)	
	   public void TC_scrollToReadAllAndClick() throws InterruptedException
	   {
		   cm.waitMethod();
		   
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_scrollToReadAllAndClick",
					"Verify Scroll down Read All and click");
		   
	       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getreadalltext()));
	       
	       ExtentReport.ExtentReportInfoLog("Scroll down to Read All Text");
	        
	       cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getReadAllLink()).click();
	       
	       ExtentReport.ExtentReportInfoLog("Read all is clicked and navigated to page");
		   
		  
		   cm.waitMethod();
		   
		   cm.scrolldown();
		   
		   cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
	       ExtentReport.ExtentReportInfoLog("Noise Logo is clicked and Navigated to Home Page");
		   
	   }*/
	   /*
	   @Test(description = "Verify Scroll down to Watch Now",priority=9)	
	   public void TC_scrollToWatchNow() throws InterruptedException
	   {
	       cm.waitMethod();
	       
	       ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_scrollToWatchNow",
					"Verify Scroll down to Watch Now");
		   
	       cm.scrolldowntoElement(driver.findElement(NoiseHomePage.getWatchNow()));
	       
	       ExtentReport.ExtentReportInfoLog("Scroll down to Watch Now");
	       
	     
	       cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getWatchNow()).click();
	       
	       ExtentReport.ExtentReportInfoLog("Click on Watch Now");
		  
		   cm.waitMethod();
		   
		   /*************** Switch to the video frame **********/
		   
		   
		   //driver.switchTo().frame(1);
		   
		 /*  driver.switchTo().frame(driver.findElement(By.xpath("//div[@class='video-wrapper']/iframe")));
		   
		   cm.waitMethod();
		   
		   /*************** Switch to the video frame **********/
		   
		   /*************** Play and pause the video ************/
		   
		  
		/*   
		   cm.YouTubeVideoPlayPause(driver.findElement(NoiseHomePage.getVideoPlayIcon()));
		  
		  /*************** Play and pause the video ************/
		   
		/*   ExtentReport.ExtentReportInfoLog("Video start playing.");
		   
		  
		   cm.waitMethod();
		   
		   driver.navigate().back();
		   
		   
		   cm.waitMethod();
		   
	       driver.findElement(NoiseHomePage.getNoiseLogo()).click();
		   
	       ExtentReport.ExtentReportInfoLog("Navigate back to Home Page");
		  
		   
	   }*/
  

 


  
}
