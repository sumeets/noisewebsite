package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.Wirelessearbudspage;
import utility.ExtentReport;
import utility.values_constant;


public class WirelessearbudsLoader extends Apache_POI_TC
{
	String actual,expected;
	ArrayList <String> list;
	
	
	/************************* New Template VS103 ********************/
	@Test(description = "Verify Buds VS103",priority=1)	
	   public void TC_verifyBudsVS103() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS103",
					"Verify Buds VS103");
		   
		   WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.findElement(ProductCartCheckoutPage.getViewAllEarBuds()).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Wireless EarBuds");
		   
          /********************** scroll down ******************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down ******************/ 
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   {
					   driver.findElement(Wirelessearbudspage.getVS103link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getVS103header()).getText(), "Buds VS103", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS103 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   WebElement producttitle= driver.findElement(Wirelessearbudspage.getbudsproducttitle());
						cm.scrolldowntoElement(producttitle);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(Wirelessearbudspage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(Wirelessearbudspage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(Wirelessearbudspage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS103 is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	/************************* New Template VS103 ********************/
	
	@Test(description = "Verify Air Buds Mini",priority=2)	
	   public void TC_verifyAirBudsMini() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirBudsMini",
					"Verify Air Buds Mini");
		   
		
		 
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getairbudminilink()))
				   {
					   driver.findElement(Wirelessearbudspage.getairbudminilink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsminiheader()).getText(), "Air Buds Mini", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Air Buds Mini Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Air Buds Mini is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	@Test(description = "Verify Airbuds+",priority=3)	
	   public void TC_verifyAirbudsplus() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirbudsplus",
					"Verify Airbuds+");
		   
		
		   
		   /**************** click on load more link ************/
		 /*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getairbudspluslink()))
				   {
					   driver.findElement(Wirelessearbudspage.getairbudspluslink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsplusheader()).getText(), "Air Buds+", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Air Buds Plus Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Airbuds Plus is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	@Test(description = "Verify Airbuds",priority=4)	
	   public void TC_verifyAirbuds() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAirbuds",
					"Verify Airbuds");
		   
		
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getAirbudslink()))
				   {
					   driver.findElement(Wirelessearbudspage.getAirbudslink()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getairbudsheader()).getText(), "Air Buds", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Air Buds Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Airbuds is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	
	   @Test(description = "Verify Buds VS303",priority=5)	
	   public void TC_verifyVS303() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyVS303",
					"Verify Buds VS303");
		   
		
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getVS303link()))
				   {
					   driver.findElement(Wirelessearbudspage.getVS303link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getvs303header()).getText(), "Buds VS303", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS303 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS303 is not present or clickable");
			 
		   }
		   
		  
	   }
	   
	   
	   @Test(description = "Verify Buds VS102",priority=6)	
	   public void TC_verifyVS102() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyVS102",
					"Verify Buds VS102");
		   
		
		   
		   /**************** click on load more link ************/
		/*   try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getVS102link()))
				   {
					   driver.findElement(Wirelessearbudspage.getVS102link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getVS102header()).getText(), "Buds VS102", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS102 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);// expected result
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS102 is not present or clickable");
			 
		   }
		   
		  
	   }
	   
  @Test(description = "Verify Buds VS201",priority=7)	
	   public void TC_verifyBudsVS201() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS201",
					"Verify Buds VS201");
		   
		
		   
		  
		 /*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
	
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getVS201link()))
				   {
					   driver.findElement(Wirelessearbudspage.getVS201link()).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getbudsVS201header()).getText(), "Buds VS201", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS201 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
					   /******************  Fetch the color text and insert in array      *******************/
				   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					  // WebElement qty= driver.findElement(SmartWatchesPage.getqty());
					  // cm.scrolldowntoElement(qty);
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS201 is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }   
  
  
	/************************* New Template ********************/
  
	@Test(description = "Verify Buds VS202",priority=8)	
	   public void TC_verifyBudsVS202() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBudsVS202",
					"Verify Buds VS202");
		   
		 
		   
		   /**************** click on load more link ************/
		 /*  try
		   {
			   if(cm.existsElement(SmartWatchesPage.getloadmorelink()))
			   {
				   driver.findElement(SmartWatchesPage.getloadmorelink()).click();
				   
				   ExtentReport.ExtentReportInfoLog("Load More link is clicked");
				   cm.waitMethod();
				   cm.scrollup();
				   cm.waitMethod();
			   }
		   }
		   catch(Exception e)
		   {
		    
			   ExtentReport.ExtentReportInfoLog("Load More Link not found");
			  
		   }*/
		  
		   
		   /**************** click on load more link ************/
		 
		   
		   try {
				   if(cm.existsElement(Wirelessearbudspage.getVS202link()))
				   {
					   driver.findElement(Wirelessearbudspage.getVS202link()).click();
					   
					   cm.waitMethod();
					   
					   WebElement producttitle= driver.findElement(Wirelessearbudspage.getbudsproducttitle());
					   cm.scrolldowntoElement(producttitle);
					   
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getbudsproducttitle()).getText(), "Buds VS202", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Buds VS202 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(Wirelessearbudspage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(Wirelessearbudspage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(Wirelessearbudspage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   }
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Buds VS202 is not present or clickable");
			 
		   }
		   
		  
		   
		   
		   
	   }
	
	/************************* New Template ********************/
	
	
}