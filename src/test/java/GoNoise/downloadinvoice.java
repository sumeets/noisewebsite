package GoNoise;

import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import utility.ExtentReport;


public class downloadinvoice extends Apache_POI_TC
{
	
   @Test(description = "Verify Download Invoice",priority=1)	
   public void TC_verifyClickDownloadInvoice() throws InterruptedException
   {
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickDownloadInvoice",
				"Verify Download Invoice");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
	   
	   cm.waitMethod();
	   
		 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
		 
		 /****************** Click on not now in Popup *****************/
	   
	   cm.scrolldown();
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
	   
	   
	   try {
		   
		   driver.findElement(NoiseHomePage.getdownloadinvoice()).click();
		   
		   ExtentReport.ExtentReportInfoLog("Download Invoice Link is Clicked");
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("Download Invoice Page is opened");
		   
		   
			   /*************************** valid order 1936834 and 8964001354 *************/
		   
			  driver.findElement(By.id("order-id")).sendKeys("12345");
			  
			  ExtentReport.ExtentReportInfoLog("Order id is entered");
			   
			  cm.waitMethod();
			   
              driver.findElement(By.id("mobile-number")).sendKeys("8240724302");
			  
			  ExtentReport.ExtentReportInfoLog("Mobile Number is entered");
			   
			  cm.waitMethod();
			  
			  driver.findElement(NoiseHomePage.getinvoicebutton()).click();
			  
			  ExtentReport.ExtentReportInfoLog("download invoice button is clicked");
			   
		      Thread.sleep(5000);
		      
		      String errmsg=driver.findElement(By.id("download-invalid-detail")).getText();
		      
		      System.out.println("error message is"+errmsg);
		      
		     if(errmsg!="")
		    {
		    	 //ExtentReport.ExtentReportInfoLog("error is "+driver.findElement(By.id("download-invalid-detail")).getText());
		    	 ExtentReport.ExtentReportErrorLog("error is "+errmsg);
		    }
		     else
		     {
		    	 ExtentReport.ExtentReportInfoLog("Invoice generated");
		    	 
		     }
		   
		  
		   
	   }
	   
	   catch(Exception e)
	   {
		   ExtentReport.ExtentReportInfoLog("Download Invoice Link is not Found nor clicked");
	   }
	  
	 
   }


  
}
