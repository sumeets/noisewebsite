package GoNoise;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.BuletoothEarPhonesPage;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import pages.Wirelessearbudspage;
import utility.ExtentReport;
import utility.commonutilmethods;
import utility.values_constant;


public class BluetoothEarphonesDynamic extends Apache_POI_TC
{
	String actual,expected;
	ArrayList <String> list;
	
	
	
	@Test(description = "Verify Flair")	
	   public void TC_verifyFlair(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyFlair",
					"Verify Flair");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
             /***** code added 20/07/2022 *********/
		   
			   String name1=name.toLowerCase();
			   //span[contains(text(),'ColorFit Pulse Grand')]
			    String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.getflairheader()).getText(), "Flair", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Flair Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Flair is sold out");
			 
		   }
		   
		  
	   }
	
	
	  @Test(description = "Verify Flair Xl")	
	   public void TC_verifyFlairXl(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyFlairXl",
					"Verify Flair Xl");
		   
		   System.out.println("name"+name);
		   /***** code added 20/07/2022 *********/
		   
		   String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			  System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
		  
		 
		   
		   try {
				 //  if(cm.existsElement(Wirelessearbudspage.getVS202link()))
				  // {
			   driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   
					   WebElement producttitle= driver.findElement(Wirelessearbudspage.getbudsproducttitle());
					   cm.scrolldowntoElement(producttitle);
					   
					   Assert.assertEquals(driver.findElement(Wirelessearbudspage.getbudsproducttitle()).getText(), "Flair XL", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Flair Xl Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariantnew()).size();
					   System.out.println("list count:"+licount);
					   
					   
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarraynew(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(Wirelessearbudspage.getnotifyme()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(Wirelessearbudspage.getnotifyme()).click();
											   cm.waitMethod();
											   driver.findElement(Wirelessearbudspage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   driver.navigate().back();
					   ExtentReport.ExtentReportInfoLog("Going back to Wireless Earbuds List");
					   cm.waitMethod();
					   list.clear();
					   
				 //  }
				   
				  
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Flair Xl is sold out");
			 
		   }
		   
		
		   
	   }
	
	
	@Test(description = "Verify Tune Elite Sport")	
	   public void TC_verifyTuneEliteSport(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyTuneEliteSport",
					"Verify Tune Elite Sport");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			
           /***** code added 20/07/2022 *********/
		   
		   	  String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.gettuneelitesportheader()).getText(), "Tune Elite Sport", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Tune Elite Sport Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Tune Elite Sport is sold out");
			 
		   }
		   
		  
	   }
	
	   @Test(description = "Verify Nerve")	
	   public void TC_verifyNerve(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNerve",
					"Verify Nerve");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			  // String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   
		   	/***** code added 20/07/2022 *********/
		   
		   		String name1=name.toLowerCase();
		   		//span[contains(text(),'ColorFit Pulse Grand')]
		   		String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.getNerveheader()).getText(), "Nerve", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Nerve Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Nerve is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Tune Active")	
	   public void TC_verifyTuneActive(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyTuneActive",
					"Verify Tune Active");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			  
             /***** code added 20/07/2022 *********/
		   
		      String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.gettuneactiveheader()).getText(), "Tune Active", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Tune Active Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Tune Active is sold out");
			 
		   }
		   
		  
	   }
	
	
	@Test(description = "Verify Sense")	
	   public void TC_verifySense(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySense",
					"Verify Sense");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   //String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   	/***** code added 20/07/2022 *********/
		   
		   	String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			String xpath="//span[text()='"+name1+"']/parent::a";
			   
			/***** code added 20/07/2022 *********/
		   
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.getSenseheader()).getText(), "Sense", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Sense Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Sense is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Bravo")	
	   public void TC_verifyBravo(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBravo",
					"Verify Bravo");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   //String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   	/***** code added 20/07/2022 *********/
		   
		   	String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			String xpath="//span[text()='"+name1+"']/parent::a";
			   
			/***** code added 20/07/2022 *********/
		   
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.getBravoheader()).getText(), "Bravo", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Bravo Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Bravo is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Combat Gaming Neckband")	
	   public void TC_verifyCombat(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyCombat",
					"Verify Combat Gaming Neckband");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
           /***** code added 20/07/2022 *********/
		   
		      String name1=name.toLowerCase();
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/  
		   
		   
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.getcombatheader()).getText(), "Combat Gaming Neckband", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Combat Gaming Neckband Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Combat Gaming Neckband is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Tune Charge")	
	   public void TC_verifyTuneCharge(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyTuneCharge",
					"Verify Tune Charge");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			  // String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   
		   		/***** code added 20/07/2022 *********/
		   
		   		String name1=name.toLowerCase();
		   		//span[contains(text(),'ColorFit Pulse Grand')]
		   		String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
		   
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.gettunechargeheader()).getText(), "Tune Charge", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Tune Charge Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Tune Charge is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Tune Active Plus")	
	   public void TC_verifyTuneActivePlus(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyTuneActivePlus",
					"Verify Tune Active Plus");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.gettuneactiveplusheader()).getText(), name, "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Tune Active Plus Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Tune Active Plus is sold out");
			 
		   }
		   
		  
	   }
	
	@Test(description = "Verify Tune Sport 2")	
	   public void TC_verifyTuneSport2(WebDriver driver,String name) throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyTuneSport2",
					"Verify Tune Sport 2");
		   
		   
		   System.out.println("name"+name);
			 //span[contains(text(),'ColorFit Pulse Grand')]
			   //String xpath="//span[text()='"+name+"']/parent::a";
			  // System.out.println("xpath:"+xpath);
		   
		   	/***** code added 20/07/2022 *********/
		   
		   		String name1=name.toLowerCase();
		   		//span[contains(text(),'ColorFit Pulse Grand')]
		   		String xpath="//span[text()='"+name1+"']/parent::a";
			   
			   /***** code added 20/07/2022 *********/
			   cm=new commonutilmethods(driver);
		   
		  
		   try {
				   //if(cm.existsElement(Wirelessearbudspage.getVS103link()))
				   //{
			      driver.findElement(By.xpath(xpath)).click();
					   
					   cm.waitMethod();
					   Assert.assertEquals(driver.findElement(BuletoothEarPhonesPage.gettunesport2header()).getText(), "Tune Sport 2", "Header Text not matched");
					   
					   ExtentReport.ExtentReportInfoLog("Redirected to Tune Sport 2 Product");
					   
					 //  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					   //cm.waitMethod();
					  
					   //System.out.print(values_constant.setBrioColors().get(0));
					   int licount=driver.findElements(SmartWatchesPage.getcolorvariant()).size();
					   System.out.println("list count:"+licount);
					   
				
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   String colortext;
					   list = new ArrayList<String>();
					   list=cm.valueaddinarray(driver, licount);
					   
					   System.out.println("list size:"+list.size());
					   
					   System.out.println("list element:"+list.get(0));
					   
					   /******************  Fetch the color text and insert in array      *******************/
					   
					   //List<WebElement> allElements = driver.findElements(By.xpath("//div[@id='product-bar']//ul/li/span")); 
					   
					  // System.out.println("licount"+licount);
					 
					   
					   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li[1]/label")).click();
					  
					   int count=list.size();
					   
					   cm.waitMethod();
					  
					   if(licount==count)
						   {
							   for(int i=0;i<licount;i++)
							   {
								  
								   try
								   {
									  expected =list.get(i);
									//  driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+1)+"]/label")).click();
									   actual = driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
									   Assert.assertEquals(actual,expected);
									  
									   System.out.println("color matched:"+actual);
									   ExtentReport.ExtentReportInfoLog("Actual color matched with "+expected);
									   cm.waitMethod();
									 
									   try
									   {
										   if(cm.existsElement(SmartWatchesPage.getnotifymeold()))
										   {
											   ExtentReport.ExtentReportInfoLog("Notify Me is present for the variant");
											   driver.findElement(SmartWatchesPage.getnotifymeold()).click();
											   cm.waitMethod();
											   driver.findElement(SmartWatchesPage.getnotifyclose()).click();
											   cm.waitMethod();
											   ExtentReport.ExtentReportInfoLog("Notify Me popup opened and closed");
										   }
										
										
									   }
									   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Buy Now button is present for the variant");
										   
									   }
									  
									   try
									   {
										   driver.findElement(By.xpath("//div[@id='product-bar']//ul/li["+(i+2)+"]/label")).click();
										   ExtentReport.ExtentReportInfoLog("Next color is clicked");
										   cm.waitMethod();
									   }
										   
									   catch(Exception ex)
									   {
										   ExtentReport.ExtentReportInfoLog("Next label color is not found or clicked");
										  break;
										   
									   }
								   }
								   
								   catch(Exception ex)
								   {
									   ExtentReport.ExtentReportInfoLog(actual+"color text is not matched with "+expected);
									   break;
								   }
								 
								  
							   }
						   }
					    else
					    {
					    	ExtentReport.ExtentReportInfoLog("No of colors are not matched");
					    }
					   
					   
					   
				   //}
				   
				   driver.navigate().back();
				   ExtentReport.ExtentReportInfoLog("Going back to Bluetooth Earphones List");
				   cm.waitMethod();
				   list.clear();
		   }
		    catch(Exception e)
		   {
		    
		    	ExtentReport.ExtentReportInfoLog("Tune Sport 2 is sold out");
			 
		   }
		   
		  
	   }
	
}
