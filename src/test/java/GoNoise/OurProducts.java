package GoNoise;

import java.util.Scanner;

import org.jsoup.select.Evaluator.IsEmpty;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import Base.Apache_POI_TC;
import pages.ContactUsPage;
import pages.NoiseHomePage;
import pages.Registercomplaintpage;
import pages.Registerwarrantypage;
import pages.trackyourorderpage;
import utility.ExtentReport;



public class OurProducts extends Apache_POI_TC
{
	
	@Test(description = "Verify click Smart Watches",priority=1)	
	   public void TC_verifyClickSmartWatches() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyClickSmartWatches",
					"Verify click Smart Watches");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		   cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getsmartwatcheslink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Smart Watches Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Smart Watches Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down below");
			
               driver.navigate().to("https://www.gonoise.com/");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
			   
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Smart Watches Link is not Clicked");
		   }
		  
		 
	   }
	
	
	  @Test(description = "Verify Bluetooth Earphones",priority=2)	
	   public void TC_verifyBluetoothEarphones() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyBluetoothEarphones",
					"Verify Bluetooth Earphones");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		 /*  cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");*/
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getearphoneslink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Bluetooth Earphones Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Bluetooth Earphones Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down");
				
			   
		     driver.navigate().to("https://www.gonoise.com/");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Bluetooth Earphones Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	  @Test(description = "Verify Wireless Earbuds",priority=3)	
	   public void TC_verifyWirelessEarbuds() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyWirelessEarbuds",
					"Verify Wireless Earbuds");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		 /*  cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");*/
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getwirelessearbudslink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Wireless Earbuds Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Wireless Earbuds Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down");
				
			   
		     driver.navigate().to("https://www.gonoise.com/");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Wireless Earbuds Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	  
	  @Test(description = "Verify Noisefit App",priority=4)	
	   public void TC_verifyNoiseFitApp() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyNoiseFitApp",
					"Verify Noisefit App");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		 /*  cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");*/
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getnoisefitapplink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("NoiseFit App Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("NoiseFit App Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down");
				
			   
		       driver.navigate().to("https://www.gonoise.com/");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("NoiseFit App Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	  @Test(description = "Verify Accessories",priority=5)	
	   public void TC_verifyAccessories() throws InterruptedException
	   {
		   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyAccessories",
					"Verify Accessories");
		   
		   /****************** Click on not now in Popup *****************/
		   
		   System.out.println(driver.findElements(By.id("desktopBannerWrapped")).size());
		   
		   cm.waitMethod();
		   
			 if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
			 
			 /****************** Click on not now in Popup *****************/
		   
		 /*  cm.scrolldown();
		   cm.waitMethod();
		   ExtentReport.ExtentReportInfoLog("Scroll down to footer");*/
		   
		   
		   try {
			   
			   driver.findElement(NoiseHomePage.getaccessorieslink()).click();
			   
			   ExtentReport.ExtentReportInfoLog("Accessories Link is Clicked");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Accessories Page is opened");
			   
			   cm.scrolldown();
			   cm.waitMethod();
			   ExtentReport.ExtentReportInfoLog("Scroll down");
				
			   
		       driver.navigate().to("https://www.gonoise.com/");
			   
			   cm.waitMethod();
			   
			   ExtentReport.ExtentReportInfoLog("Redirected back to Home Page");  
				   
			
		   }
		   
		   catch(Exception e)
		   {
			   ExtentReport.ExtentReportInfoLog("Accessories Link is not Found nor clicked");
		   }
		  
		 
	}	
	  
	
}
