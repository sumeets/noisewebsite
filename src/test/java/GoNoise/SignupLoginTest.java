package GoNoise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner; 
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.java_cup.internal.runtime.Symbol;

import Base.Apache_POI_TC;
import pages.AccountLoginpage;
import pages.NoiseHomePage;
import pages.ProductCartCheckoutPage;
import utility.ExtentReport;
import utility.GetContentFromExcelSheets;
import utility.commonutilmethods;

public class SignupLoginTest 
{
	String firstname,lastname,password,confpass,email,phone, username, pwd,otp;
	
	BufferedReader br;
	
	Scanner obj;
	
	commonutilmethods cm;
	
  /* @Test(enabled=false,description = "Verify sign up functionality",priority=1)	
   public void TC_verifySignUp() throws InterruptedException, IOException
   {
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifySignUp",
				"Verify sign up functionality");
	   
	   
	   
	   driver.get("https://www.gonoise.com/account/login");
	   
	   cm.waitMethodnew();
	   
	   cm.waitForElement(AccountLoginpage.getSignUpLink()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Sign Up Link is clicked");
	  
	   
       List<String> signupdata=new ArrayList<String>();
	   
	   signupdata=GetContentFromExcelSheets.readData("excelFileContents.xlsx",0);
	   
	   System.out.println(signupdata.size());

	    firstname=signupdata.get(0);
	    lastname=signupdata.get(1);
	    password=signupdata.get(2);
	    confpass=signupdata.get(3);
	    
	    cm.waitForElement(AccountLoginpage.getFirstName()).sendKeys(firstname);
	    ExtentReport.ExtentReportInfoLog("First name is entered");
	    
	    cm.waitForElement(AccountLoginpage.getLastName()).sendKeys(lastname);
	    
	    ExtentReport.ExtentReportInfoLog("Last name is entered");
	    
	    cm.waitForElement(AccountLoginpage.getPass()).sendKeys(password);
	    
	    ExtentReport.ExtentReportInfoLog("Password is entered");
	    cm.waitForElement(AccountLoginpage.getConfPass()).sendKeys(confpass);
	    
	    ExtentReport.ExtentReportInfoLog("Confirm Password is entered");
	    
	   
	    Scanner userInput = new Scanner(System.in);
	    
	    System.out.println("Enter email id");
	    email=userInput.nextLine();
	    System.out.println(email);
	    
	    
	    cm.waitForElement(AccountLoginpage.getEmail()).sendKeys(email);
	    
	    ExtentReport.ExtentReportInfoLog("Email is entered");
	    
	    
        Scanner userInput1 = new Scanner(System.in);
	    
	    System.out.println("Enter phone no");
	    phone=userInput1.nextLine();
	    System.out.println(phone);
	    
	    
	    cm.waitForElement(AccountLoginpage.getPhoneNo()).sendKeys(phone);
	    userInput.close();
	    userInput1.close();
	    
	    
	    ExtentReport.ExtentReportInfoLog("Phone is entered");
	    cm.waitMethod();
	    
	    cm.waitForElement(AccountLoginpage.getSignUpButton()).click();
	    
	    cm.waitMethod();
	    
	    
	    ExtentReport.ExtentReportInfoLog("Sign Up button clicked");
	    ExtentReport.ExtentReportInfoLog("All data filled in Sign Up Form and go to next page");
	    
	    
	    /****************Otp *******************/
	    
       /* Scanner otpInput = new Scanner(System.in);
	    
	    System.out.println("Enter Otp");
	    otp=otpInput.nextLine();
	    System.out.println(otp);
	    
	    cm.waitForElement(AccountLoginpage.getOtp()).sendKeys(otp);
	    otpInput.close();
	    
	    ExtentReport.ExtentReportInfoLog("Otp Entered");
	   
	    cm.waitForElement(AccountLoginpage.getVerifyButton()).click();
	    
	    ExtentReport.ExtentReportInfoLog("Verify button is clicked");
	    
	    ExtentReport.ExtentReportInfoLog("New User is registered");
	    
	    
	    /****************Otp *******************/
	    
	    
	   
 //  }
   
   
   
   @Test(description = "Verify login functionality")	
   public void TC_verifyLogin(WebDriver driver) throws InterruptedException, IOException
   {
	   
	   cm=new commonutilmethods(driver);
	   
	   ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_verifyLogin",
				"Verify login functionality");
	   
       driver.get("https://www.gonoise.com/account/login");
	   
	   cm.waitMethod();
	   ExtentReport.ExtentReportInfoLog("Sign in Page is opened");
	   
	   /****************** Click on not now in Popup *****************/
	   
	   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
		 {
			 driver.findElement(By.id("moe-dontallow_button")).click();
			   cm.waitMethod();
		 }
	  
	  
	  /****************** Click on not now in Popup *****************/
	   
	   Scanner userInput1 = new Scanner(System.in);
	    
	    System.out.println("Enter Email Id");
	    username=userInput1.nextLine();
	    System.out.println(username);
	   
	   cm.waitForElement(AccountLoginpage.getEmailLogin()).sendKeys(username);
	   
	   ExtentReport.ExtentReportInfoLog("Email Id entered");
	   
	   Scanner userInput2 = new Scanner(System.in);
	    
	    System.out.println("Enter Password");
	    pwd=userInput2.nextLine();
	    System.out.println(pwd);
	   
	   cm.waitForElement(AccountLoginpage.getPwdLogin()).sendKeys(pwd);
	   
	   ExtentReport.ExtentReportInfoLog("password entered");
	   
	   cm.waitForElement(AccountLoginpage.getbtnSignin()).click();
	   
	   ExtentReport.ExtentReportInfoLog("Loggedin successfully");
	   
	   cm.waitMethod();
	   
	 /*  cm.waitForElement(AccountLoginpage.getSignOut()).click();
	   
	   cm.waitMethod();
	   
	   ExtentReport.ExtentReportInfoLog("Loggedout successfully");*/
	   
	   
	  
   }
  
   
   
 }
  

