package Datadriven;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.google.common.base.Verify;

import Base.Apache_POI_TC;
import GoNoise.HeadPhonesDynamic;
import GoNoise.WirelessearbudsDynamic;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.commonutilmethods;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class allBluetoothHeadphones  extends Apache_POI_TC{
	//String filePath = "../exceldata/APIExcelSheet.xlsx";
	int status;
	ITestResult result;
	ArrayList list;
	
	
	
	
	@Test
	public void getAllHeadPhonesCall() throws IOException, InterruptedException {
		
		  ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_getAllHeadPhonesCall",
					"Verify All Bluetooth Headphones");
		  
		   cm.waitMethod();
		  
		  
		  /****************** Click on not now in Popup *****************/
		   
		   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
		  
		  
		  /****************** Click on not now in Popup *****************/
		  
		  WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.findElement(ProductCartCheckoutPage.getheadphones()).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("Headphones Link is clicked");
		   
          /********************** scroll down and Load all products ******************/
		   
		   cm.scrolldownone();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		

	     int productscount=driver.findElements(SmartWatchesPage.getproductnames()).size();
		 System.out.println("no of products"+productscount);

		 if(productscount>0)
		{
			 
			 list = new ArrayList<String>();
			   list=cm.productsadd(driver, productscount);
			   System.out.println("product list count"+list.size());
			   
			   System.out.println("first product"+list.get(0));
			   System.out.println("first product"+list.get(1));
			   HeadPhonesDynamic hd=new HeadPhonesDynamic();
			  // SmartWatchesLoader3 sm=new SmartWatchesLoader3();
			   
			  for(int i=0;i<list.size();i++)
			   {
				   String name=list.get(i).toString();
				   System.out.println(name);
				   if(name.equalsIgnoreCase("Noise Powr"))
				   {
					  // System.out.println("hi");
					   
					   
					   hd.TC_verifyNoisePowr(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("One Headphones"))
				   {
					  // System.out.println("hi");
					   
					   
					   hd.TC_verifyOneHeadPhones(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   else if(name.equalsIgnoreCase("Defy ANC Headphones"))
				   {
					  // System.out.println("hi");
					   
					   
					   hd.TC_verifyDefyANCHeadphones(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				  
				 
			   }
			 
		}
		 else
			{
				ExtentReport.ExtentReportInfoLog("No Products Found");
			}
			
			  
                

			
		

	}
	
	
	
}
	


