package Datadriven;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.google.common.base.Verify;

import Base.Apache_POI_TC;
import GoNoise.AccessoriesDynamic;
import GoNoise.WirelessearbudsDynamic;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.commonutilmethods;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class allaccessories  extends Apache_POI_TC{
	//String filePath = "../exceldata/APIExcelSheet.xlsx";
	int status;
	ITestResult result;
	ArrayList list;
	
	
	
	
	@Test
	public void getAllAccessories() throws IOException, InterruptedException {
		
		  ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_getAllAccessories",
					"Verify All Accessories");
		  
		   cm.waitMethod();
		  
		  
		  /****************** Click on not now in Popup *****************/
		   
		   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
		  
		  
		  /****************** Click on not now in Popup *****************/
		  
		  WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.navigate().to("https://www.gonoise.com/collections/accessories");
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("Accessories Page is opened");
		   
          /********************** scroll down and Load all products ******************/
		   
		   cm.scrolldownone();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		

	     int productscount=driver.findElements(SmartWatchesPage.getproductnames()).size();
		 System.out.println("no of products"+productscount);

		 if(productscount>0)
			{

			
			   list = new ArrayList<String>();
			   list=cm.productsadd(driver, productscount);
			   System.out.println("product list count"+list.size());
			   
			  // System.out.println("first product"+list.get(0));
			  // System.out.println("first product"+list.get(1));
			   AccessoriesDynamic ad=new AccessoriesDynamic();
			  // SmartWatchesLoader3 sm=new SmartWatchesLoader3();
			   
			    
				   for(int i=0;i<list.size();i++)
				   {
					   String name=list.get(i).toString();
					   System.out.println(name);
					   if(name.equalsIgnoreCase("classic silicone 19mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyClassicSilicon19mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   else if(name.equalsIgnoreCase("classic leather 22mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyClassicLeather22mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("premium silicone 22mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyPremiumSilicone22mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("classic silicone 20mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyClssicSilicone20mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("Classic Nylon 22mm (Strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyClssicNylon22mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("magnetic leather 22mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyMagneticLeather22mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("metallic link 22mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyMetallicLink22mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("uni weave 22mm - xs (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyUniWeave22mmxs(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("pride edition 22mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyPrideEdition22mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("sports edition 22mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifySportsEdition22mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("uni weave 22mm - large (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyUniWeave22mmLarge(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("uni weave 22mm - small (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyUniWeave22mmSmall(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("magnetic leather 20mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyMagneticLeather20mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("metallic link 20mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyMetallicLink20mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("uni weave 20mm - l (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyUniweave20mml(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("uni weave 20mm - s (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyUniweave20mms(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("uni weave 20mm - xs (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyUniweave20mmxs(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("striped silicone x-fit 1 strap (20 mm)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifySiliconexfit1(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("Classic Nylon 20mm (Strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyClassicNylon20mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("sports edition 20mm (strap)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifySportsEdition20mm(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("noise smartwatch charging dock type 4 (for colorfit ultra 2)"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyChargingDockType4(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
					   
					   else if(name.equalsIgnoreCase("noise magnetic charging cable"))
					   {
						  // System.out.println("hi");
						   
						   
						   ad.TC_verifyMagneticChargingCable(driver,name);
						   cm.scrolldownone();
						   cm.waitMethod();
						   cm.scrollup();
						   cm.waitMethod();
						   
					   }
				   
				   
				   
			        }
			   
			
	}
		 
	 else
		{
			ExtentReport.ExtentReportInfoLog("No Products Found");
		}

	
}
}
	


