package Datadriven;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.google.common.base.Verify;

import Base.Apache_POI_TC;
import GoNoise.SmartWatchesLoader2;
import GoNoise.SmartWatchesDynamic;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.commonutilmethods;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class allsmartwatches  extends Apache_POI_TC{
	//String filePath = "../exceldata/APIExcelSheet.xlsx";
	int status;
	ITestResult result;
	ArrayList list;
	
	
	
	

	/*public int getApiCall(String url) {
		
		ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_Smartwatches",
				"Verify Classic Nylon 22mm Strap");

	
	
	}*/
	
	@Test
	public void getAllWatchesCall() throws IOException, InterruptedException {
		
		  ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_getAllWatchesCall",
					"Verify All Smart Watches");
		  
		   cm.waitMethod();
		  
		  
           /****************** Click on not now in Popup *****************/
		   
		   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
		  
		  
		  /****************** Click on not now in Popup *****************/
		  
		  WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.findElement(ProductCartCheckoutPage.getViewAllsmartwatches()).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Smart Watches");
		   
          /********************** scroll down until all products loaded ******************/
		   
		  /* cm.scrolldown();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();*/
		   
		   cm.scrolldownone();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		  
		   
		   /********************** scroll down until all products loaded ******************/ 
		   
		

	int productscount=driver.findElements(SmartWatchesPage.getproductnames()).size();
		System.out.println("no of products"+productscount);

		if(productscount>0)
		{
			
			   list = new ArrayList<String>();
			   list=cm.productsadd(driver, productscount);
			   System.out.println("product list count"+list.size());
			   
			   System.out.println("first product"+list.get(0));
			   System.out.println("first product"+list.get(1));
			   SmartWatchesDynamic sm=new SmartWatchesDynamic();
			  // SmartWatchesLoader3 sm=new SmartWatchesLoader3();
			   
		     for(int i=0;i<list.size();i++)
			   {
				   String name=list.get(i).toString();
				   System.out.println(name);
				  if(name.equalsIgnoreCase("colorfit ultra buzz"))
				   {
					  // System.out.println("hi");
					   
					   
					   sm.TC_verifyUltraBuzz(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("ColorFit Pulse Grand"))
				   {
					  // System.out.println("hi");
					   
					   
					   sm.TC_verifyPulseGrand(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("colorfit ultra 2"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitUltra2(driver, name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("colorfit pro 3 alpha"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitPro3Alpha(driver,name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else  if(name.equalsIgnoreCase("colorfit caliber"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitCaliber(driver,name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("noisefit evolve 2"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyNoisefitEvolve2(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("colorfit pulse"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitPulse(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				/*   if(name.equalsIgnoreCase("ColorFit Icon Buzz"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifySmartWatchColorfitIconBuzz(driver, name);
					   
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   
				  else if(name.equalsIgnoreCase("ColorFit Ultra"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitUltra(driver, name);
					   
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }*/
				   else if(name.equalsIgnoreCase("x-fit 1 (hrx edition)"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyXFit1(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("noisefit core oxy"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyCoreOxy(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				  /* else if(name.equalsIgnoreCase("ColorFit Pro 3 Assist"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitPro3Assist(driver, name);
					   
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }*/
				  else if(name.equalsIgnoreCase("colorfit brio"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitBrio(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   
				   else if(name.equalsIgnoreCase("noise champ kids smartband"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyChampKids(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   
				   else if(name.equalsIgnoreCase("colorfit pro 3"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitPro3(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   
				  /* else if(name.equalsIgnoreCase("ColorFit Pro 2"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitPro2(driver, name);
					   
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   
				   else if(name.equalsIgnoreCase("NoiseFit Endure"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyNoisefitEndure(driver, name);
					   
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }*/
				   
				  else if(name.equalsIgnoreCase("colorfit pro 2 oxy"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitPro2Oxy(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("noisefit active"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyNoisefitActive(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   
				   else if(name.equalsIgnoreCase("noisefit agile"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyNoisefitAgile(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("colorfit nav+"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitNavPlus(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("colorfit beat"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitBeat(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("colorFit vision"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitVision(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("colorFit nav"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyColorfitNav(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("noisefit buzz"))
				   {	
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyNoisefitBuzz(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				   else if(name.equalsIgnoreCase("noise excel"))
				   {
					  // System.out.println("hello");
					   
					  
					   sm.TC_verifyNoiseExcel(driver, name);
					   
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
				   }
				 
				 
			   }
                

			
		

	}
		
	else
	{
		ExtentReport.ExtentReportInfoLog("No Products Found");
	}
	
	}
	
}
	


