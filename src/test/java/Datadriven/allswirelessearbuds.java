package Datadriven;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.google.common.base.Verify;

import Base.Apache_POI_TC;

import GoNoise.WirelessearbudsDynamic;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import pages.ProductCartCheckoutPage;
import pages.SmartWatchesPage;
import utility.ExtentReport;
import utility.commonutilmethods;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class allswirelessearbuds  extends Apache_POI_TC{
	//String filePath = "../exceldata/APIExcelSheet.xlsx";
	int status;
	ITestResult result;
	ArrayList list;
	
	
	
	
	@Test
	public void getAllEarbudsCall() throws IOException, InterruptedException {
		
		  ExtentReport.extentlog = ExtentReport.extentreport.startTest("TC_getAllEarbudsCall",
					"Verify All Wireless Earbuds");
		  
		   cm.waitMethod();
		  
		  
		  /****************** Click on not now in Popup *****************/
		   
		   if( !driver.findElements(By.id("desktopBannerWrapped")).isEmpty())
			 {
				 driver.findElement(By.id("moe-dontallow_button")).click();
				   cm.waitMethod();
			 }
		  
		  
		  /****************** Click on not now in Popup *****************/
		  
		  WebElement mainMenu= driver.findElement(ProductCartCheckoutPage.getProductLink());
		   cm.hoverElement(mainMenu);
		   
		   cm.waitMethod();
		   
		   driver.findElement(ProductCartCheckoutPage.getViewAllEarBuds()).click();
		   
		   cm.waitMethod();
		   
		   ExtentReport.ExtentReportInfoLog("View all Link is Clicked for Wireless Earbuds");
		   
          /********************** scroll down and Load all products ******************/
		   
		   cm.scrolldownone();
		   cm.waitMethod();
		   cm.scrollup();
		   cm.waitMethod();
		   
		   
		   /********************** scroll down and Load all products ******************/ 
		   
		

	     int productscount=driver.findElements(SmartWatchesPage.getproductnames()).size();
		 System.out.println("no of products"+productscount);
		 if(productscount>0)
			{
		
			
			   list = new ArrayList<String>();
			   list=cm.productsadd(driver, productscount);
			   System.out.println("product list count"+list.size());
			   
			  // System.out.println("first product"+list.get(0));
			  // System.out.println("first product"+list.get(1));
			   WirelessearbudsDynamic wd=new WirelessearbudsDynamic();
			  // SmartWatchesLoader3 sm=new SmartWatchesLoader3();
			   
			  for(int i=0;i<list.size();i++)
			   {
				   String name=list.get(i).toString();
				   System.out.println(name);
				   if(name.equalsIgnoreCase("Buds Vs202"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyBudsVS202(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("Buds Vs104"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyBudsVS104(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				  else if(name.equalsIgnoreCase("Buds Vs103"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyBudsVS103(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   else if(name.equalsIgnoreCase("Air Buds Pro"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyAirBudsPro(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   /*else if(name.equalsIgnoreCase("Air Buds+"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyAirbudsplus(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }*/
				   else if(name.equalsIgnoreCase("Buds Vs303"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyBudsVS303(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("Buds Ace"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyBudsAce(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				  /* else if(name.equalsIgnoreCase("Buds VS102"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyVS102(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("Buds VS201"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyBudsVS201(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   else if(name.equalsIgnoreCase("Shots Neo 2"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyShotsNeo2(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   else if(name.equalsIgnoreCase("Buds Solo"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyBudsSolo(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   else if(name.equalsIgnoreCase("Noise Beads"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyNoiseBeads(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   else if(name.equalsIgnoreCase("Air Buds Mini"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyAirbudsMini(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   else if(name.equalsIgnoreCase("Air Buds"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyAirbuds(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				   
				   else if(name.equalsIgnoreCase("Shots Rush"))
				   {
					  // System.out.println("hi");
					   
					   
					   wd.TC_verifyShotsRush(driver,name);
					   cm.scrolldown();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }*/
				   
				   else if(name.equalsIgnoreCase("Buds Smart"))
				   {
					  // System.out.println("hi");
					   
					   wd.TC_verifyBudsSmart(driver,name);
					   cm.scrolldownone();
					   cm.waitMethod();
					   cm.scrollup();
					   cm.waitMethod();
					   
				   }
				 
			   }
                

			
		

	}
		 
 else
	{
		ExtentReport.ExtentReportInfoLog("No Products Found");
	}
	
	}
	
}
	


