package Base;

import org.apache.commons.mail.EmailException;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
//import org.openqa.selenium.support.ui.WebDriverWait;
//import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;

import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.remote.MobileCapabilityType;
//import pages.HomePage;
import utility.Email;
import utility.ExtentReport;
import utility.ScreenshotUtility;
import utility.StringUtil;
import utility.commonutilmethods;
import utility.constant;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
//import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.RemoteWebDriver;
import java.net.URL;
//import org.apache.log4j.xml.DOMConfigurator;
//import DbConnection.connection;
import java.util.Date;
import java.util.Properties;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.service.local.AppiumDriverLocalService;

public class Apache_POI_TC {
		
	static Date now = new Date();
	public static String TimeStamp = now.toString().replace(":", "-");
	
	
	 public static WebDriver driver;
	 
	 
	// protected HomePage homePage;
	
	 public static FileInputStream fis;
	 Properties prop;
	 
	 public commonutilmethods cm;
	 
	
	
 @BeforeSuite
	public void setUpSuite()
	{
	 
	 try {
			ExtentReport.initialize(System.getProperty("user.dir")+"/Reports/"+ TimeStamp+" Gonoise_Automation.html");
			//fis=new FileInputStream(System.getProperty("user.dir")+"\\resources\\global.properties");
			//extent=new ExtentReports(System.getProperty("user.dir")+"\\Reports")
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	 
	
@BeforeTest
public void Execution() throws Exception 
{


	
      
//	DOMConfigurator.configure("log4j.xml");
	
	if(constant.PlatForm_Name.equals("web"))
	{ 
		
		if(constant.Project_Name.equals("Noise"))
		{
			fis=new FileInputStream(System.getProperty("user.dir")+"\\resources\\global.properties");
			prop=new Properties();
			prop.load(fis);
			
			String url=(String)prop.get("url");
		    
			 System.setProperty("webdriver.chrome.driver","./resources/chromedriver.exe");
			 
			 /*ChromeOptions option=new ChromeOptions();
			 option.setPageLoadStrategy(PageLoadStrategy.NONE);
			 driver = new ChromeDriver(option);*/
			 
			 driver=new ChromeDriver();
		     
		     driver.manage().deleteAllCookies();
		     driver.get(url);
			driver.manage().window().maximize();
			cm=new commonutilmethods(driver);
		
		       
		} else if (constant.Project_Name.equals("testwebsite"))
		{
			
			
		}
		
		
	}  
	
	else if(constant.PlatForm_Name.equals("app")){
		
		
	
						if(constant.Project_Name.equals("testapk"))
						{
						
						/*	prop=new Properties();
							prop.load(fis);
							
							 File appDir = new File("src");
						     File app = new File(appDir,(String)prop.get("APPName"));
						     
						     DesiredCapabilities capabilities = new DesiredCapabilities();
						     
						     String device=(String) prop.get("device");
						     
						    /* if(device.contains("emulator"))
						     {
						        startEmulator();
						     }*/
						     
						    /* capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, device);
						     //capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android Device");
						     capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
						     System.setProperty("webdriver.http.factory", "apache");
						     driver1 = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
				             Thread.sleep(5000);*/
					         
					      
						}else if(constant.Project_Name.equals("Testapkone"))
									{ 
								      // Run This Code---------------
									  System.out.println("Empty Result");
										    	   
									}
	}
	
	else if(constant.PlatForm_Name.equals("API")){
		
		if(constant.Project_Name.equals("EvaluatorREST"))
		{
			
			System.out.println("Rest Assured Project");
		}
	}
	
} 


/*
@AfterMethod
public void tearDownMethod(ITestResult result) throws IOException
{
	
	
	 if (result.getStatus() == ITestResult.FAILURE) {
		 

		 ScreenshotUtility.getScreenshot(driver);
		
		 System.out.println(result.getName()+"Test Case is Fail");
	        
	        
	        
	    } else if (result.getStatus() == ITestResult.SUCCESS) {
	    	System.out.println(result.getName()+"Test Case is Pass");
	    } else {
	    	System.out.println(result.getName()+"Test Case is Skipped");
	    }

	
	
}*/

@AfterMethod
public void afterMethod(ITestResult result) throws InterruptedException, IOException {
	
		if (result.getStatus() == ITestResult.SUCCESS) {
			ExtentReport.extentlog.log(LogStatus.PASS, "Test case: " + result.getName()+" is passed " );
			
            
		} else if (result.getStatus() == ITestResult.FAILURE) {
			
			String methodName=StringUtil.createRandomString(result.getMethod().getMethodName());
			System.out.println("METHOD........"+methodName);
			//String screenShotPath=ScreenshotUtility.captureScreenshot(driver,methodName);
			
			String base64Screenshot = ScreenshotUtility.getBase64Screenshot(driver, methodName);
			
			//MediaEntityModelProvider mediaModel = MediaEntityBuilder.createScreenCaptureFromBase64String(base64Screenshot).build();
			
			ExtentReport.extentlog.log(LogStatus.FAIL, "Test case is failed " + result.getName());
			ExtentReport.extentlog.log(LogStatus.FAIL, "Test case is failed " + result.getThrowable());
			
			//Reporter.log("<a href=\"" + "screenShotPath" +"\" target=\"_blank\">View Screenshot</a><br>");
			
			//ExtentReport.extentlog.log(LogStatus.FAIL, "Snapshot below: " + ExtentReport.extentlog.addScreenCapture(screenShotPath));
			
			
			ExtentReport.extentlog.log(LogStatus.FAIL, "Snapshot below: " + ExtentReport.extentlog.addBase64ScreenShot(base64Screenshot));
			
		} else if (result.getStatus() == ITestResult.SKIP) {
			ExtentReport.extentlog.log(LogStatus.SKIP, "Test case is Skiped " );
		}
		
		//ExtentReport.extentreport.endTest(ExtentReport.extentlog);
	
		Reporter.setCurrentTestResult(result);
		
		

}



@AfterTest
public void tearDown()
{
	try
	{
		Reporter.getCurrentTestResult();
		driver.quit();
		
		//driver.close();
		/** try catch ***/
		
        
	}
	
	catch(Exception e)
	{
		System.out.println("Driver not closed:"+e.getMessage());
	}
}



@AfterSuite
public void EndSuite() throws EmailException
{
	ExtentReport.extentreport.endTest(ExtentReport.extentlog);
   
	ExtentReport.extentreport.flush();
	//ExtentReport.extentreport.close();
	System.out.println("Close ExtentReport");
	System.out.println("after suite");

	
}

}