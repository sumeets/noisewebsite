package pages;

import org.openqa.selenium.By;


public class StoriesPage {
	
	
	private static By viewmorelink = By.xpath("(//button[contains(text(),'View More')])[1]");
	
	
	 public static By getviewmorelink()
	  {
		  return viewmorelink;
	  }
	
	 
}
