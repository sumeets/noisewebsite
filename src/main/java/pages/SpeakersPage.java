package pages;

import org.openqa.selenium.By;


public class SpeakersPage {
	
	
	private static By vibe5wspeakerlink = By.xpath("//span[text()='Vibe 5W Speaker']/parent::a");
	
	private static By zest3wspeakerlink = By.xpath("//span[text()='Zest 3W Speaker']/parent::a");
	
	private static By zest5wspeakerlink = By.xpath("//span[text()='Zest 5W Speaker']/parent::a");
	
	private static By vibe5wspeakerheader = By.xpath("//h5[contains(text(),'Vibe 5W Speaker')]");
	
	private static By zest3wspeakerheader = By.xpath("//h5[contains(text(),'Zest 3W Speaker')]");
	
	private static By zest5wspeakerheader = By.xpath("//h5[contains(text(),'Zest 5W Speaker')]");
	
	 public static By getvibe5wspeakerlink()
	  {
		  return vibe5wspeakerlink;
	  }
	 
	 public static By getzest3wspeakerlink()
	  {
		  return zest3wspeakerlink;
	  }
	 
	 public static By getzest5wspeakerlink()
	  {
		  return zest5wspeakerlink;
	  }
	 
	 public static By getvibe5wspeakerheader()
	  {
		  return vibe5wspeakerheader;
	  }
	 public static By getzest3wspeakerheader()
	  {
		  return zest3wspeakerheader;
	  }
	 
	 public static By getzest5wspeakerheader()
	  {
		  return zest5wspeakerheader;
	  }
	 
}
