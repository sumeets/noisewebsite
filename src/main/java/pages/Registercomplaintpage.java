package pages;

import org.openqa.selenium.By;


public class Registercomplaintpage {
	
	
	
	
	private static By Nextbutton = By.xpath("//button[contains(text(),'Next')]");
	
	private static By OTPTxtbox = By.xpath("//input[@placeholder='Enter OTP']");
	
	private static By verifyOTPButton = By.xpath("//button[contains(text(),'Verify OTP')]");
	
	private static By shippingaddress = By.xpath("//input[@placeholder='Enter your shipping address']");
	
	private static By landmark = By.xpath("//input[@placeholder='Enter your landmark']");
	
	private static By pincode = By.xpath("//input[@placeholder='Enter your pincode']");
	
	private static By Submitbutton = By.xpath("//button[contains(text(),'Submit Complaint')]");
	
	 public static By getNextbutton()
	  {
		  return Nextbutton;
	  }
	 
	 public static By getOTPTxtbox()
	  {
		  return OTPTxtbox;
	  }
	 
	 
	 public static By getverifyOTPButton()
	  {
		  return verifyOTPButton;
	  }
	 
	 
	 public static By getshippingaddress()
	  {
		  return shippingaddress;
	  }
	 
	 public static By getlandmark()
	  {
		  return landmark;
	  }
	 
	 public static By getpincode()
	  {
		  return pincode;
	  }
	 
	 public static By getsubmitbutton()
	  {
		  return Submitbutton;
	  }
}
