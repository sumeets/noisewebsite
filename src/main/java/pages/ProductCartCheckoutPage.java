package pages;

import org.openqa.selenium.By;


public class ProductCartCheckoutPage {
	
	  
	  private static By ProductLink = By.xpath("//a[@class='first-level-a'][normalize-space()='Products']");

	  private static By ViewAllEarBuds = By.xpath("//li[@class='menu-new wireless-earbuds']//span[@class='menu-item-title'][normalize-space()='View all']");

	  private static By ViewAllsmartwatches = By.xpath("//a[@href='/collections/smart-watches']//span[@class='menu-item-title'][normalize-space()='View all']");
	  
	  private static By   = By.xpath("//a[@href='/collections/wireless-earphones']//span[@class='menu-item-title'][normalize-space()='View all']");

	  private static By headphones = By.xpath("//a[@href='/collections/bluetooth-headphones']");

	  private static By speakers = By.xpath("//a[@href='/collections/speakers']");
	  
	  private static By accessories = By.xpath("//span[contain");
	 
	  private static By VS303Product = By.xpath("//span[normalize-space()='Buds VS303']");
	  
	  private static By airbudsplus = By.xpath("//span[normalize-space()='Air Buds+']");
	  
	  private static By brio = By.xpath("//span[normalize-space()='ColorFit Brio']");
	  
	  private static By gotobagiconlink = By.xpath("//div[@class='cart-icon']/a");
	  
	  

	
	  
	  private static By airbudspluslink = By.xpath("//span[normalize-space()='Air Buds+']/ancestor::div[@class='text-wrapper']/following-sibling::a");
	  
	  //private static By briolinkold = By.xpath("//span[normalize-space()='ColorFit Brio']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
	  
	  private static By briolink = By.xpath("//span[normalize-space()='ColorFit Brio']/parent::a");
	  
	  private static By xfitlink = By.xpath("//span[normalize-space()='Noise X-Fit 1']/parent::a");
	  
	  private static By champskidlink = By.xpath("//span[normalize-space()='Noise Champ Kids smartband']/parent::a");
	  
	  private static By ultralink = By.xpath("//span[normalize-space()='NColorFit Ultra']/parent::a");
	  
	  private static By ultra2link = By.xpath("//span[normalize-space()='ColorFit Ultra 2']/parent::a");
	  
	 // private static By pulselink = By.xpath("//span[normalize-space()='ColorFit Pulse']/parent::a");
	  
	  private static By pulselink = By.xpath("//span[normalize-space()='ColorFit Pulse Grand']/parent::a");
	  
	  private static By qubelink = By.xpath("//span[normalize-space()='ColorFit Qube O2']/parent::a");
	  
	  private static By pro3link = By.xpath("//span[normalize-space()='ColorFit Pro 3']/parent::a");
	  
	  private static By pro3assistlink = By.xpath("//span[normalize-space()='ColorFit Pro 3 Assist']/parent::a");
	  
	  private static By airbudsminilink = By.xpath("//span[normalize-space()='Air Buds Mini']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		 
	  private static By airbudslink = By.xpath("//span[normalize-space()='Air Buds']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By budsplaylink = By.xpath("//span[normalize-space()='Buds Play']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By shotsrushlink = By.xpath("//span[normalize-space()='Shots Rush']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By VS303Productlink = By.xpath("//span[contains(text(),'Buds VS303')]/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
	 
	  private static By shotsneo2link = By.xpath("//span[normalize-space()='Shots Neo 2']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");

	  private static By elanencearbudslink = By.xpath("//span[normalize-space()='Elan ENC Earbuds']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");

	  private static By FlairProductlink = By.xpath("//span[contains(text(),'Flair')]/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		 
	  private static By TuneChargeProductlink = By.xpath("//span[contains(text(),'Tune Charge')]/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By TuneActivePluslink = By.xpath("//span[contains(text(),'Tune Active Plus')]/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By TuneActivelink = By.xpath("//span[text()='Tune Active']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	
	  private static By TuneSport2link = By.xpath("//span[text()='Tune Sport 2']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By EliteSportlink = By.xpath("//span[text()='Tune Elite Sport']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By TuneSportlink = By.xpath("//span[text()='Tune Sport']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By TuneLitelink = By.xpath("//span[text()='Tune Lite']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By Nervelink = By.xpath("//span[text()='Nerve']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");
		
	  private static By Senselink = By.xpath("//span[text()='Sense']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");

	  
	  private static By budssololink = By.xpath("//span[normalize-space()='Buds Solo']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");

	  private static By budsvs201link = By.xpath("//span[normalize-space()='Buds VS201']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");

	  private static By airbudsnanolink = By.xpath("//span[normalize-space()='Air Buds Nano']/ancestor::div[@class = 'text-wrapper']/following-sibling::a");

	  
	  private static By addtobag = By.xpath("//button[contains(text(),'Add To Bag')]");
	  
	  private static By buynow = By.xpath("//button[contains(text(),'Buy Now')]");
	  
	  private static By cartIcon = By.xpath("//img[@alt='cart']/parent::a");
	  
	  private static By contactEmail = By.xpath("//input[@id='checkout_email']");
	  
	  private static By paycard = By.xpath("//button[contains(text(),'COD / CARD')]");
	  
	  private static By applybutton = By.xpath("//button[@class='field__input-btn btn']");
	  
	  private static By applycoupon = By.xpath("//div[@class='title']");
	  
	  private static By coupontxtbox = By.xpath("//input[@name='coupon']");
	  
	  private static By btnApply = By.xpath("//input[@value='Apply']");
	  
	  private static By QtyPlusbutton = By.xpath("//div[@class='cart-spinner-button']");
	  
	  private static By ConfirmButton = By.xpath("//button[@class='continue-btn']");
	  
	  public static By getProductLink()
	  {
		  return ProductLink;
	  }
	  
	  public static By getxfitlink()
	  {
		  return xfitlink;
	  }
	  
	  public static By getbuynow()
	  {
		  return buynow;
	  }
	  
	  public static By getgotobagiconlink()
	  {
		  return gotobagiconlink;
	  }
	  
	  public static By getultra2link()
	  {
		  return ultra2link;
	  }
	  
	  public static By getheadphones()
	  {
		  return headphones;
	  }
	  
	  public static By getspeakers()
	  {
		  return speakers;
	  }
	  
	  public static By getultralink()
	  {
		  return ultralink;
	  }
	  
	  public static By getqubelink()
	  {
		  return qubelink;
	  }
	  
	  public static By getpro3link()
	  {
		  return pro3link;
	  }
	  
	  
	  
	  public static By getchampskidlink()
	  {
		  return champskidlink;
	  }
	  
	  public static By getaccessories()
	  {
		  return accessories;
	  }
	  
	  
	  public static By getpulselink()
	  {
		  return pulselink;
	  }
	  
	  
	  public static By getTuneChargeProductlink()
	  {
		  return TuneChargeProductlink;
	  }
	  
	  public static By getairbudslink()
	  {
		  return airbudslink;
	  }
	  
	  public static By getbudsplaylink()
	  {
		  return budsplaylink;
	  }
	  
	  public static By getTuneActivelink()
	  {
		  return TuneActivelink;
	  }
	  
	  public static By getTuneLitelink()
	  {
		  return TuneLitelink;
	  }
	  
	  public static By getTuneSport2link()
	  {
		  return TuneSport2link;
	  }
	  
	  public static By getNurvelink()
	  {
		  return Nervelink;
	  }
	  public static By getSenselink()
	  {
		  return Senselink;
	  }
	  
	  public static By getTuneSportlink()
	  {
		  return TuneSportlink;
	  }
	  
	  public static By getFlairProductlink()
	  {
		  return FlairProductlink;
	  }
	  
	  public static By getbudsvs201link()
	  {
		  return budsvs201link;
	  }
	  
	  public static By getTuneActivePluslink()
	  {
		  return TuneActivePluslink;
	  }
	  
	  public static By getairbudsnanolink()
	  {
		  return airbudsnanolink;
	  }
	  
	  
	  public static By getshotsrushlink()
	  {
		  return shotsrushlink;
	  }
	  
	  public static By getelanencearbudslink()
	  {
		  return elanencearbudslink;
	  }
	  
	  public static By getshotsneo2link()
	  {
		  return shotsneo2link;
	  }
	  
	  public static By getbudssololink()
	  {
		  return budssololink;
	  }
	  
	  public static By getEliteSportlink()
	  {
		  return EliteSportlink;
	  }
	  
	  public static By getViewAllEarBuds()
	  {
		  return ViewAllEarBuds;
	  }
	  
	  public static By getViewAllsmartwatches()
	  {
		  return ViewAllsmartwatches;
	  }
	  
	  
	  public static By getViewAllbluetoothearphones()
	  {
		  return ViewAllbluetoothearphones;
	  }
	  
	  
	  public static By getVS303Product()
	  {
		  return VS303Product;
	  }
	  
	  public static By getbrio()
	  {
		  return brio;
	  }
	  
	  public static By getbriolink()
	  {
		  return briolink;
	  }
	  
	  
	  public static By getVS303Productlink()
	  {
		  return VS303Productlink;
	  }
	  
	  public static By getairbudsplus()
	  {
		  return airbudsplus;
	  }
	  
	  public static By getairbudspluslink()
	  {
		  return airbudspluslink;
	  }
	  
	  
	  public static By getairbudsminilink()
	  {
		  return airbudsminilink;
	  }
	  
	  public static By getaddtobag()
	  {
		  return addtobag;
	  }
	  
	  public static By getcartIcon()
	  {
		  return cartIcon;
	  }
	  
	  public static By getcontactEmail()
	  {
		  return contactEmail;
	  }
	  
	  public static By getpaycard()
	  {
		  return paycard;
	  }
	  
	  public static By getapplybutton()
	  {
		  return applybutton;
	  }
	  
	  public static By getapplycoupon()
	  {
		  return applycoupon;
	  }
	  
	  public static By getcoupontxtbox()
	  {
		  return coupontxtbox;
	  }
	  
	  public static By getbtnApply()
	  {
		  return btnApply;
	  }
	  
	  public static By getQtyPlusbutton()
	  {
		  return QtyPlusbutton;
	  }
	  
	  public static By getConfirmButton()
	  {
		  return ConfirmButton;
	  }
	
}
