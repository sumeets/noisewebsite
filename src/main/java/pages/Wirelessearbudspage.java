package pages;

import org.openqa.selenium.By;


public class Wirelessearbudspage {
	
	
	private static By budsvs303 = By.xpath("//span[normalize-space()='Buds VS303']");
	private static By vs303header = By.xpath("//h5[contains(text(),'Buds VS303')]");
	private static By BudsAceheader = By.xpath("//h5[contains(text(),'Buds Ace')]");
	private static By airbudsplus = By.xpath("//span[normalize-space()='Air Buds+']");
	private static By airbuds = By.xpath("//span[normalize-space()='Air Buds']");
	
	private static By budsplay = By.xpath("//span[normalize-space()='Buds Play']");
	
	private static By shotsneo2 = By.xpath("//span[normalize-space()='Shots Neo 2']");
	
	private static By BudsVS201 = By.xpath("//span[normalize-space()='Buds VS201']");
	
	private static By airbudsplusheader = By.xpath("//h5[contains(text(),'Air Buds+')]");
	
	private static By BudsSmartheader = By.xpath("//h5[contains(text(),'Buds Smart')]");
	private static By BudsPrimaheader = By.xpath("//h5[contains(text(),'Buds Smart')]");
	
	private static By airbudsminiheader = By.xpath("//h5[contains(text(),'Air Buds Mini')]");
	
	private static By airbudsproheader = By.xpath("//h5[contains(text(),'Air Buds Pro')]");
	private static By airbudsheader = By.xpath("//h5[contains(text(),'Air Buds')]"); 
	private static By budsplayheader = By.xpath("//h5[contains(text(),'Buds Play')]"); 
	
	private static By VS103link = By.xpath("//span[normalize-space()='Buds VS103']/parent::a");
	
	private static By VS103header = By.xpath("//h2[contains(text(),'Buds VS103')]");
	
    private static By airbudspluslink = By.xpath("//span[normalize-space()='Air Buds+']/parent::a");
    
    private static By airbudminilink = By.xpath("//span[normalize-space()='Air Buds Mini']/parent::a");
    
    private static By VS102link = By.xpath("//span[normalize-space()='Buds VS102']/parent::a");
    
    private static By VS201link = By.xpath("//span[normalize-space()='Buds VS201']/parent::a");
    
    private static By VS202link = By.xpath("//span[normalize-space()='Buds VS202']/parent::a");
    
    private static By VS303link = By.xpath("//span[normalize-space()='Buds VS303']/parent::a");
    
    private static By VS303linknew = By.xpath("//span[normalize-space()='buds vs303']/parent::a");
	
    private static By Airbudslink = By.xpath("//span[normalize-space()='Air Buds']/parent::a");
	
	private static By shotsrushheader = By.xpath("//h5[contains(text(),'Shots Rush')]");
	
	private static By shotsneo2header = By.xpath("//h5[contains(text(),'Shots Neo 2')]");
	
	private static By elanencheader = By.xpath("//h5[contains(text(),'Elan ENC Earbuds')]");
	
	private static By budssoloheader = By.xpath("//h5[contains(text(),'Buds Solo')]");
	
	private static By noisebeadsheader = By.xpath("//h5[contains(text(),'Noise Beads')]");
	
	
	private static By VS102header = By.xpath("//h5[contains(text(),'Buds VS102')]");
	
	
	private static By budsproducttitle = By.xpath("//h1[@class='product-title']");
	
	private static By notifyme = By.xpath("//button[contains(text(),'Notify Me')]");
	
	private static By notifyclose = By.xpath("//span[@class='close']");
	
	
	
	
	private static By budsVS201header = By.xpath("//h5[contains(text(),'Buds VS201')]");
	
	private static By airbudsnanoheader = By.xpath("//h5[contains(text(),'Air Buds Nano')]");
	
	  public static By getbudsvs303()
	  {
		  return budsvs303;
	  }
	  
	  public static By getairbudsplus()
	  {
		  return airbudsplus;
	  }
	  public static By getairbuds()
	  {
		  return airbuds;
	  }
	  
	  public static By getbudsproducttitle()
	  {
		  return budsproducttitle;
	  }
	  
	  public static By getAirbudslink()
	  {
		  return Airbudslink;
	  }
	  
	  public static By getVS102link()
	  {
		  return VS102link;
	  }
	  
	  public static By getVS303link()
	  {
		  return VS303link;
	  }
	  
	  public static By getVS303linknew()
	  {
		  return VS303linknew;
	  }
	  
	  public static By getVS202link()
	  {
		  return VS202link;
	  }
	  
	  public static By getvs303header()
	  {
		  return vs303header;
	  }
	  
	  public static By getVS103link()
	  {
		  return VS103link;
	  }
	  
	  
	  public static By getVS201link()
	  {
		  return VS201link;
	  }
	  
	  public static By getairbudminilink()
	  {
		  return airbudminilink;
	  }
	  
	  public static By getairbudsproheader()
	  {
		  return airbudsproheader;
	  }
	  
	  public static By getnoisebeadsheader()
	  {
		  return noisebeadsheader;
	  }
	  
	  public static By getBudsAceheader()
	  {
		  return BudsAceheader;
	  }
	  
	  public static By getVS103header()
	  {
		  return VS103header;
	  }
	  
	  public static By getairbudspluslink()
	  {
		  return airbudspluslink;
	  }
	  
	  
	  public static By getbudsplay()
	  {
		  return budsplay;
	  }
	  
	  public static By getshotsneo2()
	  {
		  return shotsneo2;
	  }
	  
	  public static By getBudsVS201()
	  {
		  return BudsVS201;
	  }
	  
	  public static By getairbudsplusheader()
	  {
		  return airbudsplusheader;
	  }
	  
	  public static By getVS102header()
	  {
		  return VS102header;
	  }
	  
	  
	  public static By getshotsneo2header()
	  {
		  return shotsneo2header;
	  }
	  
	  public static By getelanencheader()
	  {
		  return elanencheader;
	  }
	  
	  public static By getshotsrushheader()
	  {
		  return shotsrushheader;
	  }
	  
	  public static By getairbudsminiheader()
	  {
		  return airbudsminiheader;
	  }
	  
	  public static By getairbudsheader()
	  {
		  return airbudsheader;
	  }
	  
	  public static By getbudsplayheader()
	  {
		  return budsplayheader;
	  }
	  
	  public static By getBudsSmartheader()
	  {
		  return BudsSmartheader;
	  }
	  
	  public static By getbudssoloheader()
	  {
		  return budssoloheader;
	  }
	  
	  public static By getbudsVS201header()
	  {
		  return budsVS201header;
	  }
	  
	  public static By getairbudsnanoheader()
	  {
		  return airbudsnanoheader;
	  }
	  
	  public static By getnotifyme()
	  {
		  return notifyme;
	  }
	  
	  public static By getnotifyclose()
	  {
		  return notifyclose;
	  }
}
