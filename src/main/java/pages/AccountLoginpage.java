package pages;

import org.openqa.selenium.By;


public class AccountLoginpage {
	
	  
	 
	
	  private static By MyAccountIcon=By.xpath("//img[@alt='account']/parent::a");  //input[@name='firstname']
	  
	  private static By SignUpLink=By.xpath("//a[@class='signup-url']");
	  
	  private static By FirstName=By.xpath("//input[@name='firstname']");
	  
	  private static By LastName=By.xpath("//input[@name='lastname']");
	  
	  private static By Email=By.xpath("//input[@name='email']");
	  
	  private static By Pass=By.xpath("//input[@name='password']");
	  
	  private static By ConfPass=By.xpath("//input[@name='copassword']");
	  
	  private static By PhoneNo=By.xpath("//input[@name='phoneNumber']");
	  
	  private static By Otp=By.xpath("//input[@name='otp']");
	  
	  
	  
	  private static By SignUpButton=By.xpath("//button[@type='submit']");
	  
	  private static By VerifyButton=By.xpath("//button[contains(text(),'Verify')]");
	  
	  
	  /*********************** Sign In ***********************/
	  
	  private static By EmailLogin=By.xpath("//input[@id='CustomerEmail']");
	  
	  private static By PwdLogin=By.xpath("//input[@id='CustomerPassword']"); 
	  
	  private static By btnSignin=By.xpath("//input[@value='Sign In']");
	  
	  
	  private static By SignOut=By.xpath("//a[contains(text(),'Sign out')]");
	  
	
	  
	  
	  public static By getEmailLogin()
	  {
		  return EmailLogin;
	  }
	  
	  public static By getPwdLogin()
	  {
		  return PwdLogin;
	  }
	  
	  public static By getbtnSignin()
	  {
		  return btnSignin;
	  }
	  
	
	  
	  public static By getSignOut()
	  {
		  return SignOut;
	  }
	  
	  /*********************** Sign In ***********************/
	  
	  public static By getmyaccountlink()
	  {
		  return MyAccountIcon;
	  }
	  
	  public static By getSignUpLink()
	  {
		  return SignUpLink;
	  }

	  
	  public static By getFirstName()
	  {
		  return FirstName;
	  }
	  
	  public static By getLastName()
	  {
		  return LastName;
	  }
	  
	  public static By getEmail()
	  {
		  return Email;
	  }
	  
	  public static By getPass()
	  {
		  return Pass;
	  }
	  
	  public static By getConfPass()
	  {
		  return ConfPass;
	  }
	  
	  public static By getPhoneNo()
	  {
		  return PhoneNo;
	  }
	  
	  public static By getSignUpButton()
	  {
		  return SignUpButton;
	  }
	  
	  public static By getOtp()
	  {
		  return Otp;
	  }
	  
	  public static By getVerifyButton()
	  {
		  return VerifyButton;
	  }
}
