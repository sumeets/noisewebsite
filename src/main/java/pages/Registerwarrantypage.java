package pages;

import org.openqa.selenium.By;


public class Registerwarrantypage {
	
	
	private static By FullName = By.xpath("//input[@name='name']");
	
	private static By Email = By.xpath("//input[@name='email']");
	
	private static By mobile = By.xpath("//label[contains(text(),'Mobile')]/following-sibling::input");
	
	private static By distributer = By.xpath("//select[@name='source']");
	
	private static By searchproduct = By.xpath("//input[@placeholder='Search for a product']");
	
	
	private static By ordernumber = By.xpath("//input[@name='order_number']");
	
	private static By serialnumber = By.xpath("//input[@name='serial_number']");
	
	private static By registerbutton = By.xpath("//button[contains(text(),'Register Now')]");
	
	
private static By OTPTxtbox = By.xpath("//input[@placeholder='Enter OTP']");
	
	private static By verifyOTPButton = By.xpath("//button[contains(text(),'Verify OTP')]");
	
	
	
	 public static By getFullName()
	  {
		  return FullName;
	  }
	 
	 public static By getEmail()
	  {
		  return Email;
	  }
	 
	 public static By getmobile()
	  {
		  return mobile;
	  }
	 
	 public static By getdistributer()
	  {
		  return distributer;
	  }
	 
	 public static By getsearchproduct()
	  {
		  return searchproduct;
	  }
	 
	 public static By getordernumber()
	  {
		  return ordernumber;
	  }
	 
	 public static By getserialnumber()
	  {
		  return serialnumber;
	  }
	 
	 public static By getregisterbutton()
	  {
		  return registerbutton;
	  }
	 
	 public static By getOTPTxtbox()
	  {
		  return OTPTxtbox;
	  }
	 
	 
	 public static By getverifyOTPButton()
	  {
		  return verifyOTPButton;
	  }
}
