package pages;

import org.openqa.selenium.By;


public class NoiseHomePage {
	
	  
	  private static By ProductLink = By.xpath("//a[@class='first-level-a'][normalize-space()='Products']");

	  private static By NoiseLogo = By.xpath("//img[@alt='Noise']/parent::a");
	  
	  private static By SupportLink = By.xpath("//a[@class='first-level-a'][normalize-space()='Support']");
	  
	  private static By StoriesLink = By.xpath("//a[@class='first-level-a'][normalize-space()='Stories']");
	  
	  private static By SearchIcon = By.xpath("//img[@class='showInput icon-img']");
	  
	  private static By SearchText=By.xpath("//span[@title='Noise ColorFit Pro 3 Assist Smart Watch']");
	  
	  private static By CartIcon=By.xpath("//img[@alt='cart']/parent::a");
	  
	  private static By CartText=By.xpath("//span[contains(text(),'Shopping Cart')]");
	  
	  private static By CartEmptyText=By.xpath("//div[@class='empty-cart-msg']");
	  
	  private static By WirelessBudsText=By.xpath("//h2[contains(text(),'Wireless Earbudss')]");
	  
	  private static By BluetoothNeckbandsText=By.xpath("//h2[normalize-space()='Bluetooth Neckbands']");
	  
	  private static By KnowMoreWirelessBuds=By.xpath("//a[@href='/collections/wireless-earbuds'][normalize-space()='Know More']");
	  
	  private static By KnowMoreSmartWatches=By.xpath("//a[@href='/collections/smart-watches'][normalize-space()='Know More']");
	
	  private static By KnowMoreBluetoothNeckbands=By.xpath("//a[@href='/collections/bluetooth-earphones'][normalize-space()='Know More']");
	  
	  private static By KnowMoreAccessories=By.xpath("//a[@href='/collections/accessories'][normalize-space()='Know More']");
	
	  private static By MyAccountIcon=By.xpath("//img[@alt='accountt']/parent::a");
	  
	  private static By HelloNoiseMaker=By.xpath("//h3[normalize-space()='Hello NoiseMaker!']");
	  
	  private static By readalltext=By.xpath("//label[normalize-space()='April 19, 2021']");
	  
	
	  
	
	
	  private static By ReadAlldiv=By.xpath("//div[@class='bottom-container']");
	  
	  private static By ReadAllLink=By.xpath("//label[contains(text(),'Read All >')]/parent::a");
	  
	  private static By WatchNow=By.xpath("//button[normalize-space()='Watch Now']");
	  
	  private static By AudioForAll=By.xpath("//h1[normalize-space()='#AudioForAll']");
	  
	  private static By frame=By.xpath("//iframe[@src='https://cdn.shopify.com/s/files/1/0997/6284/files/Homepage_Video_Noise_Wireless_Audio.mp4?v=1617882128']");
	
	 // private static By VideoPlayIconold=By.xpath("//video[@name='media']/source");
	  
	  private static By VideoPlayIcon=By.xpath("//button[@aria-label='Play']");
	  
	
	  
	  private static By signuptxtbox=By.xpath("//input[@id='mail_form']");
	  
	  private static By signupbutton=By.xpath("//button[normalize-space()='Sign Up']");
	  
	  private static By paratag=By.xpath("//div[@class='subscribe_form_wrap']/form/p");
	  
	  private static By signuperrortext=By.xpath("//p[@class='error']");
	  
	  private static By signupsuccesstext=By.xpath("//p[@class='success']");
	  
	  private static By signupstatictext=By.xpath("//p[contains(text(),'Sign up now to hear about our latest offers, new p')]");
	  
	  /********************* Page objects of Footer elements ********************/
	  
	  private static By smartwatcheslink=By.xpath("//a[@data-react='true'][normalize-space()='Smart Watches']");
	  
	  private static By earphoneslink=By.xpath("//a[@data-react='true'][normalize-space()='Bluetooth Earphones']");
	  
	  private static By wirelessearbudslink=By.xpath("//a[@data-react='true'][normalize-space()='Wireless Earbuds']");
	  
	  private static By noisefitapplink=By.xpath("//a[normalize-space()='NoiseFit App']");
	  
	  private static By accessorieslink=By.xpath("//a[@data-react='true'][normalize-space()='Accessories']");
	  
	  private static By corporateenquirieslink=By.xpath("//a[normalize-space()='Corporate Enquiries']");
	  
	  private static By affiliatelink=By.xpath("//a[normalize-space()='Become an Affiliate']");
	  
	  private static By faqlink=By.xpath("//a[normalize-space()='FAQ']");
	  
	  private static By trackorderlink=By.xpath("//a[normalize-space()='Track Your Order']");
	  
	  private static By downloadinvoice=By.xpath("//a[@data-react='true'][normalize-space()='Download Your Invoice']");
	  
	  private static By registerwarranty=By.xpath("//a[contains(text(),'Register Your Warranty')]");
	  
	  private static By registercomplaint=By.xpath("//a[contains(text(),'Register a Complaint')]");
	  
	  private static By registeronlinecomplaint=By.xpath("//a[contains(text(),'Register an online complaint')]");
	  
	  private static By productsupport=By.xpath("//a[normalize-space()='Product Support']");
	  
	  private static By warrantyguidelines=By.xpath("//a[normalize-space()='Warranty Guidelines']");
	  
	  private static By shippingreturns=By.xpath("//a[normalize-space()='Shipping and Returns']");
	  
	  private static By contactus=By.xpath("//a[normalize-space()='Contact Us']");
	  
	  private static By aboutus=By.xpath("//a[normalize-space()='About Us']");
	  
	  private static By stories=By.xpath("//a[@data-react='true'][normalize-space()='Stories']");
	  
	  private static By storieslink=By.xpath("(//a[contains(text(),'Stories')])[2]");
	  
	  private static By careers=By.xpath("//a[normalize-space()='Careers']");
	  
	  private static By invoicebutton=By.xpath("//button[contains(text(),'Download Invoice')]");
	  
	  private static By press=By.xpath("//a[normalize-space()='In the Press']");
	  
	  private static By terms=By.xpath("//a[normalize-space()='Terms & Conditions']");
	  
	  private static By privacy=By.xpath("//a[normalize-space()='Privacy Policy']");
	  
	  private static By facebooklink=By.xpath("//span[normalize-space()='Join over 650K fans']");
	  
	  private static By instalink=By.xpath("//span[normalize-space()='Join over 475K fans']");
	  
	  private static By youtubelink=By.xpath("//span[normalize-space()='Watch our product videos']");
	  
	  /********************* Page objects of Footer elements ********************/
	  
	  public static By getreadalltext()
	  {
		  return readalltext;
	  }
	  
	  public static By getparatag()
	  {
		  return paratag;
	  }
	  
	  public static By getHelloNoiseMaker()
	  {
		  return HelloNoiseMaker;
	  }
	  
	  public static By getMyAccountIcon()
	  {
		  return MyAccountIcon;
	  }
	  
	  public static By getregisteronlinecomplaint()
	  {
		  return registeronlinecomplaint;
	  }
	  
	  public static By getaboutus()
	  {
		  return aboutus;
	  }
	  
	  public static By getstories()
	  {
		  return stories;
	  }
	  
	  public static By getcareers()
	  {
		  return careers;
	  }
	  
	  public static By getstorieslink()
	  {
		  return storieslink;
	  }
	  
	  public static By getinvoicebutton()
	  {
		  return invoicebutton;
	  }
	  public static By getpress()
	  {
		  return press;
	  }
	  
	  public static By getterms()
	  {
		  return terms;
	  }
	  
	  public static By getprivacy()
	  {
		  return privacy;
	  }
	  
	  public static By getfaqlink()
	  {
		  return faqlink;
	  }
	  
	  public static By gettrackorderlink()
	  {
		  return trackorderlink;
	  }
	  
	  public static By getdownloadinvoice()
	  {
		  return downloadinvoice;
	  }
	  
	  public static By getregisterwarranty()
	  {
		  return registerwarranty;
	  }
	  
	  public static By getregistercomplaint()
	  {
		  return registercomplaint;
	  }
	  
	  public static By getwarrantyguidelines()
	  {
		  return warrantyguidelines;
	  }
	  
	  public static By getproductsupport()
	  {
		  return productsupport;
	  }
	  
	  public static By getshippingreturns()
	  {
		  return shippingreturns;
	  }
	  
	  public static By getcontactus()
	  {
		  return contactus;
	  }
	
	  public static By getsmartwatcheslink()
	  {
		  return smartwatcheslink;
	  }
	  
	  public static By getearphoneslink()
	  {
		  return earphoneslink;
	  }
	  
	  public static By getwirelessearbudslink()
	  {
		  return wirelessearbudslink;
	  }
	  
	  public static By getnoisefitapplink()
	  {
		  return noisefitapplink;
	  }
	  
	  public static By getaccessorieslink()
	  {
		  return accessorieslink;
	  }
	  
	  public static By getcorporateenquirieslink()
	  {
		  return corporateenquirieslink;
	  }
	  
	  public static By getaffiliatelink()
	  {
		  return affiliatelink;
	  }
	  
	  public static By getProductLink()
	  {
		  return ProductLink;
	  }
	  
	  public static By getNoiseLogo()
	  {
		  return NoiseLogo;
	  }
	  
	  public static By getSupportLink()
	  {
		  return SupportLink;
	  }
	  
	  public static By getStoriesLink()
	  {
		  return StoriesLink;
	  }
	  
	  public static By getSearchIcon()
	  {
		  return SearchIcon;
	  }
	  
	  public static By getSearchText()
	  {
		  return SearchText;
	  }
	  
	  public static By getCartIcon()
	  {
		  return CartIcon;
	  }
	  
	  public static By getCartText()
	  {
		  return CartText;
	  }
	  
	  public static By getCartEmptyText()
	  {
		  return CartEmptyText;
	  }
	  
	  public static By getWirelessBudsText()
	  {
		  return WirelessBudsText;
	  }
	  
	  public static By getKnowMoreWirelessBuds()
	  {
		  return KnowMoreWirelessBuds;
	  }

	  public static By getKnowMoreSmartWatches()
	  {
		  return KnowMoreSmartWatches;
	  }
	  
	  public static By getBluetoothNeckbandsText()
	  {
		  return BluetoothNeckbandsText;
	  }
	  
	  public static By getKnowMoreBluetoothNeckbands()
	  {
		  return KnowMoreBluetoothNeckbands;
	  }
	  
	  public static By getKnowMoreAccessories()
	  {
		  return KnowMoreAccessories;
	  }
	  
	  public static By getReadAll()
	  {
		  return ReadAlldiv;
	  }
	  
	  public static By getReadAllLink()
	  {
		  return ReadAllLink;
	  }
	  
	  public static By getWatchNow()
	  {
		  return WatchNow;
	  }
	  
	  public static By getframe()
	  {
		  return frame;
	  }
	  
	  public static By getVideoPlayIcon()
	  {
		  return VideoPlayIcon;
	  }
	  
	  public static By getAudioForAll()
	  {
		  return AudioForAll;
	  }
	  
	  public static By getsignuptxtbox()
	  {
		  return signuptxtbox;
	  }
	  
	  public static By getsignupbutton()
	  {
		  return signupbutton;
	  }
	  
	  public static By getsignuperrortext()
	  {
		  return signuperrortext;
	  }
	  
	  public static By getsignupsuccesstext()
	  {
		  return signupsuccesstext;
	  }
	  
	  public static By getsignupstatictext()
	  {
		  return signupstatictext;
	  }
	  
	  public static By getfacebooklink()
	  {
		  return facebooklink;
	  }
	  
	  public static By getinstalink()
	  {
		  return instalink;
	  }
	  
	  public static By getyoutubelink()
	  {
		  return youtubelink;
	  }

}
