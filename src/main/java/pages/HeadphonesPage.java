package pages;

import org.openqa.selenium.By;


public class HeadphonesPage {
	
	
	private static By oneheadphoneslink = By.xpath("//span[text()='One Headphones']/parent::a");
	
	private static By noisepowrlink = By.xpath("//span[text()='Noise Powr']/parent::a");
	
	private static By oneheadphonesheader = By.xpath("//h5[contains(text(),'One Headphones')]");
	
	private static By Noisepowrheader = By.xpath("//h5[contains(text(),'Noise Powr')]");
	
	private static By DefyANCHeadphonesheader = By.xpath("//h5[contains(text(),'Defy ANC Headphones')]");
	
	 public static By getoneheadphoneslink()
	  {
		  return oneheadphoneslink;
	  }
	 
	 public static By getnoisepowrlink()
	  {
		  return noisepowrlink;
	  }
	 
	 public static By getoneheadphonesheader()
	  {
		  return oneheadphonesheader;
	  }
	 public static By getNoisepowrheader()
	  {
		  return Noisepowrheader;
	  }
	 public static By getDefyANCHeadphonesheader()
	  {
		  return DefyANCHeadphonesheader;
	  }
	 
}
