package pages;

import org.openqa.selenium.By;


public class SmartWatchesPage {
	
	  
	 
	  
	  
	  
	  private static By colorfitultra = By.xpath("//span[normalize-space()='ColorFit Ultra']");
	  
	  private static By brio = By.xpath("//span[normalize-space()='ColorFit Brio']");
	  private static By qube = By.xpath("//span[normalize-space()='ColorFit Qube']");
	 
	  private static By brioheader = By.xpath("//h5[contains(text(),'ColorFit Brio')]");
	  private static By xfit1header = By.xpath("//h5[contains(text(),'X-Fit 1 (HRX Edition)')]");
	  
	  private static By champkidsheader = By.xpath("//h5[contains(text(),'Noise Champ Kids smartband')]");
	  
	  private static By ultra2header = By.xpath("//h5[contains(text(),'ColorFit Ultra 2')]");
	  
	 // private static By qubeheader = By.xpath("//h5[contains(text(),'ColorFit Qube')]");
	  
	  private static By qubeheader = By.xpath("//h5[contains(text(),'ColorFit Qube O2')]");
	  
	  private static By pro3alphaheader = By.xpath("//h1[contains(text(),'ColorFit Pro 3 Alpha')]");
	  
	  private static By pulsegrandheader = By.xpath("//h1[contains(text(),'ColorFit Pulse Grand ')]");
	  
	  private static By ultrabuzzheader = By.xpath("//h1[contains(text(),'Ultra Buzz')]");
	  
	  private static By ultraheader = By.xpath("//h5[contains(text(),'ColorFit Ultra')]");
	  
	  private static By pulheader = By.xpath("//h5[contains(text(),'ColorFit Pulse')]");
	  
	  private static By pro3header = By.xpath("//h5[contains(text(),'ColorFit Pro 3')]");
	  
	  private static By visionheader = By.xpath("//h5[contains(text(),'ColorFit Vision')]");
	  
	  private static By activeheader = By.xpath("//h5[contains(text(),'NoiseFit Active')]");
	  
	  private static By pro3assistheader = By.xpath("//h5[contains(text(),'ColorFit Pro 3 Assist')]");
	  
	  private static By pro2header = By.xpath("//h5[contains(text(),'ColorFit Pro 2')]");
	  
	  private static By endureheader = By.xpath("//h5[contains(text(),'NoiseFit Endure')]");
	  
	  private static By navheader = By.xpath("//h5[contains(text(),'ColorFit Nav')]");
	  
	  private static By buzzheader = By.xpath("//h5[contains(text(),'NoiseFit Buzz')]");
	  
	  private static By excelheader = By.xpath("//h5[contains(text(),'Noise Excel')]");
	  
	  private static By coreoxyheader = By.xpath("//h5[contains(text(),'NoiseFit Core Oxy')]");
	  
	  private static By pro2oxyheader = By.xpath("//h5[contains(text(),'ColorFit Pro 2 Oxy')]");
	  
	  private static By coreheader = By.xpath("//h5[contains(text(),'NoiseFit Core')]");
	  private static By agileheader = By.xpath("//h5[contains(text(),'NoiseFit Agile')]");
	  
	  private static By navplusheader = By.xpath("//h5[contains(text(),'ColorFit Nav+')]");
	  
	  private static By beatheader = By.xpath("//h5[contains(text(),'ColorFit Beat')]");
	  
	  private static By caliberheader = By.xpath("//h5[contains(text(),'ColorFit Caliber')]");
	  
	  private static By evolve2header = By.xpath("//h5[contains(text(),'NoiseFit Evolve 2')]");
	  
	  private static By iconbuzzheader = By.xpath("//h5[contains(text(),'ColorFit Icon Buzz')]");
	  
	 // private static By qubelink = By.xpath("//p[normalize-space()='Cloud-based Watch Faces']/parent::div/parent::a");
	  private static By qubelink=By.xpath("//span[normalize-space()='ColorFit Qube O2']/parent::a");
	  
	  private static By loadmorelink = By.xpath("//button[normalize-space()='Load more']");
	  private static By colorvariant = By.xpath("//div[@id='product-bar']//ul/li");
	  private static By colorvariantnew = By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li");
	  private static By speciallaunchtext = By.xpath("//p[contains(text(),'Special Launch Offer')]");
	  private static By varianttitle = By.xpath("//p[@class='variant-title']");
	  
	  private static By varianttitlenew = By.xpath("//p[@class='varint-color-value']");
	  private static By notifyme = By.xpath("//button[contains(text(),'Notify me')]");
	  private static By notifymeold = By.xpath("//button[contains(text(),'Notify Me')]");
	  private static By notifyclose = By.xpath("//span[@class='close']");

	  private static By qty = By.xpath("//div[@class='product-quantity-box']");
	  
	  private static By productnames = By.xpath("//span[@class='product-title']");
	  
	  
	 // private static By lowestpricetext = By.xpath("//p[normalize-space()='Lowest Price | Great Gadget Sale']");
	  
	  private static By lowestpricetext = By.xpath("(//div[@class='discount-percentage offer2-active'])[2]/p");
	  
	  //private static By pulselink = By.xpath("//p[normalize-space()='heart rate monitor']/parent::div/parent::a");
	  
	  private static By pulselink =By.xpath("//span[normalize-space()='ColorFit Pulse']/parent::a");
	  
	  private static By pro3 = By.xpath("//span[contains(normalize-space(),'ColorFit Pro 3')]");
	  
	  private static By pro2 = By.xpath("//span[contains(normalize-space(),'ColorFit Pro 2')]");
	  
	  private static By nav = By.xpath("//span[contains(normalize-space(),'ColorFit Nav')]");
	  
	 // private static By ultralink = By.xpath("//p[normalize-space()='Blood Oxygen Monitor']/parent::div/parent::a");
	  
	  private static By ultralink = By.xpath("//span[normalize-space()='ColorFit Ultra']/parent::a");
	  
	  private static By pro3link = By.xpath("//span[normalize-space()='ColorFit Pro 3']/parent::a");
	  
	  private static By activelink = By.xpath("//span[normalize-space()='NoiseFit Active']/parent::a");
	  
	  private static By pro3assistlink = By.xpath("//span[normalize-space()='ColorFit Pro 3 Assist']/parent::a");
	  
	  //private static By pro3assistlink = By.xpath("//p[normalize-space()='Built-in Alexa']/parent::div/parent::a");
	  
	  //private static By pro2link = By.xpath("//p[normalize-space()='Most Stylish Smartwatch']/parent::div/parent::a");
	  
	  private static By pro2link = By.xpath("//span[normalize-space()='ColorFit Pro 2']/parent::a");
	  
	  
	  private static By endurelink = By.xpath("//span[normalize-space()='NoiseFit Endure']/parent::a");
	  
	  private static By navlink = By.xpath("//span[normalize-space()='ColorFit Nav']/parent::a");
	  
	//  private static By pro2oxylink = By.xpath("(//p[normalize-space()='For Healthy Living']/parent::div/parent::a)[2]");
	  
	  private static By pro2oxylink=By.xpath("//span[normalize-space()='ColorFit Pro 2 Oxy']/parent::a");
	  
	  private static By corelink = By.xpath("//span[contains(normalize-space(),'NoiseFit Core')]/parent::a");
	  
	  private static By agilelink = By.xpath("//span[contains(normalize-space(),'NoiseFit Agile')]/parent::a");
	  
	  private static By navpluslink = By.xpath("//span[contains(normalize-space(),'ColorFit Nav+')]/parent::a");
	  
	  private static By beatlink = By.xpath("//span[contains(normalize-space(),'ColorFit Beat')]/parent::a");
	  
	  private static By caliberlink = By.xpath("//span[contains(normalize-space(),'ColorFit Caliber')]/parent::a");
	  
	  private static By iconbuzzlink = By.xpath("//span[contains(normalize-space(),'ColorFit Icon Buzz')]/parent::a");
	  
	 
		
	  
	  public static By getproductnames()
	  {
		  return productnames;
	  }
	  
	  public static By getpulselink()
	  {
		  return pulselink;
	  }
	  
	  public static By getendurelink()
	  {
		  return endurelink;
	  }
	  
	  public static By getcaliberlink()
	  {
		  return caliberlink;
	  }
	  
	  public static By geticonbuzzlink()
	  {
		  return iconbuzzlink;
	  }
	  
	  public static By getultra2header()
	  {
		  return ultra2header;
	  }
	  
	  public static By getexcelheader()
	  {
		  return excelheader;
	  }
	  
	  public static By geticonbuzzheader()
	  {
		  return iconbuzzheader;
	  }
	  
	  public static By getultrabuzzheader()
	  {
		  return ultrabuzzheader;
	  }
	  
	  public static By getpro3alphaheader()
	  {
		  return pro3alphaheader;
	  }
	  
	  public static By getcoreoxyheader()
	  {
		  return coreoxyheader;
	  }
	  
	  public static By getpulheader()
	  {
		  return pulheader;
	  }
	  
	  public static By getbuzzheader()
	  {
		  return buzzheader;
	  }
	  
	  public static By getvisionheader()
	  {
		  return visionheader;
	  }
	  
	  public static By getevolve2header()
	  {
		  return evolve2header;
	  }
	  
	  public static By getnavlink()
	  {
		  return navlink;
	  }
	  
	  public static By getbeatlink()
	  {
		  return beatlink;
	  }
	  
	  public static By getnavpluslink()
	  {
		  return navpluslink;
	  }
	  
	  public static By getcorelink()
	  {
		  return corelink;
	  }
	  
	  public static By getpro2oxylink()
	  {
		  return pro2oxylink;
	  }
	  
	  public static By getagilelink()
	  {
		  return agilelink;
	  }
	  
	  public static By getqty()
	  {
		  return qty;
	  }
	  
	  public static By getnav()
	  {
		  return nav;
	  }
	  public static By getactiveheader()
	  {
		  return activeheader;
	  }
	  
	  public static By getchampkidsheader()
	  {
		  return champkidsheader;
	  }
	  
	  public static By getcaliberheader()
	  {
		  return caliberheader;
	  }
	  
	  
	  public static By getxfit1header()
	  {
		  return xfit1header;
	  }
	  
	  public static By getbeatheader()
	  {
		  return beatheader;
	  }
	  
	  
	  public static By getagileheader()
	  {
		  return agileheader;
	  }
	  
	  public static By getnavplusheader()
	  {
		  return navplusheader;
	  }
	  
	  public static By getcoreheader()
	  {
		  return coreheader;
	  }
	  
	  public static By getnavheader()
	  {
		  return navheader;
	  }
	  
	  public static By getpro3assistheader()
	  {
		  return pro3assistheader;
	  }
	  public static By getpro2oxyheader()
	  {
		  return pro2oxyheader;
	  }
	  
	  public static By getpro2header()
	  {
		  return pro2header;
	  }
	  
	  public static By getendureheader()
	  {
		  return endureheader;
	  }
	  public static By getultralink()
	  {
		  return ultralink;
	  }
	  
	  public static By getactivelink()
	  {
		  return activelink;
	  }
	  
	  public static By getpro3link()
	  {
		  return pro3link;
	  }
	  
	  public static By getpro2link()
	  {
		  return pro2link;
	  }
	  
	  public static By getpro3assistlink()
	  {
		  return pro3assistlink;
	  }
	  public static By getpulsegrandheader()
	  {
		  return pulsegrandheader;
	  }
	  
	  public static By getpro3header()
	  {
		  return pro3header;
	  }
	  
	  public static By getultraheader()
	  {
		  return ultraheader;
	  }
	  
	  public static By getpro3()
	  {
		  return pro3;
	  }
	  
	  
	  public static By getpro2()
	  {
		  return pro2;
	  }
	  public static By getcolorfitultra()
	  {
		  return colorfitultra;
	  }
	  public static By getbrioheader()
	  {
		  return brioheader;
	  }
	  
	  public static By getbrio()
	  {
		  return brio;
	  }
	  
	  public static By getqube()
	  {
		  return qube;
	  }
	  
	  public static By getqubelink()
	  {
		  return qubelink;
	  }
	  
	  public static By getqubeheader()
	  {
		  return qubeheader;
	  }
	  
	  public static By getloadmorelink()
	  {
		  return loadmorelink;
	  }
	  
	  public static By getcolorvariant()
	  {
		  return colorvariant;
	  }
	  
	  
	  public static By getcolorvariantnew()
	  {
		  return colorvariantnew;
	  }
	  
	  
	  public static By getspeciallaunchtext()
	  {
		  return speciallaunchtext;
	  }
	  
	  public static By getvarianttitle()
	  {
		  return varianttitle;
	  }
	  
	  public static By getvarianttitlenew()
	  {
		  return varianttitlenew;
	  }
	  
	  public static By getnotifyme()
	  {
		  return notifyme;
	  }
	  
	  public static By getnotifymeold()
	  {
		  return notifymeold;
	  }
	  
	  public static By getnotifyclose()
	  {
		  return notifyclose;
	  }
	
	  public static By getlowestpricetext()
	  {
		  return lowestpricetext;
	  }
}
