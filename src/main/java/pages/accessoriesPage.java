package pages;

import org.openqa.selenium.By;


public class accessoriesPage {
	
	
	private static By classicNylon22mm= By.xpath("//span[text()='Classic Nylon 22mm (Strap)']/parent::a");
	
	private static By classicLeather22mm= By.xpath("//span[text()='Classic Leather 22mm (Strap)']/parent::a");
	
	private static By uniweave22mmxsstrap= By.xpath("//span[text()='Uni Weave 22mm - XS (Strap)']/parent::a");
	
	private static By uniweave22mmsmall= By.xpath("//span[text()='Uni Weave 22mm - Small (Strap)']/parent::a");
	
	private static By uniweave22mmlarge= By.xpath("//span[text()='Uni Weave 22mm - Large (Strap)']/parent::a");
	
	private static By magneticleather22mm= By.xpath("//span[text()='Magnetic Leather 22mm (Strap)']/parent::a");
	
	private static By classicsilicon20mm= By.xpath("//span[text()='Classic Silicone 20mm (Strap)']/parent::a");
	
	private static By classicsilicon19mm= By.xpath("//span[text()='Classic Silicone 19mm (Strap)']/parent::a");
	
	private static By SportsEdition22mm= By.xpath("//span[text()='Sports Edition 22mm (Strap)']/parent::a");
	
	private static By PremiumSilicon22mm= By.xpath("//span[text()='Premium Silicone 22mm (Strap)']/parent::a");
	
	private static By NoiseMagneticChargingCable= By.xpath("//span[text()='Noise Magnetic Charging Cable']/parent::a");
	
	private static By StripedSiliconXFit1= By.xpath("//span[text()='Striped Silicone X-Fit 1 Strap (20 MM)']/parent::a");
	
	private static By MetallicLink22mmStrap= By.xpath("//span[text()='Metallic Link 22mm (Strap)']/parent::a");
	
	private static By MagneticLeather20mmStrap= By.xpath("//span[text()='Magnetic Leather 20mm (Strap)']/parent::a");
	
	private static By MetallicLink20mmStrap= By.xpath("//span[text()='Metallic Link 20mm (Strap)']/parent::a");
	
	private static By UniWeave20mmLStrap= By.xpath("//span[text()='Uni Weave 20mm - L (Strap)']/parent::a");
	
	private static By UniWeave20mmSStrap= By.xpath("//span[text()='Uni Weave 20mm - S (Strap)']/parent::a");
	
	private static By UniWeave20mmXSStrap= By.xpath("//span[text()='Uni Weave 20mm - XS (Strap)']/parent::a");
	
	private static By PriceEdition22mmStrap= By.xpath("//span[text()='Pride Edition 22mm (Strap)']/parent::a");
	
	private static By ChargingdockType4= By.xpath("//span[text()='Noise Smartwatch Charging Dock Type 4 (For ColorFit Ultra 2)']/parent::a");
	
	private static By classicNylon22mmheader = By.xpath("//h5[contains(text(),'Classic Nylon 22mm (Strap)')]");
	
	private static By classicNylon20mmheader = By.xpath("//h5[contains(text(),'Classic Nylon 20mm (Strap)')]");
	
	private static By classicLeather22mmheader = By.xpath("//h5[contains(text(),'Classic Leather 22mm (Strap)')]");
	
	private static By uniweave22mmxsheader = By.xpath("//h5[contains(text(),'Uni Weave 22mm - XS (Strap)')]");
	
	private static By uniweave22mmsmallheader = By.xpath("//h5[contains(text(),'Uni Weave 22mm - Small (Strap)')]");
	
	private static By uniweave22mmlargeheader = By.xpath("//h5[contains(text(),'Uni Weave 22mm - Large (Strap)')]");
	
	private static By magneticleather22mmheader = By.xpath("//h5[contains(text(),'Magnetic Leather 22mm (Strap)')]");
	
	private static By classicsilicon20mmheader = By.xpath("//h5[contains(text(),'Classic Silicone 20mm (Strap)')]");
	
	private static By classicsilicon19mmheader = By.xpath("//h5[contains(text(),'Classic Silicone 19mm (Strap)')]");
	
	private static By SportsEdition22mmheader = By.xpath("//h5[contains(text(),'Sports Edition 22mm (Strap)')]");
	
	private static By SportsEdition20mmheader = By.xpath("//h5[contains(text(),'Sports Edition 20mm (Strap)')]");
	
	private static By PremiumSilicon22mmheader = By.xpath("//h5[contains(text(),'Premium Silicone 22mm (Strap)')]");
	
	private static By NoiseMagneticChargingCableHeader = By.xpath("//h5[contains(text(),'Noise Magnetic Charging Cable')]");
	
	private static By StripedSiliconXFit1Header = By.xpath("//h5[contains(text(),'Striped Silicone X-Fit 1 Strap (20 MM)')]");
	
	private static By MetallicLink22mmHeader = By.xpath("//h5[contains(text(),'Metallic Link 22mm (Strap)')]");
	
	private static By MagneticLeather20mmHeader = By.xpath("//h5[contains(text(),'Magnetic Leather 20mm (Strap)')]");
	
	private static By MetallicLink20mmHeader = By.xpath("//h5[contains(text(),'Metallic Link 20mm (Strap)')]");
	
	private static By UniWeave20mmLHeader = By.xpath("//h5[contains(text(),'Uni Weave 20mm - L (Strap)')]");
	
	private static By UniWeave20mmSHeader = By.xpath("//h5[contains(text(),'Uni Weave 20mm - S (Strap)')]");
	
	private static By UniWeave20mmXSHeader = By.xpath("//h5[contains(text(),'Uni Weave 20mm - XS (Strap)')]");
	
	private static By PriceEdition22mmHeader = By.xpath("//h5[contains(text(),'Pride Edition 22mm (Strap)')]");
	
	private static By ChargingDockHeader = By.xpath("//h5[contains(text(),'Noise Smartwatch Charging Dock Type 4 (For ColorFit Ultra 2)')]");
	
	
	 public static By getNoiseMagneticChargingCable()
	  {
		  return NoiseMagneticChargingCable;
	  }
	 
	 public static By getMetallicLink22mmStrap()
	  {
		  return MetallicLink22mmStrap;
	  }
	 
	 public static By getMetallicLink20mmStrap()
	  {
		  return MetallicLink20mmStrap;
	  }
	 
	 public static By getUniWeave20mmLStrap()
	  {
		  return UniWeave20mmLStrap;
	  }
	 
	 public static By getclassicNylon20mmheader()
	  {
		  return classicNylon20mmheader;
	  }
	 
	 public static By getUniWeave20mmSStrap()
	  {
		  return UniWeave20mmSStrap;
	  }
	 
	 public static By getUniWeave20mmXSStrap()
	  {
		  return UniWeave20mmXSStrap;
	  }
	 
	 public static By getPriceEdition22mmStrap()
	  {
		  return PriceEdition22mmStrap;
	  }
	 
	 public static By getSportsEdition20mmheader()
	  {
		  return SportsEdition20mmheader;
	  }
	 
	 public static By getChargingdockType4()
	  {
		  return ChargingdockType4;
	  }
	 
	 public static By getMagneticLeather20mmStrap()
	  {
		  return MagneticLeather20mmStrap;
	  }
	
	 public static By getSportsEdition22mmLink()
	  {
		  return SportsEdition22mm;
	  }
	 
	 public static By getStripedSiliconXFit1Link()
	  {
		  return StripedSiliconXFit1;
	  }
	
	 public static By getclassicNylon22mmLink()
	  {
		  return classicNylon22mm;
	  }
	 
	 public static By getclassicLeather22mmLink()
	  {
		  return classicLeather22mm;
	  }
	 
	 public static By getuniweave22mmxsstrapLink()
	  {
		  return uniweave22mmxsstrap;
	  }
	 
	 public static By getuniweave22mmsmallLink()
	  {
		  return uniweave22mmsmall;
	  }
	 
	 public static By getuniweave22mmlargeLink()
	  {
		  return uniweave22mmlarge;
	  }
	 
	 public static By getmagneticleather22mmLink()
	  {
		  return magneticleather22mm;
	  }
	 
	 public static By getclassicsilicon20mmLink()
	  {
		  return classicsilicon20mm;
	  }
	 public static By getclassicsilicon19mmLink()
	  {
		  return classicsilicon19mm;
	  }
	 public static By getPremiumSilicon22mmLink()
	  {
		  return PremiumSilicon22mm;
	  }
	 
	 public static By getclassicNylon22mmheader()
	  {
		  return classicNylon22mmheader;
	  }
	 
	 public static By getStripedSiliconXFit1Header()
	  {
		  return StripedSiliconXFit1Header;
	  }
	 
	 public static By getclassicLeather22mmheader()
	  {
		  return classicLeather22mmheader;
	  }
	 
	 public static By getmagneticleather22mmheader()
	  {
		  return magneticleather22mmheader;
	  }
	 
	 public static By getuniweave22mmxsheader()
	  {
		  return uniweave22mmxsheader;
	  }
	 
	 public static By getuniweave22mmsmallheader()
	  {
		  return uniweave22mmsmallheader;
	  }
	 
	 public static By getuniweave22mmlargeheader()
	  {
		  return uniweave22mmlargeheader;
	  }
	 
	 public static By getclassicsilicon20mmheader()
	  {
		  return classicsilicon20mmheader;
	  }
	 
	 public static By getclassicsilicon19mmheader()
	  {
		  return classicsilicon19mmheader;
	  }
	 
	 public static By getSportsEdition22mmheader()
	  {
		  return SportsEdition22mmheader;
	  }
	 
	 public static By getPremiumSilicon22mmheader()
	  {
		  return PremiumSilicon22mmheader;
	  }
	 
	 public static By getNoiseMagneticChargingCableHeader()
	  {
		  return NoiseMagneticChargingCableHeader;
	  }
	 
	 public static By getMetallicLink22mmHeader()
	  {
		  return MetallicLink22mmHeader;
	  }
	 
	 public static By getMagneticLeather20mmHeader()
	  {
		  return MagneticLeather20mmHeader;
	  }
	 
	 public static By getMetallicLink20mmHeader()
	  {
		  return MetallicLink20mmHeader;
	  }
	 
	 public static By getUniWeave20mmLHeader()
	  {
		  return UniWeave20mmLHeader;
	  }
	 
	 public static By getUniWeave20mmSHeader()
	  {
		  return UniWeave20mmSHeader;
	  }
	 
	 public static By getUniWeave20mmXSHeader()
	  {
		  return UniWeave20mmXSHeader;
	  }
	 
	 public static By getPriceEdition22mmHeader()
	  {
		  return PriceEdition22mmHeader;
	  }
	 
	 public static By getChargingDockHeader()
	  {
		  return ChargingDockHeader;
	  }
	 

}
