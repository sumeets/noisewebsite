package pages;

import org.openqa.selenium.By;


public class CareerPage {
	
	
	private static By openpositionslink = By.xpath("//button[contains(text(),'See open positions')]");
	
	
	 public static By getopenpositionslink()
	  {
		  return openpositionslink;
	  }
	
	 
}
