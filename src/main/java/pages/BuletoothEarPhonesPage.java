package pages;

import org.openqa.selenium.By;

import utility.ExtentReport;


public class BuletoothEarPhonesPage {
	
	
	private static By flair = By.xpath("//span[normalize-space()='Flair']");
	
	private static By flairlink = By.xpath("//span[normalize-space()='Flair']/parent::a");
	
	private static By elitesportlink = By.xpath("//span[normalize-space()='Tune Elite Sport']/parent::a");
	
	private static By tuneactiveplusLink= By.xpath("//span[normalize-space()='Tune Active Plus']/parent::a");
	private static By tuneactiveplusheader = By.xpath("//h5[contains(text(),'Tune Active Plus')]");
	
	
	private static By tuneactiveLink= By.xpath("//span[normalize-space()='Tune Active']/parent::a");

	
	
	
	private static By tunesport = By.xpath("//span[normalize-space()='Tune Sport']");
	
	private static By sense = By.xpath("//span[normalize-space()='Sense']");
	private static By flairheader = By.xpath("//h5[contains(text(),'Flair')]");
	
	private static By flairXLheader = By.xpath("//h5[contains(text(),'Flair XL')]");
	
	private static By tunechargeheader = By.xpath("//h5[contains(text(),'Tune Charge')]");
	

	
	private static By tuneactiveheader = By.xpath("//h5[contains(text(),'Tune Active')]");
	
	private static By tunesport2header = By.xpath("//h5[contains(text(),'Tune Sport 2')]");
	
	private static By tuneelitesportheader = By.xpath("//h5[contains(text(),'Tune Elite Sport')]");
	
	private static By tunesportheader = By.xpath("//h5[contains(text(),'Tune Sport')]");
	
	private static By tuneliteheader = By.xpath("//h5[contains(text(),'Tune Lite')]");
	
	private static By Nerveheader = By.xpath("//h5[contains(text(),'Nerve')]");
	
	private static By Senseheader = By.xpath("//h5[contains(text(),'Sense')]");
	
	private static By Bravoheader = By.xpath("//h5[contains(text(),'Bravo')]");
	
	private static By combatheader = By.xpath("//h5[contains(text(),'Combat Gaming Neckband')]");
	private static By Combatlink = By.xpath("//span[normalize-space()='Combat Gaming Neckband']/parent::a");
	
	private static By TuneChargelink = By.xpath("//span[normalize-space()='Tune Charge']/parent::a");
	
	private static By Nervelink = By.xpath("//span[normalize-space()='Nerve']/parent::a");
	
	private static By Senselink = By.xpath("//span[normalize-space()='Sense']/parent::a");
	
	private static By TuneSport2 = By.xpath("//span[text()='Tune Sport 2']");
	
	//private static By TuneSport2link = By.xpath("//div[@class='sold-out-wrapper']/parent::a");
	
	private static By TuneSport2link = By.xpath("//span[contains(normalize-space(),'Tune Sport 2')]/parent::a");// complete product is sold out
	
	  public static By getflair()
	  {
		  return flair;
	  }
	  
	  public static By getTuneSport2()
	  {
		  return TuneSport2;
	  }
	  
	  public static By getflairlink()
	  {
		  return flairlink;
	  }
	  
	  public static By getelitesportlink()
	  {
		  return elitesportlink;
	  }
	  
	  public static By getNervelink()
	  {
		  return Nervelink;
	  }
	  
	  public static By getSenselink()
	  {
		  return Senselink;
	  }
	  
	  
	  public static By getTuneSport2link()
	  {
		  return TuneSport2link;
		  
	  }
		  
	  
	  public static By gettunesport()
	  {
		  return tunesport;
	  }
	  public static By gettuneactiveplusLink()
	  {
		  return tuneactiveplusLink;
	  }
	  public static By getCombatlink()
	  {
		  return Combatlink;
	  }
	  
	  public static By getcombatheader()
	  {
		  return combatheader;
	  }
	  public static By gettuneactiveLink()
	  {
		  return tuneactiveLink;
	  }
	
	  public static By getflairheader()
	  {
		  return flairheader;
	  }
	  public static By getflairXLheader()
	  {
		  return flairXLheader;
	  }
	  
	  public static By getBravoheader()
	  {
		  return Bravoheader;
	  }
	  
	  public static By gettunechargeheader()
	  {
		  return tunechargeheader;
	  }
	  public static By gettuneactiveplusheader()
	  {
		  return tuneactiveplusheader;
	  }
	  
	  public static By gettuneactiveheader()
	  {
		  return tuneactiveheader;
	  }
	  
	  public static By gettunesport2header()
	  {
		  return tunesport2header;
	  }
	  
	  public static By getTuneChargelink()
	  {
		  return TuneChargelink;
	  }
	  
	  public static By gettuneelitesportheader()
	  {
		  return tuneelitesportheader;
	  }
	  
	  public static By gettunesportheader()
	  {
		  return tunesportheader;
	  }
	  
	  public static By gettuneliteheader()
	  {
		  return tuneliteheader;
	  }
	  
	  public static By getNerveheader()
	  {
		  return Nerveheader;
	  }
	  
	  public static By getsense()
	  {
		  return sense;
	  }
	  
	  public static By getSenseheader()
	  {
		  return Senseheader;
	  }
}


