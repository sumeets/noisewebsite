package pages;

import org.openqa.selenium.By;


public class trackyourorderpage {
	
	
	
	
	private static By BtnTrackOrder = By.xpath("//button[contains(text(),'Track Order')]");
	
	private static By BtnSubmit = By.xpath("//button[contains(text(),'Submit')]");
	
	 public static By getBtnTrackOrder()
	  {
		  return BtnTrackOrder;
	  }
	 
	 public static By getBtnSubmit()
	  {
		  return BtnSubmit;
	  }
	
}
