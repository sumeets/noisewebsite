package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



//class to assign the cell value once it is getting done to
//read from excel sheet It can be String/Boolean/Numeric

public class GetContentFromExcelSheets {
	
	public static List<String> readData(String FilePath,int sheetindex) throws IOException
	{
		List<String> signupdata=new ArrayList<String>();
		
		FileInputStream file = new FileInputStream(new File(FilePath));
		//Create Workbook instance holding reference to .xlsx file
        XSSFWorkbook workbook = new XSSFWorkbook(file);

        //Get first/desired sheet from the workbook
        XSSFSheet sheet = workbook.getSheetAt(sheetindex);

        //Iterate through each rows one by one
        Iterator<Row> rowIterator = sheet.iterator();

		while (rowIterator.hasNext()) {
			Row nextRow=rowIterator.next();
			Iterator<Cell> cellIterator=nextRow.cellIterator();
			//SignUpLogin signup=new SignUpLogin();

			while (cellIterator.hasNext()) {
				Cell nextCell = cellIterator.next();
				//int columnIndex = nextCell.getColumnIndex();

				
				//	System.out.println(getCellValue(nextCell)+ "\t");
					signupdata.add(getCellValue(nextCell).toString());
					
			}
			//System.out.println("");
			
			System.out.println(signupdata.get(0));
		}
		
		return signupdata;
		
         
		
		
	}
	
	private static Object getCellValue(Cell cell)
	{
		switch (cell.getCellType()) {
		case STRING:
			return cell.getStringCellValue();

		case BOOLEAN:
			return cell.getBooleanCellValue();

		case NUMERIC:
			return cell.getNumericCellValue();
		}

		return null;
	}

}
