package utility;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import pages.SmartWatchesPage;

public class commonutilmethods {

	private WebDriver driver;
	public commonutilmethods(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void scrolldown() throws InterruptedException
	{
		Thread.sleep(5000);
		JavascriptExecutor jse = ((JavascriptExecutor) driver);
		jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		
	}
	
	/***************(scroll with wait) This function is used to scroll when the page is scrolled to down but all products are not loaded **********/
	
	public void scrolldownone() throws InterruptedException
	{
		try {
            long lastHeight = (long) ((JavascriptExecutor) driver).executeScript("return document.body.scrollHeight");
            int cont=1000;
            while (true) {
                ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, "+cont+");");
                Thread.sleep(2000);

                long newHeight = (long) ((JavascriptExecutor) driver).executeScript("return document.body.scrollHeight");
                if (newHeight <= cont) {
                    break;
                }
//              lastHeight = newHeight;
                cont+=500;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
		
	}
	/*************** This function is used to scroll when the page is scrolled to down but all products are not loaded **********/
	
	public void scrollup()
	{
		JavascriptExecutor jse = ((JavascriptExecutor) driver);
		jse.executeScript("window.scrollTo(0, -document.body.scrollHeight);");
	}
	
	public static void waitMethod() throws InterruptedException
	{
		Thread.sleep(7000);
	}
	
	public static void waitMethodnew() throws InterruptedException
	{
		Thread.sleep(5000);
	}
	
	public void hoverElement(WebElement mainMenuWeb) {
		Actions actions = new Actions(driver);
		   
		   actions.moveToElement(mainMenuWeb).build().perform();
	}
	public void scrolldowntoElement(WebElement ele) throws InterruptedException
	{
		
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ele);
		Thread.sleep(5000); 
	}
	
	/************** select dropdown value by text ********/
	
	  public void doSelectByVisibleText(By Locator,String value)
	   {
		   Select select = new Select(driver.findElement(Locator));
		   select.selectByVisibleText(value);
	   }
	  
	  
	  /************** select dropdown value by text ********/ 
	
	/************** Play and pause embeded youtube video ********/
	
	public void YouTubeVideoPlayPause(WebElement elm) throws InterruptedException {
		
		
		  String urlStr = elm.getAttribute("src");
		   System.out.println("Video Url : " + urlStr);
		   driver.navigate().to(urlStr);
		   
		   
		   waitMethod();
		   
		  
		   JavascriptExecutor jse = (JavascriptExecutor) driver;
		 //Pause
		   jse.executeScript("document.getElementsByTagName(\"video\")[0].pause()");
		   Thread.sleep(3000);
	}
	
	/************** Play and pause embeded youtube video ********/
	
	/*********** Switch to window and close *********/
	
	public void switchwindow() {
		String parentWindow = driver.getWindowHandle();
	      Set<String> handles =  driver.getWindowHandles();
	         for(String windowHandle  : handles)
	             {
	             if(!windowHandle.equals(parentWindow))
	                {
	                driver.switchTo().window(windowHandle);
	               /** Perform your operation here for new window**/
	               driver.close(); //closing child window
	               driver.switchTo().window(parentWindow); //cntrl to parent window
	                }
	             }
	}
	
	/*********** Switch to window and close *********/
	
	
	public boolean existsElement(By locator) {
	    try {
	    	System.out.println("locator");
	        driver.findElement(locator);
	    } catch (NoSuchElementException e) {
	        return false;
	    }
	    return true;
	}
	
	/*********************   Add all the color variants in a List ***************/
	public static ArrayList<String> valueaddinarray(WebDriver driver,int licount) throws InterruptedException {
		String colortext;
		 ArrayList <String> list = new ArrayList<String>();
		for(int j=0;j<licount;j++)
		   {
			   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(j+1)+"]/label")).click();
			   waitMethod();
			   colortext=driver.findElement(SmartWatchesPage.getvarianttitle()).getText();
			   list.add(colortext);
			   
		   }
		
		return list;
	}
	
	/*********************   Add all the color variants in a List ***************/
	
	/*********************   Add all the color variants in a List ***************/
	public static ArrayList<String> valueaddinarraynew(WebDriver driver,int licount) throws InterruptedException {
		String colortext;
		 ArrayList <String> list = new ArrayList<String>();
		for(int j=0;j<licount;j++)
		   {
			   driver.findElement(By.xpath("//div[@class='variants-wrapper']//ul[@class='swatch-wrap']/li["+(j+1)+"]/label")).click();
			   waitMethod();
			   colortext=driver.findElement(SmartWatchesPage.getvarianttitlenew()).getText();
			   list.add(colortext);
			   
		   }
		
		return list;
	}
	
	/*********************   Add all the color variants in a List ***************/
	
	/***************************  Explicit wait for an element ******************/
	
	 public WebElement waitForElement(By by) {
		    WebDriverWait wait = new WebDriverWait(driver,30);
		    WebElement element =  wait.until(ExpectedConditions.visibilityOfElementLocated(by));
			return element;
		}
	
	 /***************************  Explicit wait for an element ******************/
	 
	 
	 /*********************   Add all the products in a List ***************/
		public static ArrayList<String> productsadd(WebDriver driver,int productcnt) throws InterruptedException {
			String pname;
			 ArrayList <String> list = new ArrayList<String>();
			for(int j=0;j<productcnt;j++)
			   {
				  
				   pname=driver.findElement(By.xpath("(//span[@class='product-title'])["+(j+1)+"]")).getText();
				   list.add(pname);
				   
			   }
			
			return list;
		}
		
		/*********************   Add all the color variants in a List ***************/
}
