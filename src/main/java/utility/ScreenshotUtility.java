package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;




public class ScreenshotUtility {

	public static String getScreenshot(WebDriver driver)
	{
		TakesScreenshot ts=(TakesScreenshot)driver;
		
		File src=ts.getScreenshotAs(OutputType.FILE);
		
		String path=System.getProperty("user.dir")+"\\Screenshots\\"+getCurrentDateTime()+".png";
		
		File destination=new File(path);
		
		try 
		{
			FileUtils.copyFile(src, destination);
		} catch (IOException e) 
		{
			System.out.println("Capture Failed "+e.getMessage());
		}
		
		return path;
	}
	
	public static String getCurrentDateTime()
	{
		DateFormat dateFormat = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
		 
		 //get current date time with Date()
		 Date date = new Date();
		 
		 // Now format the date
		 String date1= dateFormat.format(date);
		 
		 // Print the Date
		 System.out.println("Current date and time is " +date1);
		 
		 return date1;
		 
	}
	
	public static String captureScreenshot(WebDriver driver, String screenshotName) {

		
		String destinationPath = null;
		try {
			
			Thread.sleep(10000);
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
		    System.setProperty("org.uncommons.reportng.escape-output", "false");
             
		    destinationPath = System.getProperty("user.dir") +"\\screenshots\\"+screenshotName+".png";
		    File destination = new File(destinationPath);
		    FileUtils.copyFile(source,destination);
		    
		    
            //Reporter.log("<a href='"+ destinationPath.getAbsolutePath() + "'> <img src='"+ destinationPath.getAbsolutePath() + "' height='100' width='100'/> </a>");
		   // Reporter.log("<br/><a href='"+ destinationPath.getAbsolutePath() + "'>View Screenshot</a><br/>");

		} catch (Exception e) {

		}
		return destinationPath;
	}
	
/*************** Convert screenshot to base 64 encode *********/	
	
public static String getBase64Screenshot(WebDriver driver, String screenshotName) throws InterruptedException, IOException {

		
		FileInputStream fileInputStream = null;
		
		Thread.sleep(10000);
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
	    //System.setProperty("org.uncommons.reportng.escape-output", "false");
         
		String destinationPath = System.getProperty("user.dir") +"\\screenshots\\"+screenshotName+".png";
	    File destination = new File(destinationPath);
	    FileUtils.copyFile(source,destination);
	     byte[] imageBytes=IOUtils.toByteArray(new FileInputStream(destinationPath));
		return "data:image/png;base64,"+Base64.getEncoder().encodeToString(imageBytes);
	}
/*************** Convert screenshot to base 64 encode *********/		
	

}
