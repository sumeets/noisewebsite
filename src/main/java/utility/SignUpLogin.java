package utility;

public class SignUpLogin {
	

	public SignUpLogin()
	{
		
	}
	
	private String firstName;
	
    private String LastName;
	
	private String Email;
	
	private String Pass;
	
	private String ConfPass;
	
	private String Phone;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return LastName;
	}

	public void setLastName(String lastName) {
		this.LastName = lastName;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		this.Email = email;
	}

	public String getPass() {
		return Pass;
	}

	public void setPass(String pass) {
		this.Pass = pass;
	}

	public String getConfPass() {
		return ConfPass;
	}

	public void setConfPass(String confPass) {
		this.ConfPass = confPass;
	}

	public String getPhone() {
		return Phone;
	}

	public void setPhone(String phone) {
		this.Phone = phone;
	}

	
	
	



}
