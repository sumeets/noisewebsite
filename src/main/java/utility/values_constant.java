package utility;

import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pages.SmartWatchesPage;

public class values_constant {

	static ArrayList<String> briocolor,pulsecolor,qubecolor,ultracolor,pro3color,activecolor,pro3assistcolor,pro2color,endurecolor,navcolor,pro2oxycolor,corecolor,agilecolor,navpluscolor,vs303color,airbudspluscolor,airbudsminicolor,airbudscolor,budsplaycolor,shotsrushcolor,shotsneo2color,elanENCEarbudscolor,budssolocolor,budsvs201,airbudsnanocolor,Flaircolor,Tunechargecolor,Tuneactivepluscolor,tuneactivecolor,tunesport2color,tuneelitesportcolor,tunesportcolor,tunelitecolor,nervecolor,sensecolor,xfit1color;
	
	/*********************** Smart Watches List ******************/
	public static ArrayList<String> setBrioColors()
	{
		briocolor=new ArrayList<String>();
		
		briocolor.add("Rose Pink");
		briocolor.add("Silver Grey");
		briocolor.add("Jet Black");
		
		
		//System.out.println("size is"+briocolor.size());
		return briocolor;
		
	}
	
	public static ArrayList<String> setXfit1Colors()
	{
		xfit1color=new ArrayList<String>();
		xfit1color.add("Jet Black");
		xfit1color.add("Silver Grey");
		
		
		//System.out.println("size is"+briocolor.size());
		return xfit1color;
		
	}
	
	public static ArrayList<String> setXfit2Colors(String colortext)
	{
		xfit1color=new ArrayList<String>();
		xfit1color.add(colortext);
		
		//System.out.println("size is"+briocolor.size());
		return xfit1color;
		
	}
	
	public static ArrayList<String> setQubeColors()
	{
		qubecolor=new ArrayList<String>();
		qubecolor.add("Charcoal Grey");
		qubecolor.add("Beige Gold");
		qubecolor.add("Charcoal Black");
		
		System.out.println("size is"+qubecolor.size());
		return qubecolor;
		
	}
	
	public static ArrayList<String> setPulseColors()
	{
		pulsecolor=new ArrayList<String>();
		pulsecolor.add("Jet Black");
		pulsecolor.add("Teal Green");
		pulsecolor.add("Royal Blue");
		pulsecolor.add("Deep Wine");
		pulsecolor.add("Mist Grey");
		
		System.out.println("size is"+pulsecolor.size());
		return pulsecolor;
		
	}
	
	public static ArrayList<String> setUltraColors()
	{
		ultracolor=new ArrayList<String>();
		ultracolor.add("Gunmetal Grey");
		ultracolor.add("Cloud Grey");
		ultracolor.add("Space Blue");
		System.out.println("size is"+ultracolor.size());
		return ultracolor;
		
	}
	
	public static ArrayList<String> setPro3Colors()
	{
		pro3color=new ArrayList<String>();
		pro3color.add("Jet Black");
		pro3color.add("Smoke Green");
		pro3color.add("Smoke Grey");
		pro3color.add("Jet Blue");
		pro3color.add("Rose Pink");
		pro3color.add("Rose Red");
		System.out.println("size is"+pro3color.size());
		return pro3color;
		
	}
	
	
	public static ArrayList<String> setActiveColors()
	{
		activecolor=new ArrayList<String>();
		activecolor.add("Zesty Grey");
		activecolor.add("Robust Black");
		activecolor.add("Sporty Red");
		activecolor.add("Power Blue");
		
		
		System.out.println("size is"+activecolor.size());
		return activecolor;
		
	}
	
	public static ArrayList<String> setPro3AssistColors()
	{
		pro3assistcolor=new ArrayList<String>();
		pro3assistcolor.add("Jet Black");
		pro3assistcolor.add("Smoke Green");
		pro3assistcolor.add("Smoke Grey");
		pro3assistcolor.add("Rose Pink");
		pro3assistcolor.add("Jet Blue");
		
		System.out.println("size is"+pro3assistcolor.size());
		return pro3assistcolor;
		
	}
	
	public static ArrayList<String> setPro2Colors()
	{
		pro2color=new ArrayList<String>();
		pro2color.add("Jet Black");
		pro2color.add("Teal Green");
		pro2color.add("Royal Blue");
		pro2color.add("Deep Wine");
		pro2color.add("Cherry Red");
		pro2color.add("Special Edition");
		
		System.out.println("size is"+pro2color.size());
		return pro2color;
		
	}
	
	public static ArrayList<String> setEndureColors()
	{
		endurecolor=new ArrayList<String>();
		endurecolor.add("Charcoal Black");
		endurecolor.add("Teal Green");
		endurecolor.add("Racing Red");
		
		
		System.out.println("size is"+endurecolor.size());
		return endurecolor;
		
	}
	
	public static ArrayList<String> setNavColors()
	{
		navcolor=new ArrayList<String>();
		navcolor.add("Stealth Black");
		navcolor.add("Camo Green");
		
		System.out.println("size is"+navcolor.size());
		return navcolor;
		
	}
	
	public static ArrayList<String> setpro2oxycolor()
	{
		pro2oxycolor=new ArrayList<String>();
		pro2oxycolor.add("Royal Blue");
		pro2oxycolor.add("Mist Grey");
		pro2oxycolor.add("Teal Green");
		pro2oxycolor.add("Onyx Black");
		pro2oxycolor.add("Deep Wine");
		
		System.out.println("size is"+pro2oxycolor.size());
		return pro2oxycolor;
		
	}
	
	public static ArrayList<String> setcorecolor()
	{
		corecolor=new ArrayList<String>();
		corecolor.add("Silver Grey");
		corecolor.add("Charcoal Black");
		
		System.out.println("size is"+corecolor.size());
		return corecolor;
		
	}
	
	public static ArrayList<String> setagilecolor()
	{
		agilecolor=new ArrayList<String>();
		agilecolor.add("Robust Black");
		agilecolor.add("Silver Grey");
		agilecolor.add("Soft Green");
		agilecolor.add("Rose Pink");
		agilecolor.add("Power Blue");
		
		System.out.println("size is"+agilecolor.size());
		return agilecolor;
		
	}
	
	public static ArrayList<String> setnavpluscolor()
	{
		navpluscolor=new ArrayList<String>();
		navpluscolor.add("Stealth Black");
		
		System.out.println("size is"+navpluscolor.size());
		return navpluscolor;
		
	}
	
	/*********************** Smart Watches List ******************/
	
	
	/*********************** Wireless Earbuds List ******************/
	
	public static ArrayList<String> setbudsvs303color()
	{
		vs303color=new ArrayList<String>();
		vs303color.add("Jet Black");
		
		System.out.println("size is"+vs303color.size());
		return vs303color;
		
	}
	
	public static ArrayList<String> setairbudspluscolor()
	{
		airbudspluscolor=new ArrayList<String>();
		airbudspluscolor.add("Pearl White");
		airbudspluscolor.add("Jet Black");
		
		System.out.println("size is"+airbudspluscolor.size());
		return airbudspluscolor;
		
	}
	
	public static ArrayList<String> setairbudsminicolor()
	{
		airbudsminicolor=new ArrayList<String>();
		airbudsminicolor.add("Pearl White");
		airbudsminicolor.add("Jet Black");
		
		System.out.println("size is"+airbudsminicolor.size());
		return airbudsminicolor;
		
	}
	
	public static ArrayList<String> setairbudscolor()
	{
		airbudscolor=new ArrayList<String>();
		airbudscolor.add("Jet Black");
		airbudscolor.add("Icy White");
		
		System.out.println("size is"+airbudscolor.size());
		return airbudscolor;
		
	}
	
	public static ArrayList<String> setbudsplaycolor()
	{
		budsplaycolor=new ArrayList<String>();
		budsplaycolor.add("Pearl White");
		budsplaycolor.add("Onyx Black");
		budsplaycolor.add("Celeste Blue");
		
		System.out.println("size is"+budsplaycolor.size());
		return budsplaycolor;
		
	}
	
	public static ArrayList<String> setshotsrushcolor()
	{
		shotsrushcolor=new ArrayList<String>();
		shotsrushcolor.add("Charcoal Grey");
		shotsrushcolor.add("Quick Silver");
		
		System.out.println("size is"+shotsrushcolor.size());
		return shotsrushcolor;
		
	}
	
	public static ArrayList<String> setshotsneo2color()
	{
		shotsneo2color=new ArrayList<String>();
		
		shotsneo2color.add("Raven Black");
		shotsneo2color.add("Cobalt Blue");
		shotsneo2color.add("Amber Orange");
		shotsneo2color.add("Lime Green");
		
		
		System.out.println("size is"+shotsneo2color.size());
		return shotsneo2color;
		
	}
	
	public static ArrayList<String> setelanENCEarbudscolor()
	{
		elanENCEarbudscolor=new ArrayList<String>();
		elanENCEarbudscolor.add("Shadow Grey");
		
		System.out.println("size is"+elanENCEarbudscolor.size());
		return elanENCEarbudscolor;
		
	}
	
	public static ArrayList<String> setBudsSolocolor()
	{
		budssolocolor=new ArrayList<String>();
		budssolocolor.add("Charcoal Black");
		budssolocolor.add("Ecru Gold");
		budssolocolor.add("Stone Blue");
		budssolocolor.add("Sage Green");
		
		System.out.println("size is"+budssolocolor.size());
		return budssolocolor;
		
	}
	
	public static ArrayList<String> setBudsVS201color()
	{
		budsvs201=new ArrayList<String>();
		budsvs201.add("Charcoal Black");
		budsvs201.add("Olive Green");
		budsvs201.add("Snow White");
		
		
		System.out.println("size is"+budsvs201.size());
		return budsvs201;
		
	}
	
	public static ArrayList<String> setAirBudsNanocolor()
	{
		airbudsnanocolor=new ArrayList<String>();
		airbudsnanocolor.add("Jet Black");
		airbudsnanocolor.add("Pearl White");
		
		System.out.println("size is"+airbudsnanocolor.size());
		return airbudsnanocolor;
		
	}
	
	/*********************** Wireless Earbuds List ******************/
	
	/*********************** Bluetooth Earphones List ******************/
	
	public static ArrayList<String> setFlaircolor()
	{
		Flaircolor=new ArrayList<String>();
		Flaircolor.add("Stone Blue");
		Flaircolor.add("Forest Green");
		Flaircolor.add("Mist Grey");
		Flaircolor.add("Carbon Black");
		
		System.out.println("size is"+Flaircolor.size());
		return Flaircolor;
		
	}
	
	public static ArrayList<String> setTuneChargecolor()
	{
		Tunechargecolor=new ArrayList<String>();
		Tunechargecolor.add("Jet Black");
		
		
		System.out.println("size is"+Tunechargecolor.size());
		return Tunechargecolor;
		
	}
	
	public static ArrayList<String> setTuneActivePluscolor()
	{
		Tuneactivepluscolor=new ArrayList<String>();
		Tuneactivepluscolor.add("Sapphire Blue");
		Tuneactivepluscolor.add("Garnet Purple");
		Tuneactivepluscolor.add("Jade Green");
		
		
		System.out.println("size is"+Tuneactivepluscolor.size());
		return Tuneactivepluscolor;
		
	}
	
	public static ArrayList<String> setTuneActivecolor()
	{
		tuneactivecolor=new ArrayList<String>();
		tuneactivecolor.add("Stealth Black");
		tuneactivecolor.add("Hot Red");
		tuneactivecolor.add("Pop Yellow");
		tuneactivecolor.add("Mint Green");
		
		System.out.println("size is"+tuneactivecolor.size());
		return tuneactivecolor;
		
	}
	
	public static ArrayList<String> setTuneSport2color()
	{
		tunesport2color=new ArrayList<String>();
		tunesport2color.add("Lime Green");
		tunesport2color.add("Midnight Black");
		tunesport2color.add("Electric Blue");
		tunesport2color.add("Fiery Orange");
		
		System.out.println("size is"+tunesport2color.size());
		return tunesport2color;
		
	}
	
	public static ArrayList<String> setTuneEliteSportcolor()
	{
		tuneelitesportcolor=new ArrayList<String>();
		tuneelitesportcolor.add("Zesty Lime");
		tuneelitesportcolor.add("Vivid Red");
		tuneelitesportcolor.add("Brisk Blue");
		tuneelitesportcolor.add("Lively Black");
		
		System.out.println("size is"+tuneelitesportcolor.size());
		return tuneelitesportcolor;
		
	}
	
	public static ArrayList<String> setTuneSportcolor()
	{
		tunesportcolor=new ArrayList<String>();
		tunesportcolor.add("Black");
		tunesportcolor.add("Twilight Blue");
		tunesportcolor.add("Winter Grey");
		
		System.out.println("size is"+tunesportcolor.size());
		return tunesportcolor;
		
	}
	
	public static ArrayList<String> setTuneLitecolor()
	{
		tunelitecolor=new ArrayList<String>();
		tunelitecolor.add("Stealth Black");
		
		System.out.println("size is"+tunelitecolor.size());
		return tunelitecolor;
		
	}
	
	public static ArrayList<String> setNervecolor()
	{
		nervecolor=new ArrayList<String>();
		nervecolor.add("Jet Black");
		nervecolor.add("Cobalt Blue");
		nervecolor.add("Jade Green");
		nervecolor.add("Silver Grey");
		
		System.out.println("size is"+nervecolor.size());
		return nervecolor;
		
	}
	
	public static ArrayList<String> setSensecolor()
	{
		sensecolor=new ArrayList<String>();
		sensecolor.add("Jet Black");
		sensecolor.add("Cobalt Blue");
		
		
		System.out.println("size is"+sensecolor.size());
		return sensecolor;
		
	}
	
	
	
	
	/*********************** Bluetooth Earphones List ******************/
}



